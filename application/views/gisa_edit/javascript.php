    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.inputmask.bundle.js"></script>
    <script type="text/javascript">
            $("#tgl_kwn").inputmask('DD/MM/YYYY', {
                yearrange: { minyear: 1700, maxyear: <?php echo date("Y"); ?> },
                placeholder: 'DD/MM/YYYY',
                showMaskOnHover: true,
                showMaskOnFocus: true
            });
            $("#tgl_crai").inputmask('DD/MM/YYYY', {
                yearrange: { minyear: 1700, maxyear: <?php echo date("Y"); ?> },
                placeholder: 'DD/MM/YYYY',
                showMaskOnHover: true,
                showMaskOnFocus: true
            });
        $(document).ready(function() {

            $('select[name="akta_lhr"]').on('change', function() {
            var akta_lhr = $(this).val();
            if(akta_lhr == 'TIDAK ADA') {
                $('#no_akta_lhr').attr("readonly",true);
                $('#no_akta_lhr').val("");
            }else{
                $('#no_akta_lhr').attr("readonly",false);   
            }
            });
            $('select[name="akta_kwn"]').on('change', function() {
            var akta_lhr = $(this).val();
            if(akta_lhr == 'TIDAK TERCATAT') {
                $('#no_akta_kwn').attr("readonly",true);
                $('#tgl_kwn').attr("readonly",true);
                $('#no_akta_kwn').val("");
                $('#tgl_kwn').val("");
            }else{
                $('#no_akta_kwn').attr("readonly",false);  
                $('#tgl_kwn').attr("readonly",false); 
            }
            });
            $('select[name="akta_crai"]').on('change', function() {
            var akta_lhr = $(this).val();
            if(akta_lhr == 'TIDAK TERCATAT') {
                $('#no_akta_crai').attr("readonly",true);
                $('#tgl_crai').attr("readonly",true);
                $('#no_akta_crai').val("");
                $('#tgl_crai').val("");
            }else{
                $('#no_akta_crai').attr("readonly",false);   
                $('#tgl_crai').attr("readonly",false);
            }
            });
            var akta_lhr = $('select[name="akta_lhr"]').val();
            if (akta_lhr == 'TIDAK ADA'){
                $('#no_akta_lhr').attr("readonly",true);   
            }
            var no_akta_lhr = $("#no_akta_lhr");
            var akta_kwn = $('select[name="akta_kwn"]').val();
            if (akta_kwn == 'TIDAK TERCATAT'){
                $('#no_akta_kwn').attr("readonly",true);   
                $('#tgl_kwn').attr("readonly",true);   
            }
            var no_akta_kwn = $("#no_akta_kwn");
            var akta_crai = $('select[name="akta_crai"]').val();
            if (akta_crai == 'TIDAK TERCATAT'){
                $('#no_akta_crai').attr("readonly",true);   
                $('#tgl_crai').attr("readonly",true);   
            }
            var no_akta_crai = $("#no_akta_crai");
            console.log(akta_lhr);
            console.log(no_akta_lhr.val());
            console.log(akta_kwn);
            console.log(no_akta_kwn.val());
            console.log(akta_crai);
            console.log(no_akta_crai.val());
            console.log(<?php echo $nik; ?>);
            });

        function on_save(){
        if (validationdaily()){
          do_save();
          // on_clear();
          // alert('Berhasil Save');
        }
  
        
    }
    function validationdaily() {
        var akta_lhr = $('select[name="akta_lhr"]').val();
        var no_akta_lhr = $("#no_akta_lhr");
        var akta_kwn = $('select[name="akta_kwn"]').val();
        var no_akta_kwn = $("#no_akta_kwn");
        var tgl_kwn = $("#tgl_kwn");
        var akta_crai = $('select[name="akta_crai"]').val();
        var no_akta_crai = $("#no_akta_crai");
        var tgl_crai = $("#tgl_crai");
        console.log(akta_lhr);
        console.log(no_akta_lhr.val());
        console.log(akta_kwn);
        console.log(no_akta_kwn.val());
        console.log(tgl_kwn.val());
        console.log(akta_crai);
        console.log(no_akta_crai.val());
        console.log(tgl_crai.val());
        console.log(<?php echo $nik; ?>);
            if (no_akta_lhr.val().length == 0 && akta_lhr == 'ADA') {                
                  swal("Warning!", "Input Nomor Akta Kelahiran Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (tgl_kwn.val().length == 0 && no_akta_kwn.val().length == 0 && akta_kwn == 'TERCATAT') {                
                  swal("Warning!", "Input Nomor Akta Perkawinan/Tanggal Perkawinan Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (tgl_crai.val().length == 0 && no_akta_crai.val().length == 0 && akta_lhr == 'TERCATAT') {                
                  swal("Warning!", "Input Nomor Akta Perceraian/Tanggal Perceraian Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Gisa/dosave",
                    dataType: "json",
                    data: {
                        nik : <?php echo $nik; ?>,
                        akta_lhr : $('select[name="akta_lhr"]').val(),
                        no_akta_lhr : $("#no_akta_lhr").val(),
                        akta_kwn : $('select[name="akta_kwn"]').val(),
                        no_akta_kwn : $("#no_akta_kwn").val(),
                        akta_crai : $('select[name="akta_crai"]').val(),
                        no_akta_crai : $("#no_akta_crai").val(),
                        user_name : '<?php $user_id = (!empty($user_id)) ? $user_id : '-'; echo $user_id;?>'
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
        function on_clear(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Gisa/getdata",
                    dataType: "json",
                    data: {
                        nik : <?php echo $nik; ?>
                    },
                    beforeSend:
                    function () {
                       block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        
                        if(data.akta_lhr == "ADA"){
                            $('select[name="akta_lhr"]').empty();
                            $('select[name="akta_lhr"]').append('<option value="ADA">ADA</option>');
                            $('select[name="akta_lhr"]').val("ADA").trigger("change");
                            $('#no_akta_lhr').val(data.no_akta_lhr);
                        }else{
                            $('select[name="akta_lhr"]').empty();
                            $('select[name="akta_lhr"]').append('<option value="TIDAK ADA">TIDAK ADA</option>');
                            $('select[name="akta_lhr"]').append('<option value="ADA">ADA</option>');
                            $('select[name="akta_lhr"]').val("TIDAK ADA").trigger("change");
                            $('#no_akta_lhr').val("");
                        }
                        if(data.akta_kwn == "TERCATAT"){
                            $('select[name="akta_kwn"]').empty();
                            $('select[name="akta_kwn"]').append('<option value="TERCATAT">TERCATAT</option>');
                            $('select[name="akta_kwn"]').val("TERCATAT").trigger("change");
                            $('#no_akta_kwn').val(data.no_akta_kwn);
                            $('#tgl_kwn').val(data.tgl_kwn);
                        }else{
                            $('select[name="akta_kwn"]').empty();
                            $('select[name="akta_kwn"]').append('<option value="TIDAK TERCATAT">TIDAK TERCATAT</option>');
                            $('select[name="akta_kwn"]').append('<option value="TERCATAT">TERCATAT</option>');
                            $('select[name="akta_kwn"]').val("TIDAK TERCATAT").trigger("change");
                            $('#no_akta_kwn').val("");
                            $('#tgl_kwn').val("");
                        }
                        if(data.akta_crai == "TERCATAT"){
                            $('select[name="akta_crai"]').empty();
                            $('select[name="akta_crai"]').append('<option value="TERCATAT">TERCATAT</option>');
                            $('select[name="akta_crai"]').val("TERCATAT").trigger("change");
                            $('#no_akta_crai').val(data.no_akta_crai);
                            $('#tgl_crai').val(data.tgl_crai);
                        }else{
                            $('select[name="akta_crai"]').empty();
                            $('select[name="akta_crai"]').append('<option value="TIDAK TERCATAT">TIDAK TERCATAT</option>');
                            $('select[name="akta_crai"]').append('<option value="TERCATAT">TERCATAT</option>');
                            $('select[name="akta_crai"]').val("TIDAK TERCATAT").trigger("change");
                            $('#no_akta_crai').val("");
                            $('#tgl_crai').val("");
                        }
                        $('#no_akta_lhr').attr("readonly",true);
                        $('#no_akta_kwn').attr("readonly",true);
                        $('#no_akta_crai').attr("readonly",true);
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }

    </script>