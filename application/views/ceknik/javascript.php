    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="<?php echo base_url()?>assets/js/footable-init.js"></script>
   
     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       jQuery('#tgl_lhr').datepicker({
            format: 'dd-mm-yyyy',               
            autoclose: true,
            todayHighlight: true,
            orientation: "bottom left"
        });
    $(document).ready(function($) {
        $('#nik').focus();
        $('#nama_lgkp').keyup(function()
        {
            var yourInput = $(this).val();
            re = /[`~!@#$^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
            var isSplChar = re.test(yourInput);
            if(isSplChar)
            {
                var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                $(this).val(no_spl_char);
            }
        });
        $('#nama_lgkp').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[`~!@#$^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
            })
          })
        });
        $('#nik').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[^0-9,]/g, '')
            })
          })
        });
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_kecamatan",
                    dataType: "json",
                    data: {
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec"]').attr("disabled",true);
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                       <?php if ($this->session->userdata(S_NO_KEC) == 0){ ?>
                       $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php }else if($akses_kec > 0){ ?>
                        $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php } ?>
                        $.each(data, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.NO_KEC +'">'+ value.NAMA_KEC +'</option>');
                        });
                        <?php if ($this->session->userdata(S_NO_KEC) == 0){ ?>
                       $('select[name="no_kec"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kec"]').val("<?php echo $this->session->userdata(S_NO_KEC); ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec"]').attr("disabled",false);
                        $('select[name="no_kel"]').attr("disabled",false);
                    }
                });
    $('select[name="no_kec"]').on('change', function() {

            var no_kec = $(this).val();

            if(no_kec != 0) {
                
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_kelurahan",
                    dataType: "json",
                    data: {
                        no_kec : no_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                          $('select[name="no_kel"]').empty();
                       <?php if ($this->session->userdata(S_NO_KEL) == 0){ ?>
                       $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                       <?php }else if($akses_kel > 0){ ?>
                        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                       <?php } ?>
                        $.each(data, function(key, value) {
                            $('select[name="no_kel"]').append('<option value="'+ value.NO_KEL +'">'+ value.NAMA_KEL +'</option>');
                        });
                        <?php if ($this->session->userdata(S_NO_KEL) == 0){ ?>
                       $('select[name="no_kel"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kel"]').val("<?php echo $this->session->userdata(S_NO_KEL); ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kel"]').attr("disabled",false);
                    }
                });
            }else{
                 $('select[name="no_kel"]').empty();
                 $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                 $('select[name="no_kel"]').val("0").trigger("change");
            }

        });
    });
    function do_export(){
        $('#export_niks').val();
        $('#export_data').submit();
    }
    function do_pdf(){
        $('#pdf_niks').val();
        $('#pdf_data').submit();
    }
    function on_clear() {
        $('#my_data').empty();
        $('#my_data').append('<tr><td colspan="10" style="text-align: center;">No data available</td></tr>');
        $('#my_foot').empty();
        $('#my_foot').append('<tr><th colspan="10" style="text-align: left;">Total : 0 Data</th></tr><tr style="border: 0px solid black;"><td colspan="10" style="border: 0px solid black;"><div class="text-right"><ul class="pagination"> </ul></div></td></tr>');
        $('#cb_nik').attr('checked', false);
        $('#nik').attr("disabled",true);
        $('#nik').val("");
        $('#nik').focus();
    }
    function on_serach(){
        $('#my_data').html('<tr><td colspan="10" style="text-align: center;" valign="center">Waiting For Generate Data</td></td>');
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function openInNewTab(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }
    function openTab(url) {
        var win = window.location.replace(url);
        win.focus();
    }
     jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-inverse',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
    </script>