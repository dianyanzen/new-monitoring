<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <div class="white-box">
                            <h3 class="box-title">Contact User Monitoring Civil Service And Civil Registration Officers</h3></div>
                        </div>
                    <!-- .col -->
                    <?php
                    if (!empty($theuser)){
                        $x = 0;
                    foreach($theuser as $row){
                          $x++;?>
                    <div class="col-md-6 col-sm-6">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 ext-center">
                                    
                                            <?php $filename = 'assets/upload/pp/'.$row->NIK.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$row->NIK.'.jpg') ?>'  class="img-circle img-responsive" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/calming-cat.gif" class="img-circle img-responsive" alt="user-img">
                                        <?php }?>

                                </div>
                                <div class="col-md-9 col-sm-9">
                                    <h4 class="box-title m-b-0"><?php echo $row->NAMA_LGKP ;?></h4> <a href="#"><small><?php echo $row->NAMA_KANTOR ;?></small></a>
                                    <p>
                                        <address>
                                            <?php echo $row->TMPT_LHR ;?>,
                                            <br/> <?php echo $row->TGL_LHR ;?>
                                            <br/>
                                            <br/>
                                            <abbr title="Phone">Phone: <?php echo $row->TELP ;?></abbr>
                                            <br/>
                                            <br/> <?php echo $row->ALAMAT_RUMAH ;?>
                                        </address>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
           
                </div>
          
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>