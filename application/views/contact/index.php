<!DOCTYPE html>
<html lang="en">
<?php $this->view('shared/head'); ?>
<body class="fix-header">
    <div id="wrapper">
            <?php $this->view('shared/nav_top'); ?>
            <?php $this->view('shared/nav_side'); ?>
            <?php $this->view('contact/content'); ?>
    </div>
     
    <?php $this->view('shared/footer'); ?>
    
    <?php $this->view('contact/javascript'); ?>
</body>
</html>