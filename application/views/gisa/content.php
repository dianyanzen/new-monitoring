 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> No Rw</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_rw" id="no_rw">
                                         <option  value="0">-- Pilih No Rw --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> No Rt</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_rt" id="no_rt">
                                        <option value="0">-- Pilih No Rt --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" id="done_button" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_search();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $stitle; ?></h3>
                            <?php if (!empty($data)){ 
                                $no_kks = array();
                                foreach($data as $row){
                                    $no_kks[] = $row->NO_KK;
                                    }
                                ?>
                                <form target="_blank" name ="get_form" action="<?php echo $my_url; ?>/Export" method="post">
                                    <input type="hidden" name="no_kks" value="<?php echo implode(',',$no_kks); ?>">
                                <button type="submit" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-file-excel-box m-r-5"></i> <span>Export</span></button>
                                </form>
                            <?php } ?>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEPKEL</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">RT/RW</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEL</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">DETAIL</th>
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                             <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->NO_KK ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                          <?php echo $row->NAMA_KEP ;?>
                                                </td>
                                                
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->ALAMAT ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->NO_RT ;?>/<?php echo $row->NO_RW ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->NAMA_KEC ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->NAMA_KEL ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><a href="<?php echo base_url()?>gisa/cek_detail?no_kk=<?php echo $row->NO_KK ;?>" onclick="on_menu();"><span  class="btn btn-primary btn-xs" id="btn-detail" style="margin-top: 5px;">Detail KK<i class="mdi  mdi-near-me fa-fw"></i></span></a></td>
                                            </tr>

                                            <?php } }else{ ?>
                                            
                                                <tr>
                                                <?php if (!empty($jumlah)){ ?>
                                                <td colspan="9" style="text-align: center;">Ada <?php echo number_format(htmlentities($jumlah),0,',','.');?> Kartu Keluarga</td>
                                                <?php }else{ ?>
                                                <td colspan="9" style="text-align: center;">No data available</td>
                                                <?php } ?>
                                            </tr>
                                            <?php }?>

                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="9" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                            
                                                <?php if (!empty($jumlah)){ ?>
                                                <tr>
                                                <th colspan="9" style="text-align: left;">Total : <?php echo number_format(htmlentities($jumlah),0,',','.');?> Kartu Keluarga</th>
                                                </tr>
                                                <?php }else{ ?>
                                                <tr>
                                                <th colspan="9" style="text-align: left;">Total : 0 Data</th>
                                                </tr>
                                                <?php } ?>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       <?php $this->view('shared/footer_detail'); ?>