     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
    });
    function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
    function edit(data) {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Barang/MasterBarang_in/Edit/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function hapus(barang) {
           
            swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Menghapus Data Ini ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Hapus Transaksi Barang",   
            cancelButtonText: "Tidak, Jangan Dihapus",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/delete_master_in_barang",
                    dataType: "json",
                    data: {
                        barang : barang
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Data Ini Telah Dihapus !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Data Ini Tidak Jadi Dihapus :)", "error");   
                } 
            });
        }
    function validationdaily() {
         var kd_brg = $("#kd_brg");
            if (kd_brg.val().length == 0) {                
                  swal("Warning!", "Jenis Barang Tidak Boleh Kosong !", "warning");  
                  $("#kd_brg").focus();
                 return false;
            }
         var kd_brg = $("#kd_brg");
            if (kd_brg.val() == 0) {                
                  swal("Warning!", "Jenis Barang Tidak Boleh Kosong !", "warning");  
                  $("#kd_brg").focus();
                 return false;
            }
         var received = $("#received");
            if (received.val().length == 0) {                
                  swal("Warning!", "Penerima Tidak Boleh Kosong !", "warning");  
                  $("#received").focus();
                 return false;
            }
         var jumlah = $("#jumlah");
            if (jumlah.val().length == 0) {                
                  swal("Warning!", "Jumlah Tidak Boleh Kosong !", "warning");  
                  $("#jumlah").focus();
                 return false;
            }
         var txt_ket = $("#txt_ket");
            if (txt_ket.val().length == 0) {                
                  swal("Warning!", "Keterangan Tidak Boleh Kosong !", "warning");  
                  $("#txt_ket").focus();
                 return false;
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/insert_master_in_barang",
                    dataType: "json",
                    data: {
                        kd_brg : $("#kd_brg").val(),
                        received : $("#received").val(),
                        jumlah : $("#jumlah").val(),
                        keterangan : $("#txt_ket").val(),
                       
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        on_clear();
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function on_clear(){
            $('#jumlah').val("0");
            $('#received').val("");
            $('select[name="kd_brg"]').val("0").trigger("change");
            $('#txt_ket').val("");
            $('#tanggal').val("<?php echo date('d-m-Y');?>");
            get_table();
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/get_barang",
                    dataType: "json",
                    data: {
                    },
                    beforeSend:
                    function () {
                        $('select[name="kd_brg"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kd_brg"]').empty();
                       $('select[name="kd_brg"]').append('<option value="0">-- Pilih Barang --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kd_brg"]').append('<option value="'+ value.BARANG_ID +'">'+ value.JENIS_BARANG +'</option>');
                        });
                        $('select[name="kd_brg"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kd_brg"]').attr("disabled",false);
                    }
                });
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-inverse',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#list_barang').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#jumlah').val("0");
            $('#txt_ket').val("");
            $('#tanggal').val("<?php echo date('d-m-Y');?>");
            get_table();
        });
    function get_table() {
        $('#list_barang').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Barang/get_master_barang_in",
                "type": "post",
                "data": {

                }
                }
            });
    }
    </script>
   