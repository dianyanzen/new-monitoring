 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}

 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                           <div class="row">
                            <div class="col-md-12">
                                    <div class="white-box">
                                        <h3 class="box-title"><?php echo $mtitle; ?></h3>
                                        <div class="scrollable">
                                            <div class="table-responsive">
                                                <table id="list_barang" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                               <thead>
                                                
                                                    <tr>
                                                        <th width="5%" style="text-align: center;">No</th>
                                                        <th width="25%" style="text-align: center;">Jenis Barang</th>
                                                        <th width="20%" style="text-align: center;">Satuan</th>
                                                        <th width="20%" style="text-align: center;">Harga</th>
                                                        <th width="20%" style="text-align: center;">Jumlah Masuk</th>
                                                        <th width="20%" style="text-align: center;">Jumlah Keluar</th>
                                                        <th width="20%" style="text-align: center;">Saldo</th>
                                                        <th width="20%" style="text-align: center;">Harga Aset</th>

                                                    </tr>
                                                   
                                                </thead>
                                                <tbody id="show_data">
                                                   
                                                    
                                                </tbody>
                                                
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                    <!-- </form> -->
                </div>
               
                 </div>
       <?php $this->view('shared/footer_detail'); ?>