<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <?php   if (!empty($data)){
                            foreach($data as $row){?>
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="<?php echo base_url('assets/plugins/images/wallpaper2.jpg') ?>">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"> <?php $filename = 'assets/upload/pp/'.$row->NIK.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$row->NIK.'.jpg') ?>'  class="img-circle thumb-lg" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/calming-cat.gif" class="img-circle thumb-lg" alt="user-img">
                                        <?php }?></a>
                                        <h4 class="text-white"><?php echo $row->NAMA_LGKP; ?></h4>
                                        <h5 class="text-white"><?php echo $row->NAMA_KANTOR; ?></h5>  
                                        <!-- <h5 class="text-white">Clock In : <?php echo $row->JAM_MASUK; ?> Clock Out : <?php echo $row->JAM_KELUAR; ?></h5> -->
                                    </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-success"><b>Sisa<br>Pengajuan</b></p>
                                    <h1><?php echo $row->PENGAJUAN_S; ?></h1> 
                                </div>
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-success"><b>Sisa<br>Prr</b></p>
                                    <h1><?php echo $row->PRR_S; ?></h1> 
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                   
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>