<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url()?>">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Bulan Laporan</b></h3>
                                        <!-- <input type="text" placeholder="" data-mask="99/99/9999" class="form-control">  -->
                                        <input class="form-control" type="text" id="bulan" data-mask="99/9999" name="bulan" onkeypress="return isNumberKey(event)" onchange="onlyNum()" />
                            </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            
                                            <th width="10%" style="text-align: right;">No</th>
                                            <th width="20%" style="text-align: right;">Tanggal</th>
                                            <th width="20%" style="text-align: right;">Sukasari</th>
                                            <th width="20%" style="text-align: right;">Coblong</th>
                                            <th width="20%" style="text-align: right;">Babakan Ciparay</th>
                                            <th width="20%" style="text-align: right;">Bojongloa Kaler</th>
                                            <th width="20%" style="text-align: right;">Andir</th>
                                            <th width="20%" style="text-align: right;">Cicendo</th>
                                            <th width="20%" style="text-align: right;">Sukajadi</th>
                                            <th width="20%" style="text-align: right;">Cidadap</th>
                                            <th width="20%" style="text-align: right;">Bandung Wetan</th>
                                            <th width="20%" style="text-align: right;">Astana Anyar</th>
                                            <th width="20%" style="text-align: right;">Regol</th>
                                            <th width="20%" style="text-align: right;">Batununggal</th>
                                            <th width="20%" style="text-align: right;">Lengkong</th>
                                            <th width="20%" style="text-align: right;">Cibeunying Kidul</th>
                                            <th width="20%" style="text-align: right;">Bandung Kulon</th>
                                            <th width="20%" style="text-align: right;">Kiaracondong</th>
                                            <th width="20%" style="text-align: right;">Bojongloa Kidul</th>
                                            <th width="20%" style="text-align: right;">Cibeunying Kaler</th>
                                            <th width="20%" style="text-align: right;">Sumur Bandung</th>
                                            <th width="20%" style="text-align: right;">Antapani</th>
                                            <th width="20%" style="text-align: right;">Bandung Kidul</th>
                                            <th width="20%" style="text-align: right;">Buahbatu</th>
                                            <th width="20%" style="text-align: right;">Rancasari</th>
                                            <th width="20%" style="text-align: right;">Arcamanik</th>
                                            <th width="20%" style="text-align: right;">Cibiru</th>
                                            <th width="20%" style="text-align: right;">Ujung Berung</th>
                                            <th width="20%" style="text-align: right;">Gedebage</th>
                                            <th width="20%" style="text-align: right;">Panyileukan</th>
                                            <th width="20%" style="text-align: right;">Cinambo</th>
                                            <th width="20%" style="text-align: right;">Mandalajati</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){ $i = $i+ 1; ?>
                                            
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $i ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->DAY ;?></td>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R1 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R2 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R3 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R4 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R5 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R6 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R7 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R8 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R9 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R10 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R11 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R12 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R13 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R14 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R15 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R16 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R17 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R18 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R19 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R20 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R21 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R22 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R23 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R24 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R25 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R26 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R27 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R28 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R29 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->R30 ;?></th>
                                                
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="30%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R5),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R6),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R7),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R8),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R9),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R10),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R11),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R12),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R13),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R14),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R15),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R16),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R17),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R18),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R19),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R20),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R21),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R22),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R23),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R24),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R25),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R26),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R27),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R28),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R29),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R30),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
           <?php $this->view('shared/footer_detail'); ?>
        </div>