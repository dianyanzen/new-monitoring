     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $("#txt_receive").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
        // get_all_data();
        $('input[type=text]').on('input', function(evt) {
            $(this).val(function(_, val) {
            return val.toUpperCase();
            });
        });
        $('#txt_nik').on('keyup',function(){
              var my_txt = $(this).val();
              var len = my_txt.length;
              if(len == 16)
              {
                  get_nik_paste();
                  
              }
        });
    });
    function get_nik_paste(){
         var nik = $("#txt_nik");
         if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
          }else{
                get_nik();
          }
    }
    function get_nik(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_nik",
                    dataType: "json",
                    data: {
                        nik : $("#txt_nik").val()
                    },
                    beforeSend:
                    function () {
                       block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        $('#txt_nama').val(data.nama);
                        $('select[name="no_kec"]').empty();
                        $('select[name="no_kec"]').append('<option value="'+ data.no_kec +'">'+ data.nama_kec +'</option>');
                        $('select[name="no_kec"]').val(data.no_kec).trigger("change");
                        if (data.is_exists == 1){    
                            $("#txt_nama").attr("readonly", true); 
                            $('#txt_request').focus();
                        }else{
                            $("#txt_nama").attr("readonly", false); 
                            $('#txt_nama').focus();
                        }
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function on_clear() {
        
        // $('select[name="no_kec"]').val(init_kec).trigger("change");
        // $('#txt_pengambilan').val("0");
        // $('#txt_rusak').val("0");
        $('#txt_nik').val("");
        $('#txt_nama').val("");
        $("#txt_nama").attr("readonly", true);
        $('select[name="no_kec"]').empty();
        $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
        $('select[name="no_kec"]').val(0).trigger("change");
        $('#txt_request').val("");
        $('#txt_receive').val("");
        $('#txt_nik').focus();
        // get_all_data();
    }
     function on_save(){
        if (validationdaily()){
          do_save();
          // alert("Suceess");
        }
  
        
    }
    function validationdaily() {
        var nik = $("#txt_nik");
        var nama = $("#txt_nama");
        var requset = $("#txt_request");
        var receive = $("#txt_receive");
        console.log(nik);
            if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
                 return false;
            }
            if (nama.val().length == 0) {                
                  swal("Warning!", "Nama Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (requset.val().length == 0) {                
                  swal("Warning!", "Request Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (receive.val().length == 0) {                
                  swal("Warning!", "Receive Tidak Boleh Kosong !", "warning");  
                 return false;
            }
           
          
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/do_save_cetak",
                    dataType: "json",
                    data: {
                        nik : $("#txt_nik").val(),
                        nama : $("#txt_nama").val(),
                        no_kec : $('select[name="no_kec"]').val(),
                        nama_kec : $('select[name="no_kec"]').text(),
                        request_by : $("#txt_request").val(),
                        receive : $("#txt_receive").val(),
                        tanggal : $("#txt_tanggal").val()
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
   jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>
    </script>
   