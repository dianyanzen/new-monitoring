    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () { get_data(); }, 5000);
    });
   function do_ktpl(){
            window.location.href = "<?php echo site_url('Dashboard_ktpel'); ?>";
    }
    function do_bio(){
            window.location.href = "<?php echo site_url('Dashboard_biodata'); ?>";
    }
    function do_mobi(){
            window.location.href = "<?php echo site_url('Dashboard_mobilitas'); ?>";
    }
    function do_capil(){
            window.location.href = "<?php echo site_url('Dashboard_capil'); ?>";
    }
    function get_data() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_yesterday",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#perekaman_today').html(data.perekaman_yesterday);
                            $('#pencetakan_today').html(data.pencetakan_yesterday);
                            $('#sisa_prr').html(data.sisa_prr_yesterday);
                            $('#sisa_sfe').html(data.sisa_sfe_yesterday);
                            $('#sisa_suket').html(data.sisa_suket_yesterday);
                            $('#blangko_out').html(data.blangko_out_yesterday);
                            $('#sisa_duplicate').html(data.duplicate_yesterday);
                            $('#sisa_failure').html(data.failure_yesterday);
                            $('#sisa_blangko_yesterday').html(data.sisa_blangko_yesterday);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data(); }, 5000);
                }
            });
        }
    </script>