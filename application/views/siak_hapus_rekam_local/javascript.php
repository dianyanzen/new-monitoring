
    <script type="text/javascript">
        var init_kec = <?php $user_no_kec = (!empty($user_no_kec)) ? $user_no_kec : 0; echo $user_no_kec;?>;
    </script>
     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $(document).ready(function($) {
        $('#nik').focus();
        $('#nik').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[^0-9,]/g, '')
            })
          })
        });
    });
   
    function on_clear() {
        $('#nik').val("");
        $('select[name="biometric_option"]').val("0").trigger("change");
    }
    function do_delete_biometric_rekam() {
        $.ajax({
                    type: "post",
                    url: "../Siak/delete_biometric_rekam",
                    dataType: "json",
                    data: {
                        nik : $("#txt_nik").html()
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        $('#table_rekam tr:last').after('<tr><td>DEMOGRAPHICS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>FACES</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>FINGERS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>IRIS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>SIGNATURES</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>FACE_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>FINGER_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>IRIS_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>SIGNATURE_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>BIOMETRICS_LOSSLESS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>BIOMETRIC_EXCEPTIONS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>BIOMETRIC_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>AUDITS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>DUPLICATE_RESULTS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>CARD_ISSUANCE_EVIDENCE</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>MIDDLEWARE_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>REPROCESS_FAILED_NIKS</td><td> DELETED !</td></tr>');
                        $('#table_rekam tr:last').after('<tr><td>MANUAL_DEDUP_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>DEMOGRAPHICS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>FACES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>FINGERS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>IRIS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>SIGNATURES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>FACE_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>FINGER_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>IRIS_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>SIGNATURE_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>BIOMETRICS_LOSSLESS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>BIOMETRIC_EXCEPTIONS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>BIOMETRIC_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>AUDITS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>DUPLICATE_RESULTS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>CARD_ISSUANCE_EVIDENCE</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>MIDDLEWARE_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>REPROCESS_FAILED_NIKS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>MANUAL_DEDUP_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $("#face_holder").attr("src","<?php echo base_url()?>assets/plugins/images/calming-cat.gif");
                        $("#face_holder_href").attr("href","<?php echo base_url()?>assets/plugins/images/calming-cat.gif");
                        $("#delete_biometric_rekam").css("display", "none");
                        $('#txt_nama_lgkp').html("-");
                        $('#txt_nik').html("-");
                        $('#txt_no_kk').html("-");
                        $('#txt_tmpt_lhr').html("-");
                        $('#txt_tgl_lhr').html("-");
                        $('#txt_jenis_klmin').html("-");
                        $('#txt_status_ktp').html("-");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
    }
    function do_delete_biometric_cetak(){
            $.ajax({
                    type: "post",
                    url: "../Siak/delete_biometric_cetak",
                    dataType: "json",
                    data: {
                        nik : $("#txt_nik").html()
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        $('#table_cetak tr:last').after('<tr><td>DEMOGRAPHICS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>FACES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>FINGERS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>IRIS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>SIGNATURES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>FACE_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>FINGER_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>IRIS_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>SIGNATURE_TEMPLATES</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>BIOMETRICS_LOSSLESS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>BIOMETRIC_EXCEPTIONS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>BIOMETRIC_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>AUDITS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>DUPLICATE_RESULTS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>CARD_ISSUANCE_EVIDENCE</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>MIDDLEWARE_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>REPROCESS_FAILED_NIKS</td><td> DELETED !</td></tr>');
                        $('#table_cetak tr:last').after('<tr><td>MANUAL_DEDUP_DIAGNOSTICS</td><td> DELETED !</td></tr>');
                        $("#face_holder").attr("src","<?php echo base_url()?>assets/plugins/images/calming-cat.gif");
                        $("#face_holder_href").attr("href","<?php echo base_url()?>assets/plugins/images/calming-cat.gif");
                        $("#delete_biometric_cetak").css("display", "none");
                        $('#txt_nama_lgkp').html("-");
                        $('#txt_nik').html("-");
                        $('#txt_no_kk').html("-");
                        $('#txt_tmpt_lhr').html("-");
                        $('#txt_tgl_lhr').html("-");
                        $('#txt_jenis_klmin').html("-");
                        $('#txt_status_ktp').html("-");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function on_serach(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
    </script>
   