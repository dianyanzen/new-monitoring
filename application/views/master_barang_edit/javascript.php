     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
    });
     function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Barang/MasterBarang";
            var win = window.location.replace(url);
            win.focus();
        }
       
    function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
    function validationdaily() {
         var jenis_barang = $("#jenis_barang");
            if (jenis_barang.val().length == 0) {                
                  swal("Warning!", "Jenis Barang Tidak Boleh Kosong !", "warning");  
                  $("#jenis_barang").focus();
                 return false;
            }
         var satuan = $("#satuan");
            if (satuan.val().length == 0) {                
                  swal("Warning!", "Satuan Tidak Boleh Kosong !", "warning");  
                  $("#satuan").focus();
                 return false;
            }
         var harga_satuan = $("#harga_satuan");
            if (harga_satuan.val().length == 0) {                
                  swal("Warning!", "Harga Satuan Tidak Boleh Kosong !", "warning");  
                  $("#harga_satuan").focus();
                 return false;
            }
         var txt_ket = $("#txt_ket");
            if (txt_ket.val().length == 0) {                
                  swal("Warning!", "Keterangan Tidak Boleh Kosong !", "warning");  
                  $("#txt_ket").focus();
                 return false;
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/do_edit_master_barang",
                    dataType: "json",
                    data: {
                        kode_barang : $("#kode_barang").val(),
                        jenis_barang : $("#jenis_barang").val(),
                        satuan : $("#satuan").val(),
                        harga_satuan : $("#harga_satuan").val(),
                        keterangan : $("#txt_ket").val(),
                       
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        on_back();
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>
   