<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="<?php echo base_url('assets/plugins/images/wallpaper2.jpg') ?>">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"> <?php $filename = 'assets/upload/pp/'.$user_nik.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$user_nik.'.jpg') ?>'  class="img-circle thumb-lg" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/calming-cat.gif" class="img-circle thumb-lg" alt="user-img">
                                        <?php }?></a>
                                        <h4 class="text-white"><?php echo $user_nama_lgkp; ?></h4>
                                        <h5 class="text-white"><?php if($user_level == 1){echo 'KECAMATAN';}?> <?php echo $user_nama_kantor; ?></h5>  </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-primary"><b> Clock In :</b></p>
                                   <h1><span id="clock_in">-</span></h1> 
                                </div>
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-primary"><b> Clock Out :</b></p>
                                    <h1><span id="clock_out">-</span></h1> 
                                </div>
                            </div>
                        </div>
                        <div class="white-box">
                            <h3><b>My User Control</b> <span class="pull-right"></span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <address>
                                            <h3><b class="text-danger"><?php echo $user_nama_lgkp; ?></b></h3>
                                             <h4 class="font-bold">My Siak</h4>

                                            <p class="m-l-5">
                                                <b class="font-bold">User Id : &nbsp;<span id="user_siak">-</span></b><br/>
                                                <b>Status :</b> &nbsp; <span id="status_online">-</span>
                                                <br/> <b>Ip Address :</b> &nbsp;<span id="siak_ip">-</span>
                                                <br/> <b>Last Login :</b> <i class="fa fa-calendar"></i> &nbsp;<span id="siak_last">-</span>
                                            </p>
                                             <h4 class="font-bold">My Monitoring</h4>
                                            <p class="m-l-5">
                                                 <b class="font-bold">User Id : &nbsp;<span id="user_monev">-</span></b><br/>
                                                <b>Status :</b> &nbsp; <span id="status_online_monev">-</span>
                                                <br/> <b>Ip Address :</b> &nbsp;<span id="monev_ip">-</span>
                                                <br/> <b>Last Login :</b> <i class="fa fa-calendar"></i> &nbsp;<span id="monev_last">-</span>
                                            </p>
                                        </address>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                            <div class="white-box">
                                <h3 class="box-title">Clock In</h3>
                                <hr>
                                    <div class="text-center">
                                        <a href="#" onclick="do_clockin('<?php echo base64_encode($user_id); ?>')">
                                            <i class="fa fa-sign-in fa-4x"></i>
                                            <div class="huge">Clock In</div>
                                        </a>
                                    </div>
                            </div>
                            </div>
                           <div class="col-sm-6 col-lg-6">
                            <div class="white-box">
                                <h3 class="box-title">Clock Out</h3>
                                <hr>
                                    <div class="text-center">
                                        <a href="#" onclick="do_clockout('<?php echo base64_encode($user_id); ?>')">
                                            <i class="fa fa-sign-out fa-4x"></i>
                                            <div class="huge">Clock Out</div>
                                        </a>
                                    </div>
                            </div>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                            <div class="white-box">
                                <h3 class="box-title">Daily Report</h3>
                                <hr>
                                    <div class="text-center">
                                        <a href="#" onclick="do_report_js();">
                                            <i class="fa fa-pencil-square-o fa-4x"></i>
                                            <div class="huge">Daily Report</div>
                                        </a>
                                    </div>
                            </div>
                            </div>
                           <div class="col-sm-6 col-lg-6">
                            <div class="white-box">
                                <h3 class="box-title">Rekap Activity</h3>
                                <hr>
                                    <div class="text-center">
                                        <a href="#" onclick="do_rekap_daily()">
                                            <i class="fa fa-server fa-4x"></i>
                                            <div class="huge">Rekap Activity</div>
                                        </a>
                                    </div>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6 col-lg-6">
                                <div class="white-box">
                                    <h3 class="box-title">Rekap Absensi</h3>
                                <hr>
                                    <div class="text-center">
                                        <a href="#"  onclick="do_rekap()">
                                            <i class="fa fa-steam-square fa-4x"></i>
                                            <div class="huge">Rekap Absensi</div>
                                        </a>
                                    </div>
                            </div>
                            </div>
                              <div class="col-sm-6 col-lg-6">
                                <div class="white-box">
                                    <h3 class="box-title">Excuse Claim</h3>
                                <hr>
                                    <div class="text-center">
                                        <a href="#" onclick="do_excusejs();">
                                            <i class="fa fa-bell fa-4x"></i>
                                            <div class="huge">Excuse Claim</div>
                                        </a>
                                    </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>