     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
    });
    function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
   
    function validationdaily() {
         var jenis_barang = $("#jenis_barang");
            if (jenis_barang.val().length == 0) {                
                  swal("Warning!", "Jenis Barang Tidak Boleh Kosong !", "warning");  
                  $("#jenis_barang").focus();
                 return false;
            }
         var satuan = $("#satuan");
            if (satuan.val().length == 0) {                
                  swal("Warning!", "Satuan Tidak Boleh Kosong !", "warning");  
                  $("#satuan").focus();
                 return false;
            }
         var harga_satuan = $("#harga_satuan");
            if (harga_satuan.val().length == 0) {                
                  swal("Warning!", "Harga Satuan Tidak Boleh Kosong !", "warning");  
                  $("#harga_satuan").focus();
                 return false;
            }
         var txt_ket = $("#txt_ket");
            if (txt_ket.val().length == 0) {                
                  swal("Warning!", "Keterangan Tidak Boleh Kosong !", "warning");  
                  $("#txt_ket").focus();
                 return false;
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/insert_master_barang",
                    dataType: "json",
                    data: {
                        jenis_barang : $("#jenis_barang").val(),
                        satuan : $("#satuan").val(),
                        harga_satuan : $("#harga_satuan").val(),
                        keterangan : $("#txt_ket").val(),
                       
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        on_clear();
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function on_clear(){
            $('#harga_satuan').val("0");
            $('#satuan').val("");
            $('#jenis_barang').val("");
            $('#txt_ket').val("");
            $('#tanggal').val("<?php echo date('d-m-Y');?>");
            get_table();
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-inverse',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
            jQuery('.mydatepicker').datepicker({
                   autoclose: true,
                   format: 'dd-mm-yyyy',
                   orientation: "bottom"
                });
            $('#harga_satuan').val("0");
            $('#txt_ket').val("");
            $('#tanggal').val("<?php echo date('d-m-Y');?>");
s        });
       </script>
   