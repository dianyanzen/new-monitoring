    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="<?php echo base_url()?>assets/js/footable-init.js"></script>
 <script>
   function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Report/Absensi";
            var win = window.location.replace(url);
            win.focus();
        } 
    jQuery(document).ready(function() {
        $('#pdf_tanggal').val('<?php echo date('d-m-Y');?>'+' - '+'<?php echo date('d-m-Y');?>');
        $('#pdf_userid').val('<?php echo $user_id; ?>');
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-inverse',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
           get_table();
           $('.input-daterange-datepicker').on('change', function() {
            var tgl = $(this).val();
            $('#pdf_tanggal').val(tgl);
            get_table();
            });
        });
       
    
    function get_table() {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Report/get_daily_harian_table",
                    dataType: "json",
                    data: {
                        "tanggal":  $("#tanggal").val(),
                        "user_id":  '<?php echo $user_id; ?>'
                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){
                             $('#my_data').empty();
                            $('#my_data').append("<tr><td colspan='5' style='text-align: center;'>No data available</td></tr>");  
                            $('#jumdata').val(0); 
                        }else{
                            var i=0;
                            $('#my_data').empty();
                            $.each(data, function(key, value) {
                                i++;
                                $('#my_data').append("<tr style='border: 1px solid #e4e7ea;'>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: center;'>"+i+". </td>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: center;'>"+value.USER_ID+"</td>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: center;'>"+value.TANGGAL+"</td>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: left;'>"+value.DESCRIPTION+"<br /></td>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: left;'>"+value.JUMLAH+" Aktivitas<br /></td>"+
                                                    "</tr>");
                                $('#jumdata').html(i);
                            }); 
                            
                        }
                       
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                    }
                });
    }
    function do_pdf(){
        $('#pdf_userid').val();
        $('#pdf_tanggal').val();
        $('#pdf_data').submit();
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>