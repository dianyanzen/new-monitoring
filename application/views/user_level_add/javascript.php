     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function on_add_level() {
			if (validationdaily()){
                check_userlvl();
            }
           
        }
        function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/UserLevel";
            var win = window.location.replace(url);
            win.focus();
        }
        function validationdaily() {
        var lvl_cd = $("#lvl_cd");
            if (lvl_cd.val().length == 0) {                
                  swal("Warning!", "Level Code Cannot Be Empty !", "warning");  
                 return false;
            }
        var lvl_nm = $("#lvl_nm");
            if (lvl_nm.val().length == 0) {                
                  swal("Warning!", "Level Name Cannot Be Empty !", "warning");  
                 return false;
            }
        var kdgroup = $("#kdgroup");
            if (kdgroup.val() == 0) {                
                  swal("Warning!", "Please Select The Group Level !", "warning");  
                 return false;
            }

            return true;
        }
        function check_userlvl(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/check_lvl",
                    dataType: "json",
                    data: {
                       lvl_cd : $("#lvl_cd").val()
                    },
                    beforeSend:
                    function () {
                       
                    },
                    success: function (data) {
                      if (data.is_exists == 1){    
                           swal("Warning!", "Level Code Alredy Taken By "+data.lvl_name+" !", "warning");  
                             return false;
                        }else{
                            do_save();
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                    }
                });
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/save_lvl",
                    dataType: "json",
                    data: {
                        lvl_cd : $("#lvl_cd").val(),
                        lvl_nm : $("#lvl_nm").val(),
                        kdgroup : $("#kdgroup").val()

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
        $(document).ready(function() {
           
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_group",
                    dataType: "json",
                    data: {
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdgroup"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdgroup"]').empty();
                       $('select[name="kdgroup"]').append('<option value="0">-- Select Group --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kdgroup"]').append('<option value="'+ value.LEVEL_CODE +'">'+ value.LEVEL_NAME +'</option>');
                        });
                        $('select[name="kdgroup"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdgroup"]').attr("disabled",false);
                    }
                });

    });
    function get_table(kdgroup) {
        console.log(kdgroup);
        $('#group-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Setting/get_user_level",
                "type": "post",
                "data": {
                "group_id":  kdgroup
                }
                }
            });
    }
    
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function on_clear() {
        $('#lvl_cd').val("");
        $('#lvl_nm').val("");
        $('select[name="kdgroup"]').val(0).trigger("change");
        $('#lvl_cd').focus();
    }
    </script>