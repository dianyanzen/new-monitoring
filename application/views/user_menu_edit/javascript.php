    <script src="<?php echo base_url()?>assets/js/gijgo.min.js"></script>
    <link href="<?php echo base_url()?>assets/css/gijgo.min.css" rel="stylesheet">
    
     <script>
        function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/MenuUser";
            var win = window.location.replace(url);
            win.focus();
        }
        $(document).ready(function() {
             $('select[name="menu_level"]').on('change', function() {
                 var mnlvl = $(this).val();
                 if(mnlvl == 1){
                    $('#level_head').attr("disabled",false);
                    $('#level_sub').attr("disabled",true);
                    $('#level_head').val("<?php if (!empty($chead)){echo $chead[0]->LEVEL_HEAD; }?>");
                    $('#level_sub').val("0");
                 }else{
                    $('#level_head').attr("disabled",true);
                    $('#level_sub').attr("disabled",false);
                    $('#level_head').val("0");
                    $('#level_sub').val("<?php if (!empty($chead)){echo $chead[0]->LEVEL_SUB; }?>");

                 }
                 get_parent();   
                  
            });
            $('select[name="menu_level"]').empty();
            $('select[name="menu_level"]').append('<option value="1">Level 1</option>');
            $('select[name="menu_level"]').append('<option value="2">Level 2</option>');
            $('select[name="menu_level"]').append('<option value="3">Level 3</option>');
            $('select[name="menu_level"]').val("<?php if (!empty($chead)){echo $chead[0]->MENU_LEVEL; }?>").trigger("change");
           
        });
        function get_parent(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_parent",
                    dataType: "json",
                    data: {
                        menu_level : $('select[name="menu_level"]').val()
                    },
                    beforeSend:
                    function () {
                        $('select[name="menu_level"]').attr("disabled",false);
                        // $('select[name="parent_id"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                        var no = 0;
                       $('select[name="parent_id"]').empty();
                        $.each(data, function(key, value) {
                            no = no+1;
                            $('select[name="parent_id"]').append('<option value="'+ value.MENU_ID +'">'+ no +'. '+ value.TITLE +'</option>');
                            if (value.MENU_ID == "<?php if (!empty($chead)){echo $chead[0]->PARENT_ID; }?>"){
                                $('select[name="parent_id"]').val("<?php if (!empty($chead)){echo $chead[0]->PARENT_ID; }?>").trigger("change");
                            }else if (key == 0){
                                $('select[name="parent_id"]').val(value.MENU_ID).trigger("change");
                            }
                        });
                            
                        
                    },
                    error:
                    function (data) {
                        

                    },
                    complete:
                    function (response) {
                        $('select[name="parent_id"]').attr("disabled",false);
                        get_child();
                    }
                });
        }
        function get_child(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_child",
                    dataType: "json",
                    data: {
                        menu_id : $('#menu_id').val()
                    },
                    beforeSend:
                    function () {
                        // $('select[name="parent_id"]').attr("disabled",true);
                    },
                    success: function (data) {
                       console.log(data.child);

                            if (data.child >0){
                                 $('select[name="menu_level"]').attr("disabled",true);
                            }else{
                                $('select[name="menu_level"]').attr("disabled",false);
                            }
                    
                            
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                       
                    }
                });
        }
      function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
     function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function on_edit_menu() {
            if (validationdaily()){
                do_save();
            }
           
        }
        function validationdaily() {
        var menu_id = $("#menu_id");
            if (menu_id.val().length == 0) {                
                  swal("Warning!", "Menu Id Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var title = $("#title");
            if (title.val().length == 0) {                
                  swal("Warning!", "Title Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var level_head = $("#level_head");
            if (level_head.val().length == 0) {                
                  swal("Warning!", "Level Head Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var level_sub = $("#level_sub");
            if (level_sub.val().length == 0) {                
                  swal("Warning!", "Level Sub Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var parent_id = $("#parent_id");
            if (parent_id.val().length == 0) {                
                  swal("Warning!", "Parent Id Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var menu_level = $("#menu_level");
            if (menu_level.val().length == 0) {                
                  swal("Warning!", "Menu Level Tidak Boleh Kosong !", "warning");  
                 return false;
            }

            return true;
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/do_edit_menu_id",
                    dataType: "json",
                    data: {
                        menu_id : $("#menu_id").val(),
                        title : $("#title").val(),
                        parent_id : $("#parent_id").val(),
                        menu_level : $("#menu_level").val(),
                        level_head : $("#level_head").val(),
                        level_sub : $("#level_sub").val(),

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_back();
                    }
                });
        }

    </script>