<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url()?>">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Jenis Laporan</b></h3>
                                        <select class="form-control select2" name="mlap" id="mlap">
                                <option  value="0">-- Pilih Menu --</option>
                            </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kecamatan</b></h3>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                <option  value="0">-- Pilih Kecamatan --</option>
                            </select>
                            
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kelurahan</b></h3>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($jenis_lap)){ ?>
                <?php if ($jenis_lap == 1 || $jenis_lap == 2 || $jenis_lap == 3){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">0-4 Tahun</th>
                                            <th width="20%" style="text-align: right;">5-9 Tahun</th>
                                            <th width="20%" style="text-align: right;">10-14 Tahun</th>
                                            <th width="20%" style="text-align: right;">15-19 Tahun</th>
                                            <th width="20%" style="text-align: right;">20-24 Tahun</th>
                                            <th width="20%" style="text-align: right;">25-29 Tahun</th>
                                            <th width="20%" style="text-align: right;">30-34 Tahun</th>
                                            <th width="20%" style="text-align: right;">35-39 Tahun</th>
                                            <th width="20%" style="text-align: right;">40-44 Tahun</th>
                                            <th width="20%" style="text-align: right;">45-49 Tahun</th>
                                            <th width="20%" style="text-align: right;">50-54 Tahun</th>
                                            <th width="20%" style="text-align: right;">55-59 Tahun</th>
                                            <th width="20%" style="text-align: right;">60-64 Tahun</th>
                                            <th width="20%" style="text-align: right;">65-69 Tahun</th>
                                            <th width="20%" style="text-align: right;">70-74 Tahun</th>
                                            <th width="20%" style="text-align: right;">75> Tahun</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                            $SUM_R1 = 0;
                                            $SUM_R2 = 0;
                                            $SUM_R3 = 0;
                                            $SUM_R4 = 0;
                                            $SUM_R5 = 0;
                                            $SUM_R6 = 0;
                                            $SUM_R7 = 0;
                                            $SUM_R8 = 0;
                                            $SUM_R9 = 0;
                                            $SUM_R10 = 0;
                                            $SUM_R11 = 0;
                                            $SUM_R12 = 0;
                                            $SUM_R13 = 0;
                                            $SUM_R14 = 0;
                                            $SUM_R15 = 0;
                                            $SUM_R16 = 0;
                                            $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R11 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R12 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R13 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R14 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R15 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R16 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php 
                                            $SUM_R1 += $row->R1;
                                            $SUM_R2 += $row->R2;
                                            $SUM_R3 += $row->R3;
                                            $SUM_R4 += $row->R4;
                                            $SUM_R5 += $row->R5;
                                            $SUM_R6 += $row->R6;
                                            $SUM_R7 += $row->R7;
                                            $SUM_R8 += $row->R8;
                                            $SUM_R9 += $row->R9;
                                            $SUM_R10 += $row->R10;
                                            $SUM_R11 += $row->R11;
                                            $SUM_R12 += $row->R12;
                                            $SUM_R13 += $row->R13;
                                            $SUM_R14 += $row->R14;
                                            $SUM_R15 += $row->R15;
                                            $SUM_R16 += $row->R16;
                                            $SUM_JUMLAH += $row->JUMLAH;
                                            }
                                         } ?>
                                    </tbody>
                                   <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R11),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R12),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R13),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R14),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R15),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R16),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 4 || $jenis_lap == 5 || $jenis_lap == 6){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">0-9 Tahun</th>
                                            <th width="20%" style="text-align: right;">10-19 Tahun</th>
                                            <th width="20%" style="text-align: right;">20-29 Tahun</th>
                                            <th width="20%" style="text-align: right;">30-39 Tahun</th>
                                            <th width="20%" style="text-align: right;">40-49 Tahun</th>
                                            <th width="20%" style="text-align: right;">50-59 Tahun</th>
                                            <th width="20%" style="text-align: right;">60-69 Tahun</th>
                                            <th width="20%" style="text-align: right;">70-79 Tahun</th>
                                            <th width="20%" style="text-align: right;">80 > Tahun</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                                $SUM_R1 = 0;
                                                $SUM_R2 = 0;
                                                $SUM_R3 = 0;
                                                $SUM_R4 = 0;
                                                $SUM_R5 = 0;
                                                $SUM_R6 = 0;
                                                $SUM_R7 = 0;
                                                $SUM_R8 = 0;
                                                $SUM_R9 = 0;
                                                $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php 
                                                $SUM_R1 += $row->R1;
                                                $SUM_R2 += $row->R2;
                                                $SUM_R3 += $row->R3;
                                                $SUM_R4 += $row->R4;
                                                $SUM_R5 += $row->R5;
                                                $SUM_R6 += $row->R6;
                                                $SUM_R7 += $row->R7;
                                                $SUM_R8 += $row->R8;
                                                $SUM_R9 += $row->R9;
                                                $SUM_JUMLAH += $row->JUMLAH;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 7 || $jenis_lap == 8 || $jenis_lap == 9){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">0-1 Tahun</th>
                                            <th width="20%" style="text-align: right;">1-2 Tahun</th>
                                            <th width="20%" style="text-align: right;">2-3 Tahun</th>
                                            <th width="20%" style="text-align: right;">3-4 Tahun</th>
                                            <th width="20%" style="text-align: right;">4-5 Tahun</th>
                                            <th width="20%" style="text-align: right;">5-6 Tahun</th>
                                            <th width="20%" style="text-align: right;">6-7 Tahun</th>
                                            <th width="20%" style="text-align: right;">7-8 Tahun</th>
                                            <th width="20%" style="text-align: right;">8-9 Tahun</th>
                                            <th width="20%" style="text-align: right;">9-10 Tahun</th>
                                            <th width="20%" style="text-align: right;">10-11 Tahun</th>
                                            <th width="20%" style="text-align: right;">11-12 Tahun</th>
                                            <th width="20%" style="text-align: right;">12-13 Tahun</th>
                                            <th width="20%" style="text-align: right;">13-14 Tahun</th>
                                            <th width="20%" style="text-align: right;">14-15 Tahun</th>
                                            <th width="20%" style="text-align: right;">15-16 Tahun</th>
                                            <th width="20%" style="text-align: right;">16-17 Tahun</th>
                                            <th width="20%" style="text-align: right;">17-18 Tahun</th>
                                            <th width="20%" style="text-align: right;">18-19 Tahun</th>
                                            <th width="20%" style="text-align: right;">19-20 Tahun</th>
                                            <th width="20%" style="text-align: right;">20-21 Tahun</th>
                                            <th width="20%" style="text-align: right;">21-22 Tahun</th>
                                            <th width="20%" style="text-align: right;">22-23 Tahun</th>
                                            <th width="20%" style="text-align: right;">23-24 Tahun</th>
                                            <th width="20%" style="text-align: right;">24-25 Tahun</th>
                                            <th width="20%" style="text-align: right;">25-26 Tahun</th>
                                            <th width="20%" style="text-align: right;">26-27 Tahun</th>
                                            <th width="20%" style="text-align: right;">27-28 Tahun</th>
                                            <th width="20%" style="text-align: right;">28-29 Tahun</th>
                                            <th width="20%" style="text-align: right;">29-30 Tahun</th>
                                            <th width="20%" style="text-align: right;">30-31 Tahun</th>
                                            <th width="20%" style="text-align: right;">31-32 Tahun</th>
                                            <th width="20%" style="text-align: right;">32-33 Tahun</th>
                                            <th width="20%" style="text-align: right;">33-34 Tahun</th>
                                            <th width="20%" style="text-align: right;">34-35 Tahun</th>
                                            <th width="20%" style="text-align: right;">35-36 Tahun</th>
                                            <th width="20%" style="text-align: right;">36-37 Tahun</th>
                                            <th width="20%" style="text-align: right;">37-38 Tahun</th>
                                            <th width="20%" style="text-align: right;">38-39 Tahun</th>
                                            <th width="20%" style="text-align: right;">39-40 Tahun</th>
                                            <th width="20%" style="text-align: right;">40-41 Tahun</th>
                                            <th width="20%" style="text-align: right;">41-42 Tahun</th>
                                            <th width="20%" style="text-align: right;">42-43 Tahun</th>
                                            <th width="20%" style="text-align: right;">43-44 Tahun</th>
                                            <th width="20%" style="text-align: right;">44-45 Tahun</th>
                                            <th width="20%" style="text-align: right;">45-46 Tahun</th>
                                            <th width="20%" style="text-align: right;">46-47 Tahun</th>
                                            <th width="20%" style="text-align: right;">47-48 Tahun</th>
                                            <th width="20%" style="text-align: right;">48-49 Tahun</th>
                                            <th width="20%" style="text-align: right;">49-50 Tahun</th>
                                            <th width="20%" style="text-align: right;">50-51 Tahun</th>
                                            <th width="20%" style="text-align: right;">51-52 Tahun</th>
                                            <th width="20%" style="text-align: right;">52-53 Tahun</th>
                                            <th width="20%" style="text-align: right;">53-54 Tahun</th>
                                            <th width="20%" style="text-align: right;">54-55 Tahun</th>
                                            <th width="20%" style="text-align: right;">55-56 Tahun</th>
                                            <th width="20%" style="text-align: right;">56-57 Tahun</th>
                                            <th width="20%" style="text-align: right;">57-58 Tahun</th>
                                            <th width="20%" style="text-align: right;">58-59 Tahun</th>
                                            <th width="20%" style="text-align: right;">59-60 Tahun</th>
                                            <th width="20%" style="text-align: right;">60-61 Tahun</th>
                                            <th width="20%" style="text-align: right;">61-62 Tahun</th>
                                            <th width="20%" style="text-align: right;">62-63 Tahun</th>
                                            <th width="20%" style="text-align: right;">63-64 Tahun</th>
                                            <th width="20%" style="text-align: right;">64-65 Tahun</th>
                                            <th width="20%" style="text-align: right;">65-66 Tahun</th>
                                            <th width="20%" style="text-align: right;">66-67 Tahun</th>
                                            <th width="20%" style="text-align: right;">67-68 Tahun</th>
                                            <th width="20%" style="text-align: right;">68-69 Tahun</th>
                                            <th width="20%" style="text-align: right;">69-70 Tahun</th>
                                            <th width="20%" style="text-align: right;">70-71 Tahun</th>
                                            <th width="20%" style="text-align: right;">71-72 Tahun</th>
                                            <th width="20%" style="text-align: right;">72-73 Tahun</th>
                                            <th width="20%" style="text-align: right;">73-74 Tahun</th>
                                            <th width="20%" style="text-align: right;">74-75 Tahun</th>
                                            <th width="20%" style="text-align: right;">75-76 Tahun</th>
                                            <th width="20%" style="text-align: right;">76-77 Tahun</th>
                                            <th width="20%" style="text-align: right;">77-78 Tahun</th>
                                            <th width="20%" style="text-align: right;">78-79 Tahun</th>
                                            <th width="20%" style="text-align: right;">79-80 Tahun</th>
                                            <th width="20%" style="text-align: right;">80 > Tahun</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                                $SUM_R1 = 0;
                                                $SUM_R2 = 0;
                                                $SUM_R3 = 0;
                                                $SUM_R4 = 0;
                                                $SUM_R5 = 0;
                                                $SUM_R6 = 0;
                                                $SUM_R7 = 0;
                                                $SUM_R8 = 0;
                                                $SUM_R9 = 0;
                                                $SUM_R10 = 0;
                                                $SUM_R11 = 0;
                                                $SUM_R12 = 0;
                                                $SUM_R13 = 0;
                                                $SUM_R14 = 0;
                                                $SUM_R15 = 0;
                                                $SUM_R16 = 0;
                                                $SUM_R17 = 0;
                                                $SUM_R18 = 0;
                                                $SUM_R19 = 0;
                                                $SUM_R20 = 0;
                                                $SUM_R21 = 0;
                                                $SUM_R22 = 0;
                                                $SUM_R23 = 0;
                                                $SUM_R24 = 0;
                                                $SUM_R25 = 0;
                                                $SUM_R26 = 0;
                                                $SUM_R27 = 0;
                                                $SUM_R28 = 0;
                                                $SUM_R29 = 0;
                                                $SUM_R30 = 0;
                                                $SUM_R31 = 0;
                                                $SUM_R32 = 0;
                                                $SUM_R33 = 0;
                                                $SUM_R34 = 0;
                                                $SUM_R35 = 0;
                                                $SUM_R36 = 0;
                                                $SUM_R37 = 0;
                                                $SUM_R38 = 0;
                                                $SUM_R39 = 0;
                                                $SUM_R40 = 0;
                                                $SUM_R41 = 0;
                                                $SUM_R42 = 0;
                                                $SUM_R43 = 0;
                                                $SUM_R44 = 0;
                                                $SUM_R45 = 0;
                                                $SUM_R46 = 0;
                                                $SUM_R47 = 0;
                                                $SUM_R48 = 0;
                                                $SUM_R49 = 0;
                                                $SUM_R50 = 0;
                                                $SUM_R51 = 0;
                                                $SUM_R52 = 0;
                                                $SUM_R53 = 0;
                                                $SUM_R54 = 0;
                                                $SUM_R55 = 0;
                                                $SUM_R56 = 0;
                                                $SUM_R57 = 0;
                                                $SUM_R58 = 0;
                                                $SUM_R59 = 0;
                                                $SUM_R60 = 0;
                                                $SUM_R61 = 0;
                                                $SUM_R62 = 0;
                                                $SUM_R63 = 0;
                                                $SUM_R64 = 0;
                                                $SUM_R65 = 0;
                                                $SUM_R66 = 0;
                                                $SUM_R67 = 0;
                                                $SUM_R68 = 0;
                                                $SUM_R69 = 0;
                                                $SUM_R70 = 0;
                                                $SUM_R71 = 0;
                                                $SUM_R72 = 0;
                                                $SUM_R73 = 0;
                                                $SUM_R74 = 0;
                                                $SUM_R75 = 0;
                                                $SUM_R76 = 0;
                                                $SUM_R77 = 0;
                                                $SUM_R78 = 0;
                                                $SUM_R79 = 0;
                                                $SUM_R80 = 0;
                                                $SUM_REND = 0;
                                                $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R11 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R12 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R13 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R14 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R15 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R16 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R17 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R18 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R19 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R20 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R21 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R22 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R23 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R24 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R25 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R26 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R27 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R28 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R29 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R30 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R31 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R32 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R33 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R34 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R35 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R36 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R37 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R38 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R39 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R40 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R41 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R42 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R43 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R44 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R45 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R46 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R47 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R48 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R49 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R50 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R51 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R52 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R53 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R54 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R55 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R56 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R57 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R58 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R59 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R60 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R61 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R62 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R63 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R64 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R65 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R66 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R67 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R68 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R69 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R70 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R71 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R72 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R73 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R74 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R75 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R76 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R77 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R78 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R79 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R80 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->REND ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php 
                                                $SUM_R1 += $row->R1;
                                                $SUM_R2 += $row->R2;
                                                $SUM_R3 += $row->R3;
                                                $SUM_R4 += $row->R4;
                                                $SUM_R5 += $row->R5;
                                                $SUM_R6 += $row->R6;
                                                $SUM_R7 += $row->R7;
                                                $SUM_R8 += $row->R8;
                                                $SUM_R9 += $row->R9;
                                                $SUM_R10 += $row->R10;
                                                $SUM_R11 += $row->R11;
                                                $SUM_R12 += $row->R12;
                                                $SUM_R13 += $row->R13;
                                                $SUM_R14 += $row->R14;
                                                $SUM_R15 += $row->R15;
                                                $SUM_R16 += $row->R16;
                                                $SUM_R17 += $row->R17;
                                                $SUM_R18 += $row->R18;
                                                $SUM_R19 += $row->R19;
                                                $SUM_R20 += $row->R20;
                                                $SUM_R21 += $row->R21;
                                                $SUM_R22 += $row->R22;
                                                $SUM_R23 += $row->R23;
                                                $SUM_R24 += $row->R24;
                                                $SUM_R25 += $row->R25;
                                                $SUM_R26 += $row->R26;
                                                $SUM_R27 += $row->R27;
                                                $SUM_R28 += $row->R28;
                                                $SUM_R29 += $row->R29;
                                                $SUM_R30 += $row->R30;
                                                $SUM_R31 += $row->R31;
                                                $SUM_R32 += $row->R32;
                                                $SUM_R33 += $row->R33;
                                                $SUM_R34 += $row->R34;
                                                $SUM_R35 += $row->R35;
                                                $SUM_R36 += $row->R36;
                                                $SUM_R37 += $row->R37;
                                                $SUM_R38 += $row->R38;
                                                $SUM_R39 += $row->R39;
                                                $SUM_R40 += $row->R40;
                                                $SUM_R41 += $row->R41;
                                                $SUM_R42 += $row->R42;
                                                $SUM_R43 += $row->R43;
                                                $SUM_R44 += $row->R44;
                                                $SUM_R45 += $row->R45;
                                                $SUM_R46 += $row->R46;
                                                $SUM_R47 += $row->R47;
                                                $SUM_R48 += $row->R48;
                                                $SUM_R49 += $row->R49;
                                                $SUM_R50 += $row->R50;
                                                $SUM_R51 += $row->R51;
                                                $SUM_R52 += $row->R52;
                                                $SUM_R53 += $row->R53;
                                                $SUM_R54 += $row->R54;
                                                $SUM_R55 += $row->R55;
                                                $SUM_R56 += $row->R56;
                                                $SUM_R57 += $row->R57;
                                                $SUM_R58 += $row->R58;
                                                $SUM_R59 += $row->R59;
                                                $SUM_R60 += $row->R60;
                                                $SUM_R61 += $row->R61;
                                                $SUM_R62 += $row->R62;
                                                $SUM_R63 += $row->R63;
                                                $SUM_R64 += $row->R64;
                                                $SUM_R65 += $row->R65;
                                                $SUM_R66 += $row->R66;
                                                $SUM_R67 += $row->R67;
                                                $SUM_R68 += $row->R68;
                                                $SUM_R69 += $row->R69;
                                                $SUM_R70 += $row->R70;
                                                $SUM_R71 += $row->R71;
                                                $SUM_R72 += $row->R72;
                                                $SUM_R73 += $row->R73;
                                                $SUM_R74 += $row->R74;
                                                $SUM_R75 += $row->R75;
                                                $SUM_R76 += $row->R76;
                                                $SUM_R77 += $row->R77;
                                                $SUM_R78 += $row->R78;
                                                $SUM_R79 += $row->R79;
                                                $SUM_R80 += $row->R80;
                                                $SUM_REND += $row->REND;
                                                $SUM_JUMLAH += $row->JUMLAH;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R11),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R12),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R13),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R14),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R15),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R16),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R17),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R18),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R19),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R20),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R21),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R22),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R23),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R24),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R25),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R26),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R27),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R28),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R29),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R30),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R31),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R32),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R33),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R34),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R35),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R36),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R37),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R38),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R39),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R40),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R41),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R42),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R43),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R44),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R45),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R46),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R47),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R48),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R49),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R50),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R51),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R52),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R53),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R54),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R55),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R56),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R57),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R58),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R59),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R60),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R61),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R62),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R63),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R64),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R65),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R66),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R67),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R68),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R69),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R70),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R71),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R72),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R73),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R74),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R75),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R76),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R77),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R78),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R79),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R80),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_REND),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 10 || $jenis_lap == 11 || $jenis_lap == 12){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Paud</th>
                                            <th width="20%" style="text-align: right;">TK</th>
                                            <th width="20%" style="text-align: right;">SD</th>
                                            <th width="20%" style="text-align: right;">SMP/SLTP</th>
                                            <th width="20%" style="text-align: right;">SMA/SLTA</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                                $SUM_R1 = 0;
                                                $SUM_R2 = 0;
                                                $SUM_R3 = 0;
                                                $SUM_R4 = 0;
                                                $SUM_R5 = 0;
                                                $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php 
                                            $SUM_R1 += $row->R1;
                                            $SUM_R2 += $row->R2;
                                            $SUM_R3 += $row->R3;
                                            $SUM_R4 += $row->R4;
                                            $SUM_R5 += $row->R5;
                                            $SUM_JUMLAH += $row->JUMLAH;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 13){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Laki-Laki</th>
                                            <th width="20%" style="text-align: right;">Perempuan</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                            $SUM_R1 = 0;
                                            $SUM_R2 = 0;
                                            $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php 
                                            $SUM_R1 += $row->R1;
                                            $SUM_R2 += $row->R2;
                                            $SUM_JUMLAH += $row->JUMLAH;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 14){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Belum Sekolah</th>
                                            <th width="20%" style="text-align: right;">Tidak Tamat SD</th>
                                            <th width="20%" style="text-align: right;">Tamat SD</th>
                                            <th width="20%" style="text-align: right;">SLTP</th>
                                            <th width="20%" style="text-align: right;">SLTA</th>
                                            <th width="20%" style="text-align: right;">Diploma II</th>
                                            <th width="20%" style="text-align: right;">Diploma II</th>
                                            <th width="20%" style="text-align: right;">SI</th>
                                            <th width="20%" style="text-align: right;">SII</th>
                                            <th width="20%" style="text-align: right;">SIII</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                                $SUM_R1 = 0;
                                                $SUM_R2 = 0;
                                                $SUM_R3 = 0;
                                                $SUM_R4 = 0;
                                                $SUM_R5 = 0;
                                                $SUM_R6 = 0;
                                                $SUM_R7 = 0;
                                                $SUM_R8 = 0;
                                                $SUM_R9 = 0;
                                                $SUM_R10 = 0;
                                                $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php $SUM_R1 += $row->R1;
                                                $SUM_R2 += $row->R2;
                                                $SUM_R3 += $row->R3;
                                                $SUM_R4 += $row->R4;
                                                $SUM_R5 += $row->R5;
                                                $SUM_R6 += $row->R6;
                                                $SUM_R7 += $row->R7;
                                                $SUM_R8 += $row->R8;
                                                $SUM_R9 += $row->R9;
                                                $SUM_R10 += $row->R10;
                                                $SUM_JUMLAH += $row->JUMLAH;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
            <!-- /.container-fluid -->
           <?php $this->view('shared/footer_detail'); ?>
        </div>