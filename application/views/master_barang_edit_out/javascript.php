     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/get_barang",
                    dataType: "json",
                    data: {
                    },
                    beforeSend:
                    function () {
                        $('select[name="kd_brg"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kd_brg"]').empty();
                       $('select[name="kd_brg"]').append('<option value="0">-- Pilih Barang --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kd_brg"]').append('<option value="'+ value.BARANG_ID +'">'+ value.JENIS_BARANG +'</option>');
                            if (value.BARANG_ID == "<?php if (!empty($data)){echo $data[0]->KODE_BARANG; }?>"){
                                $('select[name="kd_brg"]').val("<?php if (!empty($data)){echo $data[0]->KODE_BARANG; }?>").trigger("change");
                            }else if (key == 0){
                                    $('select[name="kd_brg"]').val(value.MENU_ID).trigger("change");
                            }
                        });
                        
                            
                        
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kd_brg"]').attr("disabled",false);
                    }
                });
    });
     function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Barang/InputMasuk";
            var win = window.location.replace(url);
            win.focus();
        }
       
    function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
      function validationdaily() {
         var kd_brg = $("#kd_brg");
            if (kd_brg.val().length == 0) {                
                  swal("Warning!", "Jenis Barang Tidak Boleh Kosong !", "warning");  
                  $("#kd_brg").focus();
                 return false;
            }
         var kd_brg = $("#kd_brg");
            if (kd_brg.val() == 0) {                
                  swal("Warning!", "Jenis Barang Tidak Boleh Kosong !", "warning");  
                  $("#kd_brg").focus();
                 return false;
            }
         var received = $("#received");
            if (received.val().length == 0) {                
                  swal("Warning!", "Penerima Tidak Boleh Kosong !", "warning");  
                  $("#received").focus();
                 return false;
            }
         var jumlah = $("#jumlah");
            if (jumlah.val().length == 0) {                
                  swal("Warning!", "Jumlah Tidak Boleh Kosong !", "warning");  
                  $("#jumlah").focus();
                 return false;
            }
         var txt_ket = $("#txt_ket");
            if (txt_ket.val().length == 0) {                
                  swal("Warning!", "Keterangan Tidak Boleh Kosong !", "warning");  
                  $("#txt_ket").focus();
                 return false;
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/do_edit_master_barang_out",
                    dataType: "json",
                    data: {
                        kode_barang : $("#kode_barang").val(),
                        kd_brg : $("#kd_brg").val(),
                        received : $("#received").val(),
                        jumlah : $("#jumlah").val(),
                        keterangan : $("#txt_ket").val(),
                       
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        on_back();
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>
   