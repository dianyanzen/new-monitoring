<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">

                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#user_detail" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs" style="color: blue !important">DETAIL USER DATA</span> </a> 
                                </li>
                                <li class="tab">
                                    <a href="#user_wilayah" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-access-point"></i></span> <span class="hidden-xs" style="color: blue !important">WILAYAH USER</span> </a> 
                                </li>
                                <li class="tab">
                                    <a href="#user_akunsiak" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-access-point"></i></span> <span class="hidden-xs" style="color: blue !important">AKUN USER</span> </a> 
                                </li>
                                <li class="tab">
                                    <a href="#user_akunset" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-access-point"></i></span> <span class="hidden-xs" style="color: blue !important">AKUN SETTING</span> </a> 
                                </li>
                                <li class="tab">
                                    <a href="#user_akunatasan" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-access-point"></i></span> <span class="hidden-xs" style="color: blue !important">ATASAN SETTING</span> </a> 
                                </li>
                               
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="user_detail">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">USER ID</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->USER_ID; }?>" id="user_id" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NAMA LENGKAP</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->NAMA_LGKP; }?>" id="nama_lgkp" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NAMA DEPAN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->NAMA_DPN; }?>" id="nama_dpn" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NIK</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->NIK; }?>" id="nik" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">TEMPAT LAHIR</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->TMPT_LHR; }?>" id="tmpt_lhr" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">TANGGAL LAHIR</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->TGL_LHR; }?>" id="tgl_lhr" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">JENIS KELAMIN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->JENIS_KLMIN; }?>" id="jenis_klmin" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">GOLONGAN DARAH</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->GOL_DRH; }?>" id="gol_drh" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NAMA KANTOR</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->NAMA_KANTOR; }?>" id="nama_kantor" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ALAMAT KANTOR</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->ALAMAT_KANTOR; }?>" id="alamat_kantor" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NOMOR TELEPON</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->TELP; }?>" id="telp" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ALAMAT RUMAH</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->ALAMAT_RUMAH; }?>" id="alamat_rumah" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                               <button type="button" class="btn btn-primary waves-effect waves-light m-r-10 pull-right" id="btn-back1" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                     <div class="tab-pane" id="user_wilayah">
                                        <form class="form-horizontal form-material">
                                       <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">LEVEL USER</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->GROUP_NAME; }?>" id="no_rt" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">GROUP USER</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->LEVEL_NAME; }?>" id="level_name" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">PROVINSI</label>
                                            <div class="col-md-12">
                                                <input type="text" value="(<?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>) <?php if (!empty($theuser)){echo $theuser[0]->NAMA_PROP; }?>" id="provinsi" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">KABUPATEN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="(<?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>) <?php if (!empty($theuser)){echo $theuser[0]->NAMA_KAB; }?>" id="kabupaten" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">KECAMATAN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="(<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>) <?php if (!empty($theuser)){echo $theuser[0]->NAMA_KEC; }?>" id="kecamatan" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">KELURAHAN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="(<?php if (!empty($theuser)){echo $theuser[0]->NO_KEL; }?>) <?php if (!empty($theuser)){echo $theuser[0]->NAMA_KEL; }?>" id="kelurahan" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NO RW</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->NO_RW; }?>" id="no_rw" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NO RT</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->NO_RT; }?>" id="no_rt" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                               <button type="button" class="btn btn-primary waves-effect waves-light m-r-10 pull-right" id="btn-back2" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                     <div class="tab-pane" id="user_akunsiak">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">AKUN SIAK</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->USER_SIAK; }?>" id="user_siak" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">AKUN BIOMORF CARDMANAGEMENT</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->USER_BCARD; }?>" id="user_bcard" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">AKUN BIOMORF ENROLMENT</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->USER_BENROL; }?>" id="user_benrol" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                               <button type="button" class="btn btn-primary waves-effect waves-light m-r-10 pull-right" id="btn-back3" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                     <div class="tab-pane" id="user_akunset">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">MONITORING</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->IS_MONITORING; }?>" id="is_monitoring" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">#GISA</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->IS_GISA; }?>" id="is_gisa" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ABSEN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->IS_ABSEN; }?>" id="is_absen" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ASN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->IS_ASN; }?>" id="is_asn" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">CHECK ABSENSI</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->ABSENSI_CHECKING; }?>" id="absensi_checking" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ACTIVE</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->IS_ACTIVE; }?>" id="is_active" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">CHECK IP</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->IPADDRESS_CHECK; }?>" id="ipaddress_check" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">SHOW INFO PROFIL</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->IS_SHOW; }?>" id="is_show" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                               <button type="button" class="btn btn-primary waves-effect waves-light m-r-10 pull-right" id="btn-back4" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    <div class="tab-pane" id="user_akunatasan">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ATASAN SATU</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->NAMA_SATU; }?>" id="is_atasan_satu" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ATASAN DUA</label>
                                            <div class="col-md-12">
                                               <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->NAMA_DUA; }?>" id="is_atasan_dua" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                                <button type="button" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btnadd5" onclick="on_add_user();">Add <i class="mdi  mdi-plus fa-fw"></i></button>
                                               <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back5" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>