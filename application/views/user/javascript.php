     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function edit(data) {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/User_id/Edit/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function reset(data) {
            var user_id = data;
            var default_pass ;
            $.ajax({
                        type: "post",
                        url: BASE_URL+"Shared_api/get_default_pass",
                        dataType: "json",
                        data: {

                        },
                        beforeSend:
                        function () {

                        },
                        success: function (data) {
                            console.log(data);
                           default_pass = data;
                           
                            
                        },
                        error:
                        function (data) {
                   

                        },
                        complete:
                        function (response) {
                            on_reset(user_id,default_pass)
                        }
                    });
        }
        function on_reset(user_id,pass) {
           
            swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Me-reset Password User "+user_id+" Menjadi "+pass+" ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Reset Password",   
            cancelButtonText: "Tidak, Jangan Dirubah",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/do_reset",
                    dataType: "json",
                    data: {
                        new_password : pass,
                        user_name : user_id
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Password User "+user_id+" Telah Diubah Menjadi "+pass+" !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Password User "+user_id+" Tidak Jadi Diubah :)", "error");   
                } 
            });
        }
        function hapus(user_id) {
           
            swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Menghapus User "+user_id+" ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Hapus User",   
            cancelButtonText: "Tidak, Jangan Dihapus",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/delete_user_id",
                    dataType: "json",
                    data: {
                        user_id : user_id
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "User "+user_id+" Telah Dihapus !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_select_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "User "+user_id+" Tidak Jadi Dihapus :)", "error");   
                } 
            });
        }
        function lihat(data) {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/User_id/Lihat/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function on_add() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/User_id/Add";
            var win = window.location.replace(url);
            win.focus();
        }
        function on_select(){
            get_select_table();
        }
        $("#user_id").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             submitform();
            }
            
          });

        $("#user_nm").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             submitform();
            }
            
          });

          function submitform(){
            get_select_table();
          }
        $(document).ready(function() {
           
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_level",
                    dataType: "json",
                    data: {
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdLevel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdLevel"]').empty();
                       $('select[name="kdLevel"]').append('<option value="0">-- Select level --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kdLevel"]').append('<option value="'+ value.USER_LEVEL +'">'+ value.LEVEL_NAME +'</option>');
                        });
                        $('select[name="kdLevel"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdLevel"]').attr("disabled",false);
                    }
                });
        $('select[name="kdLevel"]').on('change', function() {

            var kdLevel = $(this).val();;
            get_table(kdLevel);
        });

    });
    function get_table(kdLevel) {
        console.log(kdLevel);
        $('#level-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Setting/get_user_list",
                "type": "post",
                "data": {
                "level_id":  kdLevel,
                "user_id":  $("#user_id").val(),
                "user_nm":  $("#user_nm").val()

                }
                }
            });
    }
    function get_select_table() {
        $('#level-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Setting/get_user_list",
                "type": "post",
                "data": {
                "level_id":  $("#kdLevel").val(),
                "user_id":  $("#user_id").val(),
                "user_nm":  $("#user_nm").val()

                }
                }
            });
    }
    
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>