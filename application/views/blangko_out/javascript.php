     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
      $("#txt_pengambilan").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });

        $("#txt_rusak").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });

        $("#txt_pengembalian").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });
    $(document).ready(function() {
    
    console.log(init_kec);
    console.log(init_level);
     $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_will",
                    dataType: "json",
                    data: {
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.NO_WIL +'">'+ value.NAMA_WIL +'</option>');
                        });
                        $('select[name="no_kec"]').val(init_kec).trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec"]').attr("disabled",false);
                        get_all_data();
                    }
                });
            $('select[name="no_kec"]').on('change', function() {
                get_data();

            });

    });
    function get_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/get_data",
                    dataType: "json",
                    data: {
                        no_kec : $("#no_kec").val(),
                        tanggal : $("#tanggal").val()
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        $('#txt_pengambilan').val(data.pengambilan);
                        $('#txt_rusak').val(data.rusak);
                        $('#txt_pengembalian').val(data.pengembalian);
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function get_all_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/get_all_data",
                    dataType: "json",
                    data: {
                        tanggal : $("#tanggal").val()
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        $('#lbl_pengambilan').text(data.pengambilan);
                        $('#lbl_rusak').text(data.rusak);
                        $('#lbl_pengembalian').text(data.pengembalian);
                        $('#lbl_total').text(data.total);
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function on_clear() {
        
        $('select[name="no_kec"]').val(init_kec).trigger("change");
        $('#txt_pengambilan').val("0");
        $('#txt_rusak').val("0");
        $('#txt_pengembalian').val("0");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
    }
     function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
    function validationdaily() {
        var pengambilan = $("#txt_pengambilan");
        var rusak = $("#txt_rusak");
        var pengembalian = $("#txt_pengembalian");
        var total_kembali = parseInt(rusak.val()) + parseInt(pengembalian.val());
        console.log(total_kembali);
            if (pengambilan.val().length == 0) {                
                  swal("Warning!", "Pengembalian Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (rusak.val().length == 0) {                
                  swal("Warning!", "Keping Rusak Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (pengembalian.val().length == 0) {                
                  swal("Warning!", "Pengembalian Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if( parseInt(pengambilan.val()) < total_kembali) {
                swal("Error!", "Pengembalian Tidak Boleh Lebih Banyak Dari Pengambilan", "error"); 
             return false; 
            }
            if( parseInt(pengambilan.val()) < 0) {
                swal("Error!", "Tidak Boleh Mengambil Kurang Dari 0", "error"); 
              return false; 
            }
            if (init_kec != 0){
             console.log(init_kec);
             swal("Error!", "Hanya Operator Dinas Saja Yang Boleh Menginput Kendali Blangko", "error"); 
              return false;    
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/do_save",
                    dataType: "json",
                    data: {
                        no_kec : $("#no_kec").val(),
                        tanggal : $("#tanggal").val(),
                        pengambilan : $("#txt_pengambilan").val(),
                        rusak : $("#txt_rusak").val(),
                        pengembalian : $("#txt_pengembalian").val()
                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_all_data();
                    }
                });
        }
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-inverse',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        $('#txt_pengambilan').val("0");
        $('#txt_rusak').val("0");
        $('#txt_pengembalian').val("0");
        });
    </script>
   