<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-desktop fa-fw"></i> Absensi Operator Komputer
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>USER ID</th>
                                            <th>NAMA</th>
                                            <th>TANGGAL</th>
                                            <th>JAM MASUK</th>
                                            <th>JAM KELUAR</th>
                                            <th>KETERANGAN</th>
                                            <th>IP ADDRESS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $data['user_id']; ?></td>
                                            <td><?php echo $user_nama_lgkp; ?></td>
                                            <td><?php echo $data['tanggal']; ?></td>
                                            <td><?php echo $data['jam_masuk']; ?></td>
                                            <td><?php echo $data['jam_keluar']; ?></td>
                                            <td><?php echo $data['keterangan']; ?></td>
                                            <td><?php echo $data['ip_address']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                    <div class="panel-footer">
                    <input type="hidden" id="absen_user_id" value=<?PHP echo $data['user_id']; ?> />
                    <input type="hidden" id="absen_nama" value=<?php echo $user_nama_lgkp; ?> />
                    <input type="hidden" id="absen_tanggal" value=<?PHP echo $data['tanggal']; ?> />
                    <input type="hidden" id="absen_masuk" value=<?PHP echo $data['jam_masuk']; ?> />
                    <input type="hidden" id="absen_keluar" value=<?PHP echo $data['jam_keluar']; ?> />
                    <input type="hidden" id="absen_ket" value=<?PHP echo $data['keterangan']; ?> />
                    <input type="hidden" id="absen_ip" value=<?PHP echo $data['ip_address']; ?> /> 
                    <?php if ($data["is_ci"] == 0){ ?>
                        <button type="button" name="is_ci" class="btn btn-info btn-lg btn-block" onclick="do_ci()"><i class="fa fa-sign-in fa-1x"></i> Clock In</button>
                    <?php }else{ ?>
                        <button type="button" name="is_ci" class="btn btn-info btn-lg btn-block" onclick="on_back()"><i class="fa fa-arrow-circle-left fa-1x"></i> Kembali, Anda Tidak Dapat Clock In Lebih Dari 1 Kali</button>
                    <?php } ?>
                    
                    </div>
            
                
                </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>