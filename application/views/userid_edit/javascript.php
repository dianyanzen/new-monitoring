    <script src="<?php echo base_url()?>assets/js/gijgo.min.js"></script>
    <link href="<?php echo base_url()?>assets/css/gijgo.min.css" rel="stylesheet">
    
     <script>
        $(document).ready(function() {
            $('select[name="is_monitoring"]').empty();
            $('select[name="is_monitoring"]').append('<option value="1">YA</option>');
            $('select[name="is_monitoring"]').append('<option value="0">TIDAK</option>');
            $('select[name="is_monitoring"]').val("<?php if (!empty($theuser)){echo ($theuser[0]->IS_MONITORING == 'YA') ? '1' : '0' ;}?>").trigger("change");

            $('select[name="is_gisa"]').empty();
            $('select[name="is_gisa"]').append('<option value="1">YA</option>');
            $('select[name="is_gisa"]').append('<option value="0">TIDAK</option>');
            $('select[name="is_gisa"]').val("<?php if (!empty($theuser)){echo ($theuser[0]->IS_GISA == 'YA') ? '1' : '0' ;}?>").trigger("change");
            
            $('select[name="is_absen"]').empty();
            $('select[name="is_absen"]').append('<option value="1">YA</option>');
            $('select[name="is_absen"]').append('<option value="0">TIDAK</option>');
            $('select[name="is_absen"]').val("<?php if (!empty($theuser)){echo ($theuser[0]->IS_ABSEN == 'YA') ? '1' : '0' ;}?>").trigger("change");
            
            $('select[name="is_asn"]').empty();
            $('select[name="is_asn"]').append('<option value="1">YA</option>');
            $('select[name="is_asn"]').append('<option value="0">TIDAK</option>');
            $('select[name="is_asn"]').val("<?php if (!empty($theuser)){echo ($theuser[0]->IS_ASN == 'YA') ? '1' : '0' ;}?>").trigger("change");
            
            $('select[name="absensi_checking"]').empty();
            $('select[name="absensi_checking"]').append('<option value="1">YA</option>');
            $('select[name="absensi_checking"]').append('<option value="0">TIDAK</option>');
            $('select[name="absensi_checking"]').val("<?php if (!empty($theuser)){echo ($theuser[0]->ABSENSI_CHECKING == 'YA') ? '1' : '0' ;}?>").trigger("change");
            
            $('select[name="is_active"]').empty();
            $('select[name="is_active"]').append('<option value="1">YA</option>');
            $('select[name="is_active"]').append('<option value="0">TIDAK</option>');
            $('select[name="is_active"]').val("<?php if (!empty($theuser)){echo ($theuser[0]->IS_ACTIVE == 'YA') ? '1' : '0' ;}?>").trigger("change");
            
            $('select[name="ipaddress_check"]').empty();
            $('select[name="ipaddress_check"]').append('<option value="1">YA</option>');
            $('select[name="ipaddress_check"]').append('<option value="0">TIDAK</option>');
            $('select[name="ipaddress_check"]').val("<?php if (!empty($theuser)){echo ($theuser[0]->IPADDRESS_CHECK == 'YA') ? '1' : '0' ;}?>").trigger("change");
            
            $('select[name="is_show"]').empty();
            $('select[name="is_show"]').append('<option value="1">YA</option>');
            $('select[name="is_show"]').append('<option value="0">TIDAK</option>');
            $('select[name="is_show"]').val("<?php if (!empty($theuser)){echo ($theuser[0]->IS_SHOW == 'YA') ? '1' : '0' ;}?>").trigger("change");

            $('select[name="jenis_klmin"]').empty();
            $('select[name="jenis_klmin"]').append('<option value="1">LAKI-LAKI</option>');
            $('select[name="jenis_klmin"]').append('<option value="2">PEREMPUAN</option>');
            $('select[name="jenis_klmin"]').val("<?php if (!empty($theuser)){echo $theuser[0]->KLMN; }?>").trigger("change");

            $('select[name="gol_drh"]').empty();
            $('select[name="gol_drh"]').append('<option value="1">1 - A</option>');
            $('select[name="gol_drh"]').append('<option value="2">2 - B</option>');
            $('select[name="gol_drh"]').append('<option value="3">3 - AB</option>');
            $('select[name="gol_drh"]').append('<option value="4">4 - O</option>');
            $('select[name="gol_drh"]').append('<option value="5">5 - A+</option>');
            $('select[name="gol_drh"]').append('<option value="6">6 - A-</option>');
            $('select[name="gol_drh"]').append('<option value="7">7 - B+</option>');
            $('select[name="gol_drh"]').append('<option value="8">8 - B-</option>');
            $('select[name="gol_drh"]').append('<option value="9">9 - AB+</option>');
            $('select[name="gol_drh"]').append('<option value="10">10 - AB-</option>');
            $('select[name="gol_drh"]').append('<option value="11">11 - O+</option>');
            $('select[name="gol_drh"]').append('<option value="12">12 - O-</option>');
            $('select[name="gol_drh"]').append('<option value="13">13 - TIDAK TAHU</option>');
            $('select[name="gol_drh"]').val("<?php if (!empty($theuser)){echo $theuser[0]->GLDRH; }?>").trigger("change");
             $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_atasan",
                    dataType: "json",
                    data: {
                    },
                    beforeSend:
                    function () {
                        $('select[name="is_atasan_satu"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                        var no = 0;
                       $('select[name="is_atasan_satu"]').empty();
                        $.each(data, function(key, value) {
                            no = no+1;
                            $('select[name="is_atasan_satu"]').append('<option value="'+ value.PEJABAT_ID +'">'+ no +'. '+ value.NAMA +'</option>');
                        });
                        $('select[name="is_atasan_satu"]').val("<?php if (!empty($theuser)){echo $theuser[0]->PEJABAT_SATU; }?>").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="is_atasan_satu"]').attr("disabled",false);
                    }
                });
             $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_atasan",
                    dataType: "json",
                    data: {
                    },
                    beforeSend:
                    function () {
                        $('select[name="is_atasan_dua"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                        var no = 0;
                       $('select[name="is_atasan_dua"]').empty();
                        $.each(data, function(key, value) {
                            no = no+1;
                            $('select[name="is_atasan_dua"]').append('<option value="'+ value.PEJABAT_ID +'">'+ no +'. '+ value.NAMA +'</option>');
                        });
                        $('select[name="is_atasan_dua"]').val("<?php if (!empty($theuser)){echo $theuser[0]->PEJABAT_DUA; }?>").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="is_atasan_dua"]').attr("disabled",false);
                    }
                });
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_group",
                    dataType: "json",
                    data: {
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdgroup"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdgroup"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="kdgroup"]').append('<option value="'+ value.LEVEL_CODE +'">'+ value.LEVEL_NAME +'</option>');
                        });
                        $('select[name="kdgroup"]').val("<?php if (!empty($theuser)){echo $theuser[0]->GROUP_CODE; }?>").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdgroup"]').attr("disabled",false);
                    }
                });

            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_group_lvl",
                    dataType: "json",
                    data: {
                       lvl : <?php if (!empty($theuser)){echo $theuser[0]->GROUP_CODE; }?>
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdlvl"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdlvl"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="kdlvl"]').append('<option value="'+ value.USER_LEVEL +'">'+ value.LEVEL_NAME +'</option>');
                        });
                        $('select[name="kdlvl"]').val("<?php if (!empty($theuser)){echo $theuser[0]->USER_LEVEL; }?>").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdlvl"]').attr("disabled",false);
                    }
                });

            $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_master_provinsi",
                    dataType: "json",
                    data: {
                       no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdprop"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdprop"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="kdprop"]').append('<option value="'+ value.NO_PROP +'">('+ value.NO_PROP +') '+ value.NAMA_PROP +'</option>');
                        });
                        $('select[name="kdprop"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdprop"]').attr("disabled",false);
                    }
                });

            $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_master_kabupaten",
                    dataType: "json",
                    data: {
                       no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>,
                       no_kab : <?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>,
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdkab"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdkab"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="kdkab"]').append('<option value="'+ value.NO_KAB +'">('+ value.NO_KAB +') '+ value.NAMA_KAB +'</option>');
                        });
                        $('select[name="kdkab"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdkab"]').attr("disabled",false);
                    }
                });
            
            <?php if (!empty($theuser)){ ?>
                <?php if ($theuser[0]->GROUP_CODE == 2){ ?>
                $.ajax({
                        type: "post",
                        url: BASE_URL+"Shared_api/get_master_kecamatan",
                        dataType: "json",
                        data: {
                           no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>,
                           no_kab : <?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>,
                        },
                        beforeSend:
                        function () {
                            $('select[name="kdkec"]').attr("disabled",true);
                        },
                        success: function (data) {
                            console.log(data);
                           $('select[name="kdkec"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="kdkec"]').append('<option value="'+ value.NO_KEC +'">('+ value.NO_KEC +') '+ value.NAMA_KEC +'</option>');
                            });
                            $('select[name="kdkec"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>").trigger("change");
                            
                        },
                        error:
                        function (data) {
                   

                        },
                        complete:
                        function (response) {
                            $('select[name="kdkec"]').attr("disabled",false);
                        }
                    });
                <?php }else if ($theuser[0]->GROUP_CODE == 3){ ?>
                        $.ajax({
                        type: "post",
                        url: BASE_URL+"Shared_api/get_master_kecamatan",
                        dataType: "json",
                        data: {
                           no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>,
                           no_kab : <?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>,
                        },
                        beforeSend:
                        function () {
                            $('select[name="kdkec"]').attr("disabled",true);
                        },
                        success: function (data) {
                            console.log(data);
                           $('select[name="kdkec"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="kdkec"]').append('<option value="'+ value.NO_KEC +'">('+ value.NO_KEC +') '+ value.NAMA_KEC +'</option>');
                            });
                            $('select[name="kdkec"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>").trigger("change");
                            
                        },
                        error:
                        function (data) {
                   

                        },
                        complete:
                        function (response) {
                            $('select[name="kdkec"]').attr("disabled",false);
                        }
                    });
                <?php }else{ ?>
                     $('select[name="kdkec"]').empty();
                     $('select[name="kdkec"]').append('<option value="0">(0) -</option>');
                     $('select[name="kdkec"]').val("0").trigger("change");

                <?php } ?>
            <?php } ?>
            <?php if (!empty($theuser)){ ?>
                <?php if ($theuser[0]->GROUP_CODE == 3){ ?>
                $.ajax({
                        type: "post",
                        url: BASE_URL+"Shared_api/get_master_kelurahan",
                        dataType: "json",
                        data: {
                           no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>,
                           no_kab : <?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>,
                           no_kec : <?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>,
                        },
                        beforeSend:
                        function () {
                            $('select[name="kdkel"]').attr("disabled",true);
                        },
                        success: function (data) {
                            console.log(data);
                           $('select[name="kdkel"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="kdkel"]').append('<option value="'+ value.NO_KEL +'">('+ value.NO_KEL +') '+ value.NAMA_KEL +'</option>');
                            });
                            $('select[name="kdkel"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_KEL; }?>").trigger("change");
                            
                        },
                        error:
                        function (data) {
                   

                        },
                        complete:
                        function (response) {
                            $('select[name="kdkel"]').attr("disabled",false);
                        }
                    });
                <?php }else{ ?>
                     $('select[name="kdkel"]').empty();
                     $('select[name="kdkel"]').append('<option value="0">(0) -</option>');
                     $('select[name="kdkel"]').val("0").trigger("change");

                <?php } ?>
            <?php } ?>
            $('select[name="kdgroup"]').on('change', function() {
                var kdgroup = $(this).val();
                get_group(kdgroup);
            });
            $('select[name="kdkec"]').on('change', function() {
                var kdkec = $(this).val();
                if ( $('select[name="kdgroup"]').val() == 3){
                    get_kel(kdkec);    
                }
            });
        });

        function get_group(kdgroup){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_group_lvl",
                    dataType: "json",
                    data: {
                       lvl : kdgroup
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdlvl"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdlvl"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="kdlvl"]').append('<option value="'+ value.USER_LEVEL +'">'+ value.LEVEL_NAME +'</option>');
                            if (value.USER_LEVEL == "<?php if (!empty($theuser)){echo $theuser[0]->USER_LEVEL; }?>"){
                                $('select[name="kdlvl"]').val("<?php if (!empty($theuser)){echo $theuser[0]->USER_LEVEL; }?>").trigger("change");
                            }else if (key == 0){
                                $('select[name="kdlvl"]').val(value.USER_LEVEL).trigger("change");
                            }
                        });
                        
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdlvl"]').attr("disabled",false);
                    }
                });
            if (kdgroup == 1){
                $('select[name="kdkec"]').empty();
                $('select[name="kdkec"]').append('<option value="0">(0) -</option>');
                $('select[name="kdkec"]').val("0").trigger("change");
                $('select[name="kdkel"]').empty();
                $('select[name="kdkel"]').append('<option value="0">(0) -</option>');
                $('select[name="kdkel"]').val("0").trigger("change");
            }else if(kdgroup == 2){
                $.ajax({
                        type: "post",
                        url: BASE_URL+"Shared_api/get_master_kecamatan",
                        dataType: "json",
                        data: {
                           no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>,
                           no_kab : <?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>,
                        },
                        beforeSend:
                        function () {
                            $('select[name="kdkec"]').attr("disabled",true);
                        },
                        success: function (data) {
                            console.log(data);
                           $('select[name="kdkec"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="kdkec"]').append('<option value="'+ value.NO_KEC +'">('+ value.NO_KEC +') '+ value.NAMA_KEC +'</option>');
                                if (value.NO_KEC == "<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>"){
                                $('select[name="kdkec"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>").trigger("change");
                                }else if (key == 0){
                                    $('select[name="kdkec"]').val(value.NO_KEC).trigger("change");
                                }
                            });
                            
                        },
                        error:
                        function (data) {
                   

                        },
                        complete:
                        function (response) {
                            $('select[name="kdkec"]').attr("disabled",false);
                        }
                    });
                $('select[name="kdkel"]').empty();
                $('select[name="kdkel"]').append('<option value="0">(0) -</option>');
                $('select[name="kdkel"]').val("0").trigger("change");
            }else if(kdgroup == 3){
                $.ajax({
                        type: "post",
                        url: BASE_URL+"Shared_api/get_master_kecamatan",
                        dataType: "json",
                        data: {
                           no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>,
                           no_kab : <?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>,
                        },
                        beforeSend:
                        function () {
                            $('select[name="kdkec"]').attr("disabled",true);
                        },
                        success: function (data) {
                            console.log(data);
                           $('select[name="kdkec"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="kdkec"]').append('<option value="'+ value.NO_KEC +'">('+ value.NO_KEC +') '+ value.NAMA_KEC +'</option>');
                                if (value.NO_KEC == "<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>"){
                                $('select[name="kdkec"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>").trigger("change");
                                }else if (key == 0){
                                    $('select[name="kdkec"]').val(value.NO_KEC).trigger("change");
                                }
                            });
                            
                        },
                        error:
                        function (data) {
                   

                        },
                        complete:
                        function (response) {
                            $('select[name="kdkec"]').attr("disabled",false);
                        }
                    });
                $.ajax({
                        type: "post",
                        url: BASE_URL+"Shared_api/get_master_kelurahan",
                        dataType: "json",
                        data: {
                           no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>,
                           no_kab : <?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>,
                           no_kec : <?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>,
                        },
                        beforeSend:
                        function () {
                            $('select[name="kdkel"]').attr("disabled",true);
                        },
                        success: function (data) {
                            console.log(data);
                           $('select[name="kdkel"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="kdkel"]').append('<option value="'+ value.NO_KEL +'">('+ value.NO_KEL +') '+ value.NAMA_KEL +'</option>');
                                if (value.NO_KEL == "<?php if (!empty($theuser)){echo $theuser[0]->NO_KEL; }?>" && value.NO_KEC == "<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>"){
                                $('select[name="kdkel"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_KEL; }?>").trigger("change");
                                }else if (key == 0){
                                    $('select[name="kdkel"]').val(value.NO_KEL).trigger("change");
                                }
                            });
                            
                        },
                        error:
                        function (data) {
                   

                        },
                        complete:
                        function (response) {
                            $('select[name="kdkel"]').attr("disabled",false);
                        }
                    });

            }
        }
        function get_kel(kdkec){
            $.ajax({
                        type: "post",
                        url: BASE_URL+"Shared_api/get_master_kelurahan",
                        dataType: "json",
                        data: {
                           no_prop : <?php if (!empty($theuser)){echo $theuser[0]->NO_PROP; }?>,
                           no_kab : <?php if (!empty($theuser)){echo $theuser[0]->NO_KAB; }?>,
                           no_kec : kdkec,
                        },
                        beforeSend:
                        function () {
                            $('select[name="kdkel"]').attr("disabled",true);
                        },
                        success: function (data) {
                            console.log(data);
                           $('select[name="kdkel"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="kdkel"]').append('<option value="'+ value.NO_KEL +'">('+ value.NO_KEL +') '+ value.NAMA_KEL +'</option>');
                                if (value.NO_KEL == "<?php if (!empty($theuser)){echo $theuser[0]->NO_KEL; }?>" && value.NO_KEC == "<?php if (!empty($theuser)){echo $theuser[0]->NO_KEC; }?>"){
                                $('select[name="kdkel"]').val("<?php if (!empty($theuser)){echo $theuser[0]->NO_KEL; }?>").trigger("change");
                                }else if (key == 0){
                                    $('select[name="kdkel"]').val(value.NO_KEL).trigger("change");
                                }
                            });
                            
                        },
                        error:
                        function (data) {
                   

                        },
                        complete:
                        function (response) {
                            $('select[name="kdkel"]').attr("disabled",false);
                        }
                    });
        }
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       jQuery('#tgl_lhr').datepicker({
  
        format: 'dd-mm-yyyy',               
        autoclose: true,
        todayHighlight: true
        });
        function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/user";
            var win = window.location.replace(url);
            win.focus();
        }
        function on_edit() {
			if (validationdaily()){
              do_edit();
            }
            
        }
        function validationdaily() {
        var user_id = $("#user_id");
            if (user_id.val().length == 0) {                
                  swal("Warning!", "User Id Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var nama_lgkp = $("#nama_lgkp");
            if (nama_lgkp.val().length == 0) {                
                  swal("Warning!", "Nama Lengkap Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var nama_dpn = $("#nama_dpn");
            if (nama_dpn.val().length == 0) {                
                  swal("Warning!", "Nama Depan Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var nik = $("#nik");
            if (nik.val().length == 0) {                
                  swal("Warning!", "Nik Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var tmpt_lhr = $("#tmpt_lhr");
            if (tmpt_lhr.val().length == 0) {                
                  swal("Warning!", "Tempat Lahir Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var tmpt_lhr = $("#tmpt_lhr");
            if (tmpt_lhr.val().length == 0) {                
                  swal("Warning!", "Tempat Lahir Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var jenis_klmin = $("#jenis_klmin");
            if (jenis_klmin.val().length == 0) {                
                  swal("Warning!", "Jenis Kelamin Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var gol_drh = $("#gol_drh");
            if (gol_drh.val().length == 0) {                
                  swal("Warning!", "Golongan Darah Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var nama_kantor = $("#nama_kantor");
            if (nama_kantor.val().length == 0) {                
                  swal("Warning!", "Nama Kantor Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var alamat_kantor = $("#alamat_kantor");
            if (alamat_kantor.val().length == 0) {                
                  swal("Warning!", "Alamat Kantor Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var telp = $("#telp");
            if (telp.val().length == 0) {                
                  swal("Warning!", "Nomor Telepon Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var alamat_rumah = $("#alamat_rumah");
            if (alamat_rumah.val().length == 0) {                
                  swal("Warning!", "Alamat Rumah Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var kdgroup = $("#kdgroup");
            if (kdgroup.val().length == 0) {                
                  swal("Warning!", "Kode Wilayah Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var kdlvl = $("#kdlvl");
            if (kdlvl.val().length == 0) {                
                  swal("Warning!", "Kode Level Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var kdprop = $("#kdprop");
            if (kdprop.val().length == 0) {                
                  swal("Warning!", "Provinsi Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var kdkab = $("#kdkab");
            if (kdkab.val().length == 0) {                
                  swal("Warning!", "Kabupaten Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var kdkec = $("#kdkec");
            if (kdkec.val().length == 0) {                
                  swal("Warning!", "Kecamatan Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var kdkel = $("#kdkel");
            if (kdkel.val().length == 0) {                
                  swal("Warning!", "Kelurahan Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var no_rw = $("#no_rw");
            if (no_rw.val().length == 0) {                
                  swal("Warning!", "Nomor RW Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var no_rt = $("#no_rt");
            if (no_rt.val().length == 0) {                
                  swal("Warning!", "Nomor RT Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var is_monitoring = $("#is_monitoring");
            if (is_monitoring.val().length == 0) {                
                  swal("Warning!", "Monitoring Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var is_gisa = $("#is_gisa");
            if (is_gisa.val().length == 0) {                
                  swal("Warning!", "#Gisa Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var is_absen = $("#is_absen");
            if (is_absen.val().length == 0) {                
                  swal("Warning!", "Absen Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var is_asn = $("#is_asn");
            if (is_asn.val().length == 0) {                
                  swal("Warning!", "Asn/Non Asn Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var absensi_checking = $("#absensi_checking");
            if (absensi_checking.val().length == 0) {                
                  swal("Warning!", "Pengecekan Absensi Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var is_active = $("#is_active");
            if (is_active.val().length == 0) {                
                  swal("Warning!", "Active User Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var ipaddress_check = $("#ipaddress_check");
            if (ipaddress_check.val().length == 0) {                
                  swal("Warning!", "Pengecekan IP Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var is_show = $("#is_show");
            if (is_show.val().length == 0) {                
                  swal("Warning!", "Tampilan Master Menu Tidak Boleh Kosong !", "warning");  
                 return false;
            }
           
            return true;
        }
        function do_edit(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/edit_userid",
                    dataType: "json",
                    data: {
                        user_id : $("#user_id").val(),
                        nama_lgkp : $("#nama_lgkp").val(),
                        nama_dpn : $("#nama_dpn").val(),
                        nik : $("#nik").val(),
                        tmpt_lhr : $("#tmpt_lhr").val(),
                        tgl_lhr : $("#tgl_lhr").val(),
                        jenis_klmin : $("#jenis_klmin").val(),
                        gol_drh : $("#gol_drh").val(),
                        nama_kantor : $("#nama_kantor").val(),
                        alamat_kantor : $("#alamat_kantor").val(),
                        telp : $("#telp").val(),
                        alamat_rumah : $("#alamat_rumah").val(),
                        kdgroup : $("#kdgroup").val(),
                        kdlvl : $("#kdlvl").val(),
                        kdprop : $("#kdprop").val(),
                        kdkab : $("#kdkab").val(),
                        kdkec : $("#kdkec").val(),
                        kdkel : $("#kdkel").val(),
                        no_rw : $("#no_rw").val(),
                        no_rt : $("#no_rt").val(),
                        user_siak : $("#user_siak").val(),
                        user_bcard : $("#user_bcard").val(),
                        user_benrol : $("#user_benrol").val(),
                        is_monitoring : $("#is_monitoring").val(),
                        is_gisa : $("#is_gisa").val(),
                        is_absen : $("#is_absen").val(),
                        is_asn : $("#is_asn").val(),
                        absensi_checking : $("#absensi_checking").val(),
                        is_active : $("#is_active").val(),
                        ipaddress_check : $("#ipaddress_check").val(),
                        is_show : $("#is_show").val(),
                        is_atasan_satu : $("#is_atasan_satu").val(),
                        is_atasan_dua : $("#is_atasan_dua").val(),

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_back();
                    }
                });
        }
      function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }

    </script>