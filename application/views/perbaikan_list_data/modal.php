<button class="btn btn-primary btn-lg" id="do_aprove" data-toggle="modal" data-target="#aprove-modal" style="display: none;"></button>
<button class="btn btn-primary btn-lg" id="do_reject" data-toggle="modal" data-target="#reject-modal" style="display: none;"></button>

           <div class="modal fade" id="aprove-modal" tabindex="-1" role="dialog" aria-labelledby="aprove" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close_aprove()">&times;</button>
                      <h4 class="modal-title">Aprove Helpdesk</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Sequence</label>
                                    <div class="col-sm-10">
                                               <input class="form-control" id="aprove_seq_id" placeholder="" type="text" value="" readonly>
                                
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Nama</label>
                                    <div class="col-sm-10">
                                               <input class="form-control" id="aprove_nama" placeholder="" type="text" value="" readonly>
                                
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Pengajuan</label>
                                    <div class="col-sm-10">
                                               <input class="form-control" id="aprove_option_req" placeholder="" type="text" value="" readonly>
                                
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Keterangan</label>
                                    <div class="col-sm-10">
                                     <textarea type="text" rows="5" name="keterangan_aprove" id="keterangan_aprove" placeholder="" maxlength="300" class="form-control" style="resize: none;text-transform:uppercase !important" readonly></textarea>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Balasan</label>
                                    <div class="col-sm-10">
                                     <textarea type="text" rows="5" name="balasan_aprove" id="balasan_aprove" placeholder="" maxlength="300" class="form-control" style="resize: none;text-transform:uppercase !important"></textarea>
                                    </div>
                                  </div>



                                  <!-- Text input-->
                                 
                                  
                                     <div class="col-sm-12" id="bantuan_text">
            
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                      <button type="button" id="closemodalaprove" class="btn btn-default" data-dismiss="modal" onclick="onClearModal_aprove()">Tutup</button>
                      <button type="button" id="go_aprove" class="btn btn-success" onclick="save_aprove()">Aprove</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->

           <div class="modal fade" id="reject-modal" tabindex="-1" role="dialog" aria-labelledby="reject" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close_reject()">&times;</button>
                      <h4 class="modal-title">Reject Helpdesk</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Sequence</label>
                                    <div class="col-sm-10">
                                               <input class="form-control" id="reject_seq_id" placeholder="" type="text" value="" readonly>
                                
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Nama</label>
                                    <div class="col-sm-10">
                                               <input class="form-control" id="reject_nama" placeholder="" type="text" value="" readonly>
                                
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Pengajuan</label>
                                    <div class="col-sm-10">
                                               <input class="form-control" id="reject_option_req" placeholder="" type="text" value="" readonly>
                                
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Keterangan</label>
                                    <div class="col-sm-10">
                                     <textarea type="text" rows="5" name="keterangan_reject" id="keterangan_reject" placeholder="" maxlength="300" class="form-control" style="resize: none;text-transform:uppercase !important" readonly></textarea>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Balasan</label>
                                    <div class="col-sm-10">
                                     <textarea type="text" rows="5" name="balasan_reject" id="balasan_reject" placeholder="" maxlength="300" class="form-control" style="resize: none;text-transform:uppercase !important"></textarea>
                                    </div>
                                  </div>



                                  <!-- Text input-->
                                 
                                  
                                     <div class="col-sm-12" id="bantuan_text">
            
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                      <button type="button" id="closemodalreject" class="btn btn-default" data-dismiss="modal" onclick="onClearModal_reject()">Tutup</button>
                      <button type="button" id="go_reject" class="btn btn-danger" onclick="save_reject()">Reject</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->