<!DOCTYPE html>
<html lang="en">
<?php $this->view('shared/head'); ?>
<body class="fix-header">
    <div id="wrapper">
            <?php $this->view('shared/nav_top'); ?>
            <?php $this->view('shared/nav_side'); ?>
            <?php $this->view('perbaikan_list_data/content'); ?>
            <?php $this->view('perbaikan_list_data/modal'); ?>
    </div>
     
    <?php $this->view('shared/footer'); ?>
    
    <?php $this->view('perbaikan_list_data/javascript'); ?>
</body>
</html>