<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-12 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                               <button type="button" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-addlevel" onclick="do_refresh();">Refresh <i class="mdi mdi-sync fa-fw"></i></button>
                                  
                            </div>
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#pending" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-crosshairs-gps"></i></span> <span class="hidden-xs">Pending Request</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#aproved" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-check-circle"></i></span> <span class="hidden-xs">Success Request</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#rejected" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-close-circle"></i></span> <span class="hidden-xs">Rejected Request</span> </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="pending">
                                    <div class="steamline">
                                        <div id="request_pending">
                                            <div class="sl-item">
                                                    <div class="sl-right" >Request Pending Anda Kosong</div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="aproved">
                                    <div class="steamline">
                                        <div id="request_success">
                                            <div class="sl-item">
                                                    <div class="sl-right">Request Success Anda Kosong</div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="rejected">
                                    <div class="steamline">
                                    <div id="request_reject">
                                        <div class="sl-item">
                                                <div class="sl-right">Request Rejected Anda Kosong</div>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>