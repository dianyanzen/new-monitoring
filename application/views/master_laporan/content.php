<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url()?>">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Jenis Laporan</b></h3>
                                        <select class="form-control select2" name="mlap" id="mlap">
                                <option  value="0">-- Pilih Menu --</option>
                            </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kecamatan</b></h3>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                <option  value="0">-- Pilih Kecamatan --</option>
                            </select>
                            
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kelurahan</b></h3>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($jenis_lap)){ ?>
                <?php if ($jenis_lap == 1 || $jenis_lap == 5 || $jenis_lap == 27 || $jenis_lap == 28){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Laki Laki</th>
                                            <th width="20%" style="text-align: right;">Perempuan</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->LK ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PR ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->LK),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->PR),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 2 || $jenis_lap == 3 || $jenis_lap == 4){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">0-4 Tahun</th>
                                            <th width="20%" style="text-align: right;">5-9 Tahun</th>
                                            <th width="20%" style="text-align: right;">10-14 Tahun</th>
                                            <th width="20%" style="text-align: right;">15-19 Tahun</th>
                                            <th width="20%" style="text-align: right;">20-24 Tahun</th>
                                            <th width="20%" style="text-align: right;">25-29 Tahun</th>
                                            <th width="20%" style="text-align: right;">30-34 Tahun</th>
                                            <th width="20%" style="text-align: right;">35-39 Tahun</th>
                                            <th width="20%" style="text-align: right;">40-44 Tahun</th>
                                            <th width="20%" style="text-align: right;">45-49 Tahun</th>
                                            <th width="20%" style="text-align: right;">50-54 Tahun</th>
                                            <th width="20%" style="text-align: right;">55-59 Tahun</th>
                                            <th width="20%" style="text-align: right;">60-64 Tahun</th>
                                            <th width="20%" style="text-align: right;">65-69 Tahun</th>
                                            <th width="20%" style="text-align: right;">70-74 Tahun</th>
                                            <th width="20%" style="text-align: right;">75> Tahun</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R11 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R12 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R13 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R14 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R15 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R16 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R11),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R12),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R13),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R14),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R15),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R16),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 6 || $jenis_lap == 7 || $jenis_lap == 8){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Belum Sekolah</th>
                                            <th width="20%" style="text-align: right;">Tidak Tamat SD</th>
                                            <th width="20%" style="text-align: right;">Tamat SD</th>
                                            <th width="20%" style="text-align: right;">SLTP</th>
                                            <th width="20%" style="text-align: right;">SLTA</th>
                                            <th width="20%" style="text-align: right;">Diploma II</th>
                                            <th width="20%" style="text-align: right;">Diploma III</th>
                                            <th width="20%" style="text-align: right;">SI</th>
                                            <th width="20%" style="text-align: right;">SII</th>
                                            <th width="20%" style="text-align: right;">SIII</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 9 || $jenis_lap == 10 || $jenis_lap == 11){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Belum Tidak Bekerja</th>
                                            <th width="20%" style="text-align: right;">Mengurus Rumah Tangga</th>
                                            <th width="20%" style="text-align: right;">Pelajar/Mahasiswa</th>
                                            <th width="20%" style="text-align: right;">Aparatur Sipil Negara</th>
                                            <th width="20%" style="text-align: right;">Tni/Polri</th>
                                            <th width="20%" style="text-align: right;">Pensiunan</th>
                                            <th width="20%" style="text-align: right;">Karyawan Swasta</th>
                                            <th width="20%" style="text-align: right;">Karyawan BUMN/BUMD</th>
                                            <th width="20%" style="text-align: right;">Tenaga Medis</th>
                                            <th width="20%" style="text-align: right;">Wiraswasta</th>
                                            <th width="20%" style="text-align: right;">Pengajar (Dosen/Guru)</th>
                                            <th width="20%" style="text-align: right;">Pekerjaan Lainya</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R11 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R12 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R11),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R12),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 12 || $jenis_lap == 13 || $jenis_lap == 14){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Belum Kawin</th>
                                            <th width="20%" style="text-align: right;">Kawin</th>
                                            <th width="20%" style="text-align: right;">Cerai Hidup</th>
                                            <th width="20%" style="text-align: right;">Cerai Mati</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 15 || $jenis_lap == 16 || $jenis_lap == 17){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Islam</th>
                                            <th width="20%" style="text-align: right;">Kristen</th>
                                            <th width="20%" style="text-align: right;">Katholik</th>
                                            <th width="20%" style="text-align: right;">Hindu</th>
                                            <th width="20%" style="text-align: right;">Budha</th>
                                            <th width="20%" style="text-align: right;">Kong Huchu</th>
                                            <th width="20%" style="text-align: right;">Kepercayaan</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 18 || $jenis_lap == 19 || $jenis_lap == 20){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">A</th>
                                            <th width="20%" style="text-align: right;">B</th>
                                            <th width="20%" style="text-align: right;">AB</th>
                                            <th width="20%" style="text-align: right;">O</th>
                                            <th width="20%" style="text-align: right;">A(+)</th>
                                            <th width="20%" style="text-align: right;">A(-)</th>
                                            <th width="20%" style="text-align: right;">B(+)</th>
                                            <th width="20%" style="text-align: right;">B(-)</th>
                                            <th width="20%" style="text-align: right;">AB(+)</th>
                                            <th width="20%" style="text-align: right;">AB(-)</th>
                                            <th width="20%" style="text-align: right;">O(+)</th>
                                            <th width="20%" style="text-align: right;">O(-)</th>
                                            <th width="20%" style="text-align: right;">Tidak Tahu</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R11 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R12 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R13 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R11),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R12),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R13),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 21 || $jenis_lap == 22 || $jenis_lap == 23){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Cacat Fisik</th>
                                            <th width="20%" style="text-align: right;">Tuna Netra</th>
                                            <th width="20%" style="text-align: right;">Tuna Rungu</th>
                                            <th width="20%" style="text-align: right;">Cacat Mental</th>
                                            <th width="20%" style="text-align: right;">Cacat Fisik Mental</th>
                                            <th width="20%" style="text-align: right;">Lainnya</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 24 || $jenis_lap == 25 || $jenis_lap == 26){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">60-64 Tahun</th>
                                            <th width="20%" style="text-align: right;">65-69 Tahun</th>
                                            <th width="20%" style="text-align: right;">70-74 Tahun</th>
                                            <th width="20%" style="text-align: right;">75-79 Tahun</th>
                                            <th width="20%" style="text-align: right;">80-84 Tahun</th>
                                            <th width="20%" style="text-align: right;">85-89 Tahun</th>
                                            <th width="20%" style="text-align: right;">90-94 Tahun</th>
                                            <th width="20%" style="text-align: right;">95-99 Tahun</th>
                                            <th width="20%" style="text-align: right;">100> Tahun</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->JUMLAH ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                           <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
            <!-- /.container-fluid -->
           <?php $this->view('shared/footer_detail'); ?>
        </div>