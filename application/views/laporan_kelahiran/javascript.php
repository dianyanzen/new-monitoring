 <script>
    $(document).ready(function() {
     $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_kecamatan",
                    dataType: "json",
                    data: {
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec"]').attr("disabled",true);
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                       <?php if ($this->session->userdata(S_NO_KEC) == 0){ ?>
                       $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php }else if($akses_kec > 0){ ?>
                        $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php } ?>
                        $.each(data, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.NO_KEC +'">'+ value.NAMA_KEC +'</option>');
                        });
                        <?php if ($this->session->userdata(S_NO_KEC) == 0){ ?>
                       $('select[name="no_kec"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kec"]').val("<?php echo $this->session->userdata(S_NO_KEC); ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec"]').attr("disabled",false);
                        $('select[name="no_kel"]').attr("disabled",false);
                    }
                });
    $('select[name="no_kec"]').on('change', function() {

            var no_kec = $(this).val();

            if(no_kec != 0) {
                
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_kelurahan",
                    dataType: "json",
                    data: {
                        no_kec : no_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                          $('select[name="no_kel"]').empty();
                       <?php if ($this->session->userdata(S_NO_KEL) == 0){ ?>
                       $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                       <?php }else if($akses_kel > 0){ ?>
                        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                       <?php } ?>
                        $.each(data, function(key, value) {
                            $('select[name="no_kel"]').append('<option value="'+ value.NO_KEL +'">'+ value.NAMA_KEL +'</option>');
                        });
                        <?php if ($this->session->userdata(S_NO_KEL) == 0){ ?>
                       $('select[name="no_kel"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kel"]').val("<?php echo $this->session->userdata(S_NO_KEL); ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kel"]').attr("disabled",false);
                    }
                });
            }else{
                 $('select[name="no_kel"]').empty();
                 $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                 $('select[name="no_kel"]').val("0").trigger("change");
            }

        });

    });
    function on_clear() {
        $('#my_data').empty();
        $('#my_data').append('<tr><td colspan="4" style="text-align: center;" valign="center">No data available in table</td></td>');
        $('#my_foot').html('<tr><th width="80%" colspan="3" style="text-align: center;">Jumlah</th><th width="10%" style="text-align: right;">0</th></td>');
        $('select[name="no_kec"]').val("0").trigger("change");
        $('select[name="no_kel"]').empty();
        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
        $('select[name="no_kel"]').val("0").trigger("change");
    }
    function on_serach(){
        $('#my_data').html('<tr><td colspan="4" style="text-align: center;" valign="center">Waiting For Generate Data</td></td>');
        $('#my_foot').html('<tr><th width="80%" colspan="3" style="text-align: center;">Jumlah</th><th width="10%" style="text-align: right;">0</th></td>');
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-inverse',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
           
        });
        $('#mytable').DataTable({
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>