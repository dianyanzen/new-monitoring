    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () { get_data(); }, 5000);
    });
      function get_detail_lahir_u(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdclut'); ?>";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdcluy'); ?>";
        }
    }
    function get_detail_lahir_t(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdcltt'); ?>";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdclty'); ?>";
        }
    }
    function get_detail_kawin(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdckt'); ?>";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdcky'); ?>";
        }
    }
    function get_detail_cerai(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdcct'); ?>";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdccy'); ?>";
        }
    }
    function get_detail_mati(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdcmt'); ?>";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "<?php echo site_url('Detail/gdcmy'); ?>";
        }
    }
    function get_data() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard_capil/get_data",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#capil_nulahir').html(data.capil_nulahir);
                            $('#capil_ntlahir').html(data.capil_ntlahir);
                            $('#capil_nmati').html(data.capil_nmati);
                            $('#capil_nkawin').html(data.capil_nkawin);
                            $('#capil_ncerai').html(data.capil_ncerai);
                            $('#capil_yulahir').html(data.capil_yulahir);
                            $('#capil_ytlahir').html(data.capil_ytlahir);
                            $('#capil_ymati').html(data.capil_ymati);
                            $('#capil_ykawin').html(data.capil_ykawin);
                            $('#capil_ycerai').html(data.capil_ycerai);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   setTimeout(function () { get_data(); }, 5000);
                }
            });
        }
    </script>
    