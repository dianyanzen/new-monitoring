     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function lihat(data) {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/Helpdesk/Lihat/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function edit(data) {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/Helpdesk/Change/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function hapus(data) {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/Helpdesk/Delete/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function on_add_level() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/Helpdesk/Add";
            var win = window.location.replace(url);
            win.focus();
        }
    
        $(document).ready(function() {
           
            get_table();

    });
    function get_table() {
        $('#group-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Setting/get_user_helpdesk",
                "type": "post",
                "data": {
                
                }
                }
            });
    }
    
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>