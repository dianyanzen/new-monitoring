<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                 <div class="row">
                    <form name ="get_form" id="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik Duplicate</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" maxlength="16" onkeypress="return isNumberKey(event)"/></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik_single" type="checkbox" checked disabled="true">
                                            <label for="cb_nik_single"> Nik Single</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik_single" name="nik_single" maxlength="16" onkeypress="return isNumberKey(event)"/></div>
                                </div>
                                </div>
                            <div class="row">
                                <a class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="get_data();" >Search <i class="mdi  mdi-magnify fa-fw"></i></a>
                                  <a  class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($data) && !empty($sdata)){ ?>
                    
                            <?php if ($data[0]->KET == 'AKTIF DI SIAK' && $sdata[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                          <?php if ($data[0]->CURRENT_STATUS_CODE != 'DUPLICATE_RECORD' &&  $data[0]->CURRENT_STATUS_CODE != 'BELUM REKAM'){ ?> 
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->NIK; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                    <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Nik Duplicate Memiliki Ktp Dengan Status E-Ktp <?php echo $data[0]->CURRENT_STATUS_CODE; ?> <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                         <?php }else{ ?>
                                            <?php if ($sdata[0]->CURRENT_STATUS_CODE != 'DUPLICATE_RECORD'){ ?>
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->NIK; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                    <button class="btn btn-block btn-success btn-rounded" onclick="repair_biodata('<?php echo $data[0]->NIK; ?>','<?php echo $sdata[0]->NIK; ?>','<?php echo $data[0]->CURRENT_STATUS_CODE; ?>','<?php echo $sdata[0]->CURRENT_STATUS_CODE; ?>','<?php echo $data[0]->NAMA_LGKP; ?>','<?php echo $sdata[0]->NAMA_LGKP; ?>');" id="repair_biodata"><i class="mdi mdi-database-plus"></i> SESUAIKAN BIODATA </button>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>                                 
                                                <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->NIK; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                    <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Nik Single Tidak Dapat Di Restore Karena Status E-Ktp <?php echo $sdata[0]->CURRENT_STATUS_CODE; ?> <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                            <?php } ?>
                                         <?php } ?>

                                    <?php }else if ($data[0]->KET == 'TIDAK AKTIF DI SIAK' && $sdata[0]->KET == 'AKTIF DI SIAK'){ ?>
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->NIK; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                   <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Nik Single Sudah Aktif Di Siak <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                    <?php }else if ($data[0]->KET == 'AKTIF DI SIAK' && $sdata[0]->KET == 'AKTIF DI SIAK'){ ?>
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->NIK; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                   <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Keduanya Aktif Di Siak <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                    <?php }else if ($data[0]->KET == 'TIDAK AKTIF DI SIAK' && $sdata[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->NIK; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                   <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Keduanya Tidak Aktif Di Siak <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                    <?php }else{ ?>
                                    <?php } ?>
                                    <?php } ?>
                <?php if (!empty($data)){ ?>
                         <div class="row" id="all_content_duplicate">
                    <div class="col-lg-12" id="all_<?php echo $data[0]->NIK; ?>">
                        <div class="white-box">
                            <div class="">
                                <h1 class="m-b-0 m-t-0">Nik Duplicate</h1>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                    <div class="white-box">
                                        <?php if ($data[0]->PATH != '-'){ ?>
                                    <?php $filename = 'http://10.32.73.222:8080/Siak/'.$data[0]->PATH.'/'.$data[0]->NIK.'.jpg';
                                        if ($_SERVER['SERVER_NAME'] == '10.32.73.224') { ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo $filename; ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo $filename; ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }else{ ?>
                                                <?php if ($data[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 

                                            <?php } ?> 
                                        <?php }else{ ?>
                                            <?php if ($data[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    
                                    </div>
                                    </tr>
                                     
                                    </tbody>
                                    </table>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4><?php echo $data[0]->NAMA_LGKP; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4><?php echo $data[0]->NIK; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4><?php echo $data[0]->NO_KK; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STAT HUBKEL</h4></td>
                                                        <td> <h4><?php echo $data[0]->STAT_HBKEL; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->TMPT_LHR; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->TGL_LHR; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4><?php echo $data[0]->JENIS_KLMIN; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KECAMATAN</h4></td>
                                                        <td> <h4><?php echo $data[0]->NAMA_KEC; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KELURAHAN</h4></td>
                                                        <td> <h4><?php echo $data[0]->NAMA_KEL; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_REGIONAL' || $data[0]->CURRENT_STATUS_CODE == 'PACKET_RETRY' || $data[0]->CURRENT_STATUS_CODE == 'RECEIVED_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'PROCESSING' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_ENROLLMENT' || $data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'SEARCH_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_IN_PROCESS' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_DEDUP' || $data[0]->CURRENT_STATUS_CODE == 'DUPLICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'BELUM REKAM'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $data[0]->CURRENT_STATUS_CODE; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $data[0]->CURRENT_STATUS_CODE; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN </h4></td>
                                                    <?php if ($data[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $data[0]->KET; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $data[0]->KET; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <?php if ($data[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($pindah)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY PINDAH</h4></td>
                                                            <?php if ($pindah[0]->KLASIFIKASI_PINDAH == 'ANTAR KECAMATAN' || $pindah[0]->KLASIFIKASI_PINDAH == 'ANTAR DESA/KELURAHAN' || $pindah[0]->KLASIFIKASI_PINDAH == 'DALAM SATU DESA/KELURAHAN'){ ?>
                                                                <td> <h4 class="text-success"><?php echo $pindah[0]->NO_PINDAH; ?></h4> </td>
                                                            <td> <h4 class="text-success"><?php echo $pindah[0]->KLASIFIKASI_PINDAH; ?></h4> </
                                                               <?php }else{ ?> 
                                                                <td> <h4 class="text-danger"><?php echo $pindah[0]->NO_PINDAH; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $pindah[0]->KLASIFIKASI_PINDAH; ?></h4> </
                                                               <?php } ?> 
                                                            td>

                                                        </tr>
                                                        <tr>
                                                            <td><h4>DARI :</h4></td>
                                                            <td> <h4><?php echo $pindah[0]->DARI_NAMA_PROVINSI; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->DARI_NAMA_KABUPATEN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $pindah[0]->DARI_NAMA_KECAMATAN; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->DARI_NAMA_KELURAHAN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4>KE :</h4></td>
                                                            <td> <h4><?php echo $pindah[0]->TUJUAN_NAMA_PROVINSI; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->TUJUAN_NAMA_KABUPATEN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $pindah[0]->TUJUAN_NAMA_KECAMATAN; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->TUJUAN_NAMA_KELURAHAN; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if ($data[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($mati)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY KEMATIAN</h4></td>
                                                            <td colspan="2"> <h4 class="text-danger"><?php echo $mati[0]->KET; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                    </div>
                   
               
                <?php }else{ ?>
                <?php if (!empty($is_ada)){ ?>
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h1 class="m-b-0 m-t-0">Nik Duplicate Tidak Ditemukan</h1>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/calming-cat.gif" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/calming-cat.gif"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
                 <?php if (!empty($sdata)){ ?>
                         <div class="row" id="all_content_single">
                    <div class="col-lg-12" id="all_<?php echo $sdata[0]->NIK; ?>">
                        <div class="white-box">
                            <div class="">
                                <h1 class="m-b-0 m-t-0">Nik Single</h1>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                    <div class="white-box">
                                        <?php if ($sdata[0]->PATH != '-'){ ?>
                                    <?php $filename = 'http://10.32.73.222:8080/Siak/'.$sdata[0]->PATH.'/'.$sdata[0]->NIK.'.jpg';
                                        if ($_SERVER['SERVER_NAME'] == '10.32.73.224') { ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo $filename; ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo $filename; ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }else{ ?>
                                                <?php if ($sdata[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 

                                            <?php } ?> 
                                        <?php }else{ ?>
                                            <?php if ($sdata[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    
                                    </div>
                                    </tr>
                                    
                                    </tbody>
                                    </table>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->NAMA_LGKP; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->NIK; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->NO_KK; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STAT HUBKEL</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->STAT_HBKEL; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->TMPT_LHR; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->TGL_LHR; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->JENIS_KLMIN; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KECAMATAN</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->NAMA_KEC; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KELURAHAN</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->NAMA_KEL; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($sdata[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_REGIONAL' || $sdata[0]->CURRENT_STATUS_CODE == 'PACKET_RETRY' || $sdata[0]->CURRENT_STATUS_CODE == 'RECEIVED_AT_CENTRAL' || $sdata[0]->CURRENT_STATUS_CODE == 'PROCESSING' || $sdata[0]->CURRENT_STATUS_CODE == 'SENT_FOR_ENROLLMENT' || $sdata[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_CENTRAL' || $sdata[0]->CURRENT_STATUS_CODE == 'SEARCH_FAILURE_AT_CENTRAL' || $sdata[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_RECORD' || $sdata[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_IN_PROCESS' || $sdata[0]->CURRENT_STATUS_CODE == 'SENT_FOR_DEDUP' || $sdata[0]->CURRENT_STATUS_CODE == 'DUPLICATE_RECORD' || $sdata[0]->CURRENT_STATUS_CODE == 'BELUM REKAM'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $sdata[0]->CURRENT_STATUS_CODE; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $sdata[0]->CURRENT_STATUS_CODE; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN </h4></td>
                                                    <?php if ($sdata[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $sdata[0]->KET; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $sdata[0]->KET; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <?php if ($sdata[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($spindah)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY PINDAH</h4></td>
                                                            <?php if ($spindah[0]->KLASIFIKASI_PINDAH == 'ANTAR KECAMATAN' || $spindah[0]->KLASIFIKASI_PINDAH == 'ANTAR DESA/KELURAHAN' || $spindah[0]->KLASIFIKASI_PINDAH == 'DALAM SATU DESA/KELURAHAN'){ ?>
                                                                <td> <h4 class="text-success"><?php echo $spindah[0]->NO_PINDAH; ?></h4> </td>
                                                            <td> <h4 class="text-success"><?php echo $spindah[0]->KLASIFIKASI_PINDAH; ?></h4> </
                                                               <?php }else{ ?> 
                                                                <td> <h4 class="text-danger"><?php echo $spindah[0]->NO_PINDAH; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $spindah[0]->KLASIFIKASI_PINDAH; ?></h4> </
                                                               <?php } ?> 
                                                            td>

                                                        </tr>
                                                        <tr>
                                                            <td><h4>DARI :</h4></td>
                                                            <td> <h4><?php echo $spindah[0]->DARI_NAMA_PROVINSI; ?></h4> </td>
                                                            <td> <h4><?php echo $spindah[0]->DARI_NAMA_KABUPATEN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $spindah[0]->DARI_NAMA_KECAMATAN; ?></h4> </td>
                                                            <td> <h4><?php echo $spindah[0]->DARI_NAMA_KELURAHAN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4>KE :</h4></td>
                                                            <td> <h4><?php echo $spindah[0]->TUJUAN_NAMA_PROVINSI; ?></h4> </td>
                                                            <td> <h4><?php echo $spindah[0]->TUJUAN_NAMA_KABUPATEN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $spindah[0]->TUJUAN_NAMA_KECAMATAN; ?></h4> </td>
                                                            <td> <h4><?php echo $spindah[0]->TUJUAN_NAMA_KELURAHAN; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if ($sdata[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($smati)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY KEMATIAN</h4></td>
                                                            <td colspan="2"> <h4 class="text-danger"><?php echo $smati[0]->KET; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                    </div>
                   
               
                <?php }else{ ?>
                <?php if (!empty($is_ada)){ ?>
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h1 class="m-b-0 m-t-0">Nik Single Tidak Ditemukan</h1>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/calming-cat.gif" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/calming-cat.gif"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
                
                        

            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>