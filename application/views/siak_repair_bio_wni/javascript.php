
    <script type="text/javascript">
        var init_kec = <?php $user_no_kec = (!empty($user_no_kec)) ? $user_no_kec : 0; echo $user_no_kec;?>;
    </script>
     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $(document).ready(function($) {
        $('#nik').focus();
        $('#nik').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[^0-9,]/g, '')
            })
          })
        });
    });
   
    function on_clear() {
        $('#nik').val("");
        $('#nik_single').val("");
        $("#all_content_single").css("display", "none");
        $("#all_content_duplicate").css("display", "none");
        $("#all_content_button").css("display", "none");
    }
    function on_clear_content() {
        $("#all_content_single").css("display", "none");
        $("#all_content_duplicate").css("display", "none");
        $("#all_content_button").css("display", "none");
    }

    
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
   
    function validationdaily_getdata() {
            var nik = $("#nik");
            var nik_single = $("#nik_single");

            if (nik.val().length == 0 ) {                
                toastr.warning('Nik Duplicate Tidak Boleh Kosong');
                nik.select();
                on_clear_content();
                return false;

            }
            if (nik_single.val().length == 0) {                
                toastr.warning('Nik Single Tidak Boleh Kosong');
                nik_single.select();
                on_clear_content();
                return false;
            }
            if (nik.val().length != 16) {                
                toastr.warning('Nik Duplicate Harus 16 Digit');
                nik.select();
                on_clear_content();
                return false;
            }
            if (nik_single.val().length != 16) {                
                toastr.warning('Nik Single Harus 16 Digit');
                nik_single.select();
                on_clear_content();
                return false;
            }
            if (nik.val() == nik_single.val() ) {                
                toastr.warning('Nik Duplicate Dan Nik Single Tidak Boleh Sama');
                nik.select();
                on_clear_content();
                return false;
            }

            return true;
        }
         function repair_biodata(nik_duplicate,nik_single,status_ektp_duplicate,status_ektp_single,nama_duplicate,nama_single) {
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/repair_biodata_wni",
                    dataType: "json",
                    data: {
                        nik_duplicate : nik_duplicate,
                        nik_single : nik_single,
                        status_ektp_duplicate : status_ektp_duplicate,
                        status_ektp_single : status_ektp_single,
                        nama_duplicate : nama_duplicate,
                        nama_single : nama_single

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.success)
                        {
                            swal("Success!", data.message, "success");
                            on_clear();
                           
                        }
                        else
                        {
                            swal("Warning!", data.message, "warning");
                        }
                            
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        $('#nik').val("");
                        $('#nik_single').val("");
                    }
                });
    }
  
    function get_data(){
        if (!validationdaily_getdata()) return;
        on_serach();
        $('#get_form').submit();
         

    }
    $('#get_form').on('keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
        e.preventDefault();
           get_data();
    }
    });
    function on_serach(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
    </script>
   