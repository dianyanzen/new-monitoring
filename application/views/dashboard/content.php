 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="white-box p-b-0">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Ktp-El</h2>
                                    <h5 class="text-muted m-t-0">Tanggal <span id="date-part"></span> <span id="time-part"></span></h5>
                                </div>
                            </div>
                           
                            <div class="row minus-margin">
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-headphone-alt text-info"></i>
                                            <div>
                                                <h2><span id="perekaman_today">0</span></h2>
                                                <h4>Perekaman KTP-EL</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-home text-info"></i>
                                            <div>
                                                <h2><span id="pencetakan_today">0</span></h2>
                                                <h4>Pencetakan KTP-EL</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-credit-card text-info"></i>
                                            <div>
                                                <h2><span id="sisa_blangko">0</span></h2>
                                                <h4>Pengeluaran Blangko</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-unlink text-info"></i>
                                            <div>
                                                <h2><span id="blangko_out">0</span></h2>
                                                <h4>Sisa Blangko E-Ktp</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-paper-plane-o text-info"></i>
                                            <div>
                                                <h2><span id="sisa_prr">0</span></h2>
                                                <h4>Print Ready Record</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-shopping-cart text-info"></i>
                                            <div>
                                                <h2><span id="sisa_sfe">0</span></h2>
                                                <h4>Sent For Enrollment</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-drupal text-info"></i>
                                            <div>
                                                <h2><span id="sisa_duplicate">0</span></h2>
                                                <h4>Duplicate Record</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-print text-info"></i>
                                            <div>
                                                <h2><span id="sisa_suket">0</span></h2>
                                                <h4>PR Suket</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                                
                            </div>
                            <div class="row" style="margin-top: 10px !important">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Dafduk</h2>
                                </div>
                            </div>
                           
                            <div class="row minus-margin">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-book text-info"></i>
                                            <div>
                                                <h2><span id="pen_kk">0</span></h2>
                                                <h4>Pencetakan KK</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-book text-info"></i>
                                            <div>
                                                <h2><span id="pen_kia">0</span></h2>
                                                <h4>Pencetakan Kia</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-user text-info"></i>
                                            <div>
                                                <h2><span id="pen_nik_baru">0</span></h2>
                                                <h4>Nik Baru</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-truck text-info"></i>
                                            <div>
                                                <h2><span id="pen_pindah_akab">0</span></h2>
                                                <h4>Kepindahan Antar Kab</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-truck text-info"></i>
                                            <div>
                                                <h2><span id="pen_pindah_akec">0</span></h2>
                                                <h4>Kepindahan Antar Kec</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-truck text-info"></i>
                                            <div>
                                                <h2><span id="pen_pindah_dkec">0</span></h2>
                                                <h4>Kepindahan Dalam Kec</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-car text-info"></i>
                                            <div>
                                                <h2><span id="pen_datang_akab">0</span></h2>
                                                <h4>Kedatangan Antar Kab</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-car text-info"></i>
                                            <div>
                                                <h2><span id="pen_datang_akec">0</span></h2>
                                                <h4>Kedatangan Antar Kec</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-car text-info"></i>
                                            <div>
                                                <h2><span id="pen_datang_dkec">0</span></h2>
                                                <h4>Kedatangan Dalam Kec</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                </div>
                            <div class="row" style="margin-top: 10px !important">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Capil</h2>
                                </div>
                            </div>
                           
                            <div class="row minus-margin">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-baby-buggy text-info"></i>
                                            <div>
                                                <h2><span id="pen_lahir_lu">0</span></h2>
                                                <h4>Akta Kelahiran Umum</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-baby-buggy text-info"></i>
                                            <div>
                                                <h2><span id="pen_lahir_lt">0</span></h2>
                                                <h4>Akta Kelahiran Terlambat</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-hospital-building text-info"></i>
                                            <div>
                                                <h2><span id="pen_mati">0</span></h2>
                                                <h4>Akta Kematian</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-heart text-info"></i>
                                            <div>
                                                <h2><span id="pen_kawin">0</span></h2>
                                                <h4>Akta Perkawinan</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-heart-broken text-info"></i>
                                            <div>
                                                <h2><span id="pen_cerai">0</span></h2>
                                                <h4>Akta Perceraian</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                 
                </div>
        
            
                <!-- <div class="row">

                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6"> 
                        <div class="panel panel-danger">
                            <div class="panel-heading">Dashboard Ktp-El
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                            <img class="img-responsive" alt="user" src="<?php echo base_url()?>assets/plugins/images/disduk/b.png">
                            <button class="btn btn-danger btn-rounded waves-effect waves-light m-t-20" onclick="do_ktpl();">Lihat Aktivitas</button>
                        </div>
                        </div>
                        </div>
                    </div>
                     <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6"> 
                        <div class="panel panel-primary">
                            <div class="panel-heading">Dashboard Biodata
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                            <img class="img-responsive" alt="user" src="<?php echo base_url()?>assets/plugins/images/disduk/i.png">
                            <button class="btn btn-primary btn-rounded waves-effect waves-light m-t-20" onclick="do_bio();">Lihat Aktivitas</button>
                        </div>
                        </div>
                        </div>
                    </div>
                     <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6"> 
                        <div class="panel panel-info">
                            <div class="panel-heading">Dashboard Mobilitas
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                            <img class="img-responsive" alt="user" src="<?php echo base_url()?>assets/plugins/images/disduk/s.png">
                            <button class="btn btn-info btn-rounded waves-effect waves-light m-t-20" onclick="do_mobi();">Lihat Aktivitas</button>
                        </div>
                        </div>
                        </div>
                    </div>
                     <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6"> 
                        <div class="panel panel-warning">
                            <div class="panel-heading">Dashboard Capil
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                            <img class="img-responsive" alt="user" src="<?php echo base_url()?>assets/plugins/images/disduk/a.png">
                            <button class="btn btn-warning btn-rounded waves-effect waves-light m-t-20" onclick="do_capil();">Lihat Aktivitas</button>
                        </div>
                        </div>
                        </div>
                    </div>
                </div> -->
                 </div>
       <?php $this->view('shared/footer_detail'); ?>