    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function () {
    var interval = setInterval(function() {
        var momentNow = moment();
        $('#date-part').html(momentNow.format('DD-MM-YYYY'));
        $('#time-part').html(momentNow.format('HH:mm:ss'));
    }, 100);
        setTimeout(function () { get_data_m1(); }, 500);
    });
   function do_ktpl(){
            window.location.href = "<?php echo site_url('Dashboard_ktpel'); ?>";
    }
    function do_bio(){
            window.location.href = "<?php echo site_url('Dashboard_biodata'); ?>";
    }
    function do_mobi(){
            window.location.href = "<?php echo site_url('Dashboard_mobilitas'); ?>";
    }
    function do_capil(){
            window.location.href = "<?php echo site_url('Dashboard_capil'); ?>";
    }
    function get_data_m1() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m1",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#perekaman_today').html(data.perekaman_today);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m2(); }, 500);
                }
            });
        }
    function get_data_m2() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m2",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pencetakan_today').html(data.pencetakan_today);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m3(); }, 500);
                }
            });
        }
    function get_data_m3() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m3",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_prr').html(data.sisa_prr);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m4(); }, 500);
                }
            });
        }
    function get_data_m4() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m4",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_sfe').html(data.sisa_sfe);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m5(); }, 500);
                }
            });
        }
    function get_data_m5() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m5",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                           $('#sisa_suket').html(data.sisa_suket);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m6(); }, 500);
                }
            });
        }
    function get_data_m6() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m6",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#blangko_out').html(data.blangko_out);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m7(); }, 500);
                }
            });
        }
    function get_data_m7() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m7",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_duplicate').html(data.duplicate);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m8(); }, 500);
                }
            });
        }
    function get_data_m8() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m8",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_blangko').html(data.sisa_blangko);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_kk(); }, 500);
                }
            });
        }

      function get_data_kk() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_kk",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_kk').html(data.pen_kk);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_kia(); }, 500);
                }
            });
        }
        function get_data_kia() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_kia",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_kia').html(data.pen_kia);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_nik(); }, 500);
                }
            });
        }
        function get_data_nik() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_nik",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_nik_baru').html(data.pen_nik_baru);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_lu(); }, 500);
                }
            });
        }
        function get_data_akta_lu() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_lu",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_lahir_lu').html(data.pen_lahir_lu);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_lt(); }, 500);
                }
            });
        }
         function get_data_akta_lt() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_lt",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_lahir_lt').html(data.pen_lahir_lt);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_mt(); }, 500);
                }
            });
        }
        function get_data_akta_mt() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_mt",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_mati').html(data.pen_mati);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_kwn(); }, 500);
                }
            });
        }
        function get_data_akta_kwn() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_kwn",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_kawin').html(data.pen_kawin);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_cry(); }, 500);
                }
            });
        }
        function get_data_akta_cry() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_cry",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_cerai').html(data.pen_cerai);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_pdh_akab(); }, 500);
                }
            });
        }
        function get_data_pdh_akab() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_pdh_akab",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_pindah_akab').html(data.pen_pindah_akab);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_pdh_akec(); }, 500);
                }
            });
        }
        function get_data_pdh_akec() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_pdh_akec",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_pindah_akec').html(data.pen_pindah_akec);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_pdh_dkec(); }, 500);
                }
            });
        }

        function get_data_pdh_dkec() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_pdh_dkec",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_pindah_dkec').html(data.pen_pindah_dkec);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_dtg_akab(); }, 500);
                }
            });
        }

        function get_data_dtg_akab() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_dtg_akab",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_datang_akab').html(data.pen_datang_akab);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_dtg_akec(); }, 500);
                }
            });
        }
        function get_data_dtg_akec() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_dtg_akec",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_datang_akec').html(data.pen_datang_akec);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_dtg_dkec(); }, 500);
                }
            });
        }
        function get_data_dtg_dkec() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_dtg_dkec",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_datang_dkec').html(data.pen_datang_dkec);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m1(); }, 500);
                }
            });
        }
    </script>   
    