     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
     $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#btn-search').trigger('click');
             }
        });
        $(document).ready(function() {
            $('#antrian-list').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 10,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_cek_nohp",
                "type": "post",
                "data": {
                    "tlp":  $('#antrian_tlp').val()
                }
                }
            });

    });
    function get_table() {
        if (validationdaily()){
        block_screen();
        $('#antrian-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 'columnDefs': [
                  {
                      "targets": 0, // your case first column
                      "className": "text-center",
                      "width": "4%"
                 },
                 {
                      "targets": 1,
                      "className": "text-center",
                 },
                 {
                      "targets": 2,
                      "className": "text-center",
                 },
                 {
                      "targets": 3,
                      "className": "text-center",
                 },
                 {
                      "targets": 4,
                      "className": "text-center",
                 },
                 {
                      "targets": 5,
                      "className": "text-center",
                 }],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_cek_nohp",
                "type": "post",
                "data": {
                    "tlp":  $('#antrian_tlp').val()
                },
                complete: function() {
                    cek_exists();
                }
                }
            });
        
        unblock_screen();
        }
    }
    function cek_exists(){
        var totalRecords =  $("#antrian-list").DataTable().page.info().recordsTotal;
        if (totalRecords ==0){
            if ( $('#antrian_tlp').val().length > 3){
        swal("Warning!", "Nomor Telepon "+$('#antrian_tlp').val()+" \n Tidak Ditemukan !", "warning");                  
            }
        }
        $('#antrian_tlp').val("+62");
    }
    function validationdaily() {
        var tlp = $("#antrian_tlp");
            if (tlp.val().length <= 3) {                
                  swal("Warning!", "Nomor Telepon Tidak Boleh Kosong !", "warning");  
                  on_clear();
                 return false;
            }
            return true;
       
        }
        function on_delete(Telepon) {
            $.ajax({
                    type: "post",
                    url: "do_delete_phone",
                    dataType: "json",
                    data: {
                        tlp : Telepon
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                      if(data.is_save == 0){
                        swal("Success!", data.message, "success");  
                      }else{
                         swal("Warning!", data.message, "warning");  
                      }
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }

    function on_clear() {
        $('#antrian_tlp').val("+62");
        $('#antrian-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_cek_nohp",
                "type": "post",
                "data": {
                    "tlp":  $('#antrian_tlp').val()
                }
                }
            });
    }
    function block_screen(){
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>