
    <script type="text/javascript">
        var init_kec = <?php $user_no_kec = (!empty($user_no_kec)) ? $user_no_kec : 0; echo $user_no_kec;?>;
    </script>
     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $(document).ready(function($) {
        $('#nik').focus();
        $('#nik').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[^0-9,]/g, '')
            })
          })
        });
    });
   
    function on_clear() {
        $('#nik').val("");
        $("#all_content").css("display", "none");
        // $('select[name="biometric_option"]').val("0").trigger("change");
    }
    function on_fast_search(res_nik) {
         $("#nik").val(res_nik);
         $("#get_form").submit(); 
    }
    function restore_biodata(res_nik,res_no_kk,res_stat_hbkel) {
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/restore_biodata_wni",
                    dataType: "json",
                    data: {
                        nik : res_nik,
                        no_kk : res_no_kk,
                        stat_hbkel : res_stat_hbkel

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.success)
                        {
                            swal("Success!", data.message, "success");
                           $("#all_"+res_nik).css("display", "none");
                           refresh_kk(res_nik,res_no_kk);
                        }
                        else
                        {
                            swal("Warning!", data.message, "warning");
                        }
                            
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        $('#nik').val("");
                    }
                });
    }
    function restore_pindah_biodata(res_nik,res_no_kk,res_stat_hbkel) {
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/restore_biodata_pindah_wni",
                    dataType: "json",
                    data: {
                        nik : res_nik,
                        no_kk : res_no_kk,
                        stat_hbkel : res_stat_hbkel

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.success)
                        {
                            swal("Success!", data.message, "success");
                           $("#all_"+res_nik).css("display", "none");
                           refresh_kk(res_nik,res_no_kk);
                        }
                        else
                        {
                            swal("Warning!", data.message, "warning");
                        }
                            
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        $('#nik').val("");
                    }
                });
    }
    function delete_biodata(del_nik,del_no_kk,del_stat_hbkel) {
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/delete_biodata_wni",
                    dataType: "json",
                    data: {
                        nik : del_nik,
                        no_kk : del_no_kk

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.success)
                        {
                            swal("Success!", data.message, "success");
                           $("#all_"+del_nik).css("display", "none");
                           refresh_kk(del_nik,del_no_kk);
                        }
                        else
                        {
                            swal("Warning!", data.message, "warning");
                        }
                            
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();  
                        $('#nik').val("");
                    }
                });
    }

    function refresh_kk(res_nik,res_no_kk){
        $("#table_kk_"+res_no_kk).empty();
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/get_js_kk",
                    dataType: "json",
                    data: {
                        nik : res_nik

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                            var counter = 0;
                          $.each(data, function(key, value) {
                            counter = counter +1 ;
                            if(value.NIK == res_nik){
                                if(value.DELETED_BY == "TIDAK AKTIF DI SIAK"){
                                $("#table_kk_"+res_no_kk).append('<tr>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+counter+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NIK+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NO_KK+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NAMA_LGKP+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.STAT_HUBKEL+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.TGL_LHR+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NAMA_KECAMATAN+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NAMA_KELURAHAN+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.CURRENT_STATUS_CODE+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.FLAG_STATUS+'</h6></td>'+
                                    '<td><h6 class="text-danger" style="text-align: center;">'+value.DELETED_BY+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>Siak/restore_bio_wni?nik='+value.NIK+'"><i class="icon-magnifier"></i></a></h6></td>'+
                                    '</tr>');
                                }else{
                                    $("#table_kk_"+res_no_kk).append('<tr>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+counter+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NIK+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NO_KK+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NAMA_LGKP+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.STAT_HUBKEL+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.TGL_LHR+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NAMA_KECAMATAN+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.NAMA_KELURAHAN+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.CURRENT_STATUS_CODE+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;">'+value.FLAG_STATUS+'</h6></td>'+
                                    '<td><h6 class="text-success" style="text-align: center;">'+value.DELETED_BY+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>Siak/restore_bio_wni?nik='+value.NIK+'"><i class="icon-magnifier"></i></a></h6></td>'+
                                    '</tr>');
                                }
                            }else{
                                if(value.DELETED_BY == "TIDAK AKTIF DI SIAK"){
                                $("#table_kk_"+res_no_kk).append('<tr>'+
                                    '<td><h6 style="text-align: center;">'+counter+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NIK+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NO_KK+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NAMA_LGKP+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.STAT_HUBKEL+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.TGL_LHR+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NAMA_KECAMATAN+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NAMA_KELURAHAN+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.CURRENT_STATUS_CODE+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.FLAG_STATUS+'</h6></td>'+
                                    '<td><h6 class="text-danger" style="text-align: center;">'+value.DELETED_BY+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>Siak/restore_bio_wni?nik='+value.NIK+'"><i class="icon-magnifier"></i></a></h6></td>'+
                                    '</tr>');
                                }else{
                                    $("#table_kk_"+res_no_kk).append('<tr>'+
                                    '<td><h6 style="text-align: center;">'+counter+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NIK+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NO_KK+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NAMA_LGKP+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.STAT_HUBKEL+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.TGL_LHR+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NAMA_KECAMATAN+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.NAMA_KELURAHAN+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.CURRENT_STATUS_CODE+'</h6></td>'+
                                    '<td><h6 style="text-align: center;">'+value.FLAG_STATUS+'</h6></td>'+
                                    '<td><h6 class="text-success" style="text-align: center;">'+value.DELETED_BY+'</h6></td>'+
                                    '<td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>Siak/restore_bio_wni?nik='+value.NIK+'"><i class="icon-magnifier"></i></a></h6></td>'+
                                    '</tr>');
                                }
                            }
                            
                          });  
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        $('#nik').val("");
                    }
                });
    }
    function cek_no_kk_baru(){
    if (!validationdaily_getkk()) return;
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/cek_no_kk_baru",
                    dataType: "json",
                    data: {
                        no_kk_baru : $("#no_kk_baru").val()

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.success)
                        {
                           $("#form_kk_baru").css("display", "block");
                           $("#txt_no_kk_baru").html(data.data.no_kk);
                           $("#txt_nama_kep_baru").html(data.data.nama_kep);
                           $("#txt_no_kec_baru").html(data.data.no_kec);
                           $("#txt_no_kel_baru").html(data.data.no_kel);
                           $("#txt_rw_baru").html(data.data.rw);
                           $("#txt_rt_baru").html(data.data.rt);
                           $("#txt_alamat_baru").html(data.data.alamat);
                           // $("#ubah_kk").removeClass();
                           $("#ubah_kk").attr("disabled",false);
                           // $("#ubah_kk").css('background-color', 'green');
                           // $("#ubah_kk").addClass('btn btn-danger waves-effect waves-light');
                           // refresh_kk(del_nik,del_no_kk);
                        }
                        else
                        {
                            swal("Warning!", data.message, "warning");
                        }
                            
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();  
                        $("#no_kk_baru").val("");
                    }
                });
    }
    $("#new_kk_modal").on("hidden.bs.modal", function () {
       $("#form_kk_baru").css("display", "none");
       $("#txt_no_kk_baru").html("-");
       $("#txt_nama_kep_baru").html("-");
       $("#txt_no_kec_baru").html("-");
       $("#txt_no_kel_baru").html("-");
       $("#txt_rw_baru").html("-");
       $("#txt_rt_baru").html("-");
       $("#txt_alamat_baru").html("-");
       $("#ubah_kk").attr("disabled",true);
    });
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    $(document).ready(function() {
        $('#no_kk_baru').on('keyup',function(){
              var my_txt = $(this).val();
              var len = my_txt.length;
              if(len == 16)
              {
                  get_no_kk_paste();
                  
              }
        });
    });
    function get_no_kk_paste(){
         var no_kk = $("#no_kk_baru");
         if (no_kk.val().length != 16) {                
                  toastr.warning('No KK Harus 16 Digit');
          }else{
                cek_no_kk_baru()
          }
    }
    $('#get_form').on('keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
        e.preventDefault();
           get_data();
    }
    });
    function validationdaily_getdata() {
            var nik = $("#nik");
            var no_kk = $("#no_kk");

            if (nik.val().length == 0 && no_kk.val().length == 0) {                
                toastr.warning('Nik Dan No KK Tidak Boleh Kosong');
                nik.select();
                return false;
            }
            if (nik.val().length != 16 && no_kk.val().length == 0) {                
                toastr.warning('Nik Harus 16 Digit');
                nik.select();
                return false;
            }
            if (no_kk.val().length != 16 && nik.val().length == 0) {                
                toastr.warning('No KK Harus 16 Digit');
                nik.select();
                return false;
            }

            return true;
        }
    function validationdaily_getkk() {
            var no_kk_baru = $("#no_kk_baru");
            if (no_kk_baru.val().length == 0) {                
                toastr.warning('No KK Tidak Boleh Kosong');
                no_kk_baru.select();
                return false;
            }
            var no_kk_baru = $("#no_kk_baru");
            if (no_kk_baru.val().length != 16) {                
                toastr.warning('No KK Harus 16 Digit');
                no_kk_baru.select();
                return false;
            }

            return true;
        }
    function get_data(){
        if (!validationdaily_getdata()) return;
        on_serach();
        $('#get_form').submit();
         

    }
    function change_no_kk(res_nik){
        if ($("#txt_no_kk_lama").text().length != 16){
             swal("Warning!", "No KK Harus 16 Digit", "warning");
             return;
        }
        if ($("#txt_no_kk_baru").text() == $("#txt_no_kk_lama").text()){
             swal("Warning!", "No KK Harus Berbeda Dengan Nomer KK Lama", "warning");
             return;
        }
        if ($("#txt_no_kec_baru").text() != $("#txt_no_kec_lama").text()){
             swal("Warning!", "No KK Baru Harus Harus Satu Kecamatan Dengan Nomor KK Lama", "warning");
             return;
        }
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/change_no_kk",
                    dataType: "json",
                    data: {
                        nik : res_nik,
                        no_kk_lama : $("#txt_no_kk_lama").text(),
                        no_kec_lama : $("#txt_no_kec_lama").text(),
                        no_kel_lama : $("#txt_no_kel_lama").text(),
                        no_rw_lama : $("#txt_rw_lama").text(),
                        no_rt_lama : $("#txt_rt_lama").text(),
                        alamat_lama : $("#txt_alamat_lama").text(),
                        no_kk_baru : $("#txt_no_kk_baru").text(),
                        no_kec_baru : $("#txt_no_kec_baru").text(),
                        no_kel_baru : $("#txt_no_kel_baru").text(),
                        no_rw_baru : $("#txt_rw_baru").text(),
                        no_rt_baru : $("#txt_rt_baru").text(),
                        alamat_baru : $("#txt_alamat_baru").text()

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.success)
                        {
                            on_serach();
                           $('#nik').val(res_nik);
                           $('#get_form').submit();
                        }
                        else
                        {
                            swal("Warning!", data.message, "warning");
                        }
                            
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        // unblock_screen();  
                    }
                });
        

    }
    function on_serach(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
    </script>
   