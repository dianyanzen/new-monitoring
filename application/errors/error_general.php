<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$rootURL=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'];
$rootURL.= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

$base_url = $rootURL;
?>
<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_url;?>assets/plugins/images/pemkot.png">
<title>Sistem Monitoring Pelayanan Disduk Capil Yanzen - Dinas Kependudukan Dan Pencatatan Sipil Yanzen</title>
		<link href="<?php echo $base_url;?>assets/404/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $base_url;?>assets/404/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/404/demo/default/media/img/logo/favicon.ico" />
	</head>
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid  m-error-3" style="background-image: url(<?php echo $base_url;?>assets/404/app/media/img/error/bg3.jpg);">
				<div class="m-error_container">
					<span class="m-error_number">
						<h1>
							Error General
						</h1>
					</span>
					<p class="m-error_title m--font-light">
						We So Sorry, We Will Repair This, <a href="<?php echo $base_url;?>" class="waves-effect waves-light m-b-40">Lets Go Back</a>   
					</p>
					<p class="m-error_subtitle">
						<?php echo $heading; ?>
					</p>
					<p class="m-error_description">
						<?php echo $message; ?>
					</p>
				</div>
			</div>
		</div>
		<script src="<?php echo $base_url;?>assets/404/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="<?php echo $base_url;?>assets/404/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
	</body>
</html>


</body>
</html>
