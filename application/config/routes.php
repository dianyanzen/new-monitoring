<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Dashboard
$route['Dashboard'] = 'Dashboard';
$route['Dashboard/Userktp'] = 'Dashboard/ktpel';
$route['Dashboard/Ktpel'] = 'Dashboard_ktpel';
$route['Dashboard/Biodata'] = 'Dashboard_biodata';
$route['Dashboard/Mobilitas'] = 'Dashboard_mobilitas';
$route['Dashboard/Capil'] = 'Dashboard_capil';

// Master Menu
$route['ActivityRekap'] = 'Activity/rekap';
$route['ActivityKedatangan'] = 'Activity/kedatangan';
$route['KK/Detail'] = 'Activity/detail_kk';
$route['KK/Rekap'] = 'Activity/rekap_kk';
$route['KK/Bulanan'] = 'Activity/detail_kk_bulan';
$route['KK/Pdf_detail'] = 'Activity/do_pdf_detail_kk';
$route['Master/MyActivity'] = 'Activity';
$route['Master/Activity'] = 'Master_activity';
$route['Master/List'] = 'Master_list';
$route['Master/Request'] = 'Master_pengajuan';
$route['Master/Absensi'] = 'Master_absensi';
$route['Master/Tte'] = 'Master_tte';
$route['Master/TteKelahiran'] = 'Master_tte/kelahiran';
$route['Master/TteKematian'] = 'Master_tte/kematian';
$route['Master/TtePerpindahan'] = 'Master_tte/perpindahan';
$route['Master/Contact'] = 'Contact';
$route['Master/Pemanfaatan'] = 'Pemanfaatan_data';

// Check Ktp-el
$route['Check/Ktpel'] = 'Nik_cetak';
$route['Check/KK'] = 'Nik_kk';
$route['Check/KKRoket'] = 'Nik_kkroket';
$route['Check/KK/Push'] = 'Nik_kk/do_push';
$route['Check/Ktpel/Export'] = 'Nik_cetak/export';
$route['Check/Ktpel/Pdf'] = 'Nik_cetak/pdf';
$route['Check/Ktpel/Push'] = 'Nik_cetak/do_push';
$route['Check/History'] = 'Nik_history';
$route['Check/Duplicate'] = 'Nik_duplicate';
$route['Check/Duplicate/Export'] = 'Nik_duplicate/export';
$route['Check/Prr'] = 'Nik_prr';
$route['Check/Suket'] = 'Nik_prsuket';
$route['Check/Rekam'] = 'Nik_srekam';
$route['Check/Cetak'] = 'Nik_scetak';
$route['Check/ListDuplicate'] = 'Nik_list_duplicate';
$route['Check/Sfe'] = 'Nik_list_sfe';
$route['Check/Failure'] = 'Nik_list_failure';
$route['Check/Beginner'] = 'Nik_pemula';
$route['Check/FclPrr'] = 'Fcl_prr';
$route['Check/FclRekam'] = 'Fcl_srekam';
$route['Check/FclCetak'] = 'Fcl_scetak';
$route['Check/NotRecord'] = 'Nik_belum_rekam';


// Request
$route['Request/Tte/Capil_Waiting'] = 'Search_req/uttelhr';
$route['Request/Tte/Capil_Done'] = 'Search_req/dttelhr';
$route['Request/Tte/Dafduk_Waiting'] = 'Search_req/uttekk';
$route['Request/Tte/Dafduk_Done'] = 'Search_req/dttekk';
$route['Request/PengajuanKK/Wait'] = 'Search_req/upkk';
$route['Request/PengajuanKK/Done'] = 'Search_req/dpkk';

// Siak
$route['Siak/Restore'] = 'Siak/restore_bio_wni';
$route['Siak/Penyesuaian'] = 'Siak/repair_bio_wni';
$route['Siak/PerbaikanData'] = 'Siak/do_perbaikan_data';
$route['Siak/ListPerbaikan'] = 'Siak/list_perbaikan_data';

// Setting
$route['Setting/Helpdesk'] = 'Setting/set_helpdesk';
$route['Setting/Helpdesk/Add'] = 'Setting/set_helpdesk_add';
$route['Setting/Helpdesk/Lihat/(:any)'] = 'Setting/set_helpdesk_see/$1';
$route['Setting/Helpdesk/Change/(:any)'] = 'Setting/set_helpdesk_edit/$1';
$route['Setting/Helpdesk/Delete/(:any)'] = 'Setting/set_helpdesk_delete/$1';
$route['Setting/UserLevel'] = 'Setting/user_level';
$route['Setting/User/Add'] = 'Setting/user_add';
$route['Setting/App'] = 'Setting/seting_app';
$route['Setting/MenuUser'] = 'Setting/menu_user';
$route['Setting/MenuUser/Lihat/(:any)'] = 'Setting/see_menu_user/$1';
$route['Setting/MenuUser/Change/(:any)'] = 'Setting/edit_menu_user/$1';
$route['Setting/Pejabat'] = 'Setting/seting_pejabat';
$route['Setting/Pejabat/Add'] = 'Setting/add_pejabat';
$route['Setting/Pejabat/Edit/(:any)'] = 'Setting/edit_pejabat/$1';
$route['Setting/User/Lihat/(:any)'] = 'Setting/see_user/$1';
$route['Setting/User/Change/(:any)'] = 'Setting/change_user/$1';
$route['Setting/User/Edit/(:any)'] = 'Setting/edit_user/$1';
$route['Setting/User/Delete/(:any)'] = 'Setting/delete_user/$1';
$route['Setting/User_id/Lihat/(:any)'] = 'Setting/see_user_id/$1';
$route['Setting/User_id/Edit/(:any)'] = 'Setting/edit_user_id/$1';
$route['Setting/User_id/Add'] = 'Setting/userid_add';
$route['Setting/ActivityId'] = 'Setting/User_activity';
$route['Setting/ActivityId/Add'] = 'Setting/activity_add';
$route['Setting/ActivityId/Edit/(:any)'] = 'Setting/edit_activity/$1';

// Report
$route['Report/Absensi'] = 'Report/Absensi';
$route['Report/Rekap'] = 'Report/Rekap';
$route['Report/RekapDaily'] = 'Report/Rekap_daily';
$route['Report/Doci/(:any)'] = 'Report/Absensi_ci/$1';
$route['Report/Doco/(:any)'] = 'Report/Absensi_co/$1';

$route['Report/LaporanAbsen'] = 'Report/Laporan_absensi';
$route['Report/LaporanAbsen/Lihat/(:any)'] = 'Report/Detail_absensi/$1';
$route['Report/LaporanDaily'] = 'Report/Laporan_activity';
$route['Report/LaporanDaily/Lihat/(:any)'] = 'Report/Detail_activity/$1';
$route['Report/AbsensiBulanan'] = 'Report/Absensi_bulanan';
$route['Report/GetDaily'] = 'Report/Get_daily_harian';
$route['Report/GetDaily/Pdf'] = 'Report/do_pdf';

//Barang
$route['Barang/MasterBarang'] = 'Barang/Master_barang';
$route['Barang/MasterAset'] = 'Barang/Master_aset';
$route['Barang/InputMasuk'] = 'Barang/Master_barang_in';
$route['Barang/InputKeluar'] = 'Barang/Master_barang_out';
$route['Barang/CekBarang'] = 'Barang/Status_barang';
$route['Barang/Laporan'] = 'Barang/Laporan_barang';
$route['Barang/InputAset'] = 'Barang/Master_aset_in';
$route['Barang/MasterBarang/Edit/(:any)'] = 'Barang/edit_master_barang/$1';
$route['Barang/MasterBarang_in/Edit/(:any)'] = 'Barang/edit_master_barang_in/$1';
$route['Barang/MasterBarang_out/Edit/(:any)'] = 'Barang/edit_master_barang_out/$1';