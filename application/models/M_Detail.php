 <?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Detail extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->db2 = $this->load->database('DB2', TRUE);
			$this->db221 = $this->load->database('DB221', TRUE);
			$this->yzdb = $this->load->database('YZDB', TRUE);
		}
		
		public function get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT 
                        A.NO_KEC 
                        , A.NAMA_KEC";
            if($is_today == 1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
			if ($is_coklit == 1){
				$sql .= " , CASE WHEN B.UMUM IS NULL THEN 0 ELSE B.UMUM END AS UMUM
                        , CASE WHEN B.COKLIT IS NULL THEN 0 ELSE B.COKLIT END AS COKLIT
                		, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
			}else{
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
            }
            
            if ($is_database == '222'){
				$sql .= "  FROM SETUP_KEC A LEFT JOIN";
			}else{
				$sql .= "  FROM SETUP_KEC".$is_dblink." A LEFT JOIN";
			}
                 $sql .= "  
           			(SELECT 
					    X.NO_KEC
					    , X.NAMA_KEC";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					    , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " , to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL
					    , COUNT(1) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK 
					INNER JOIN ";
			if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON C.NO_PROP = X.NO_PROP 
					    AND C.NO_KAB = X.NO_KAB 
					    AND C.NO_KEC = X.NO_KEC ";
			}else{
				$sql .= " INNER JOIN ";
				if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON A.NO_PROP = X.NO_PROP 
					    AND A.NO_KAB = X.NO_KAB 
					    AND A.NO_KEC = X.NO_KEC ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
				$sql .= " GROUP BY 
					    X.NO_KEC
					    ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY')
					ORDER BY 
					    X.NO_KEC,X.NAMA_KEC) B ON 
                        A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
 				
 			

             if ($is_database == '222'){
             	$q = $this->db->query($sql);
             }else if($is_database == '221'){
             	$q = $this->db221->query($sql);
             }else if($is_database == '2'){
             	$q = $this->db2->query($sql);
             }else if($is_database == 'YZDB'){
             	$q = $this->yzdb->query($sql);
             }

               return $q->result();
        }
         
         function get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT  COUNT(1) AS JUMLAH";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					      , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
		    			// echo  $sql;
         //               die;
             if ($is_database == '222'){
             	$q = $this->db->query($sql);
             }else if($is_database == '221'){
             	$q = $this->db221->query($sql);
             }else if($is_database == '2'){
             	$q = $this->db2->query($sql);
             }else if($is_database == 'YZDB'){
             	$q = $this->yzdb->query($sql);
             }

               return $q->result();
        }
        public function get_detail_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT 
                        A.NO_KEC 
                        , A.NAMA_KEC";
            if($is_today == 1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
			if ($is_coklit == 1){
				$sql .= " , CASE WHEN B.UMUM IS NULL THEN 0 ELSE B.UMUM END AS UMUM
                        , CASE WHEN B.COKLIT IS NULL THEN 0 ELSE B.COKLIT END AS COKLIT
                		, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
			}else{
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
            }
            
            if ($is_database == '222'){
				$sql .= "  FROM SETUP_KEC A LEFT JOIN";
			}else{
				$sql .= "  FROM SETUP_KEC".$is_dblink." A LEFT JOIN";
			}
                 $sql .= "  
           			(SELECT 
					    X.NO_KEC
					    , X.NAMA_KEC";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					    , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " , to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL
					    , COUNT(1) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK 
					INNER JOIN ";
			if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON C.NO_PROP = X.NO_PROP 
					    AND C.NO_KAB = X.NO_KAB 
					    AND C.NO_KEC = X.NO_KEC ";
			}else{
				$sql .= " INNER JOIN ";
				if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON A.NO_PROP = X.NO_PROP 
					    AND A.NO_KAB = X.NO_KAB 
					    AND A.NO_KEC = X.NO_KEC ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
		     $sql .= " AND EXISTS(SELECT 1 FROM FACES B WHERE A.".$pk_col." =B.NIK) ";
				$sql .= " GROUP BY 
					    X.NO_KEC
					    ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY')
					ORDER BY 
					    X.NO_KEC,X.NAMA_KEC) B ON 
                        A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
 				
 			

                       // echo  $sql;
                       // die;
             if ($is_database == '222'){
             	$q = $this->db->query($sql);
             }else if($is_database == '221'){
             	$q = $this->db221->query($sql);
             }else if($is_database == '2'){
             	$q = $this->db2->query($sql);
             }else if($is_database == 'YZDB'){
             	$q = $this->yzdb->query($sql);
             }

               return $q->result();
        }
         
         function get_jumlah_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT  COUNT(1) AS JUMLAH";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					      , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
		     $sql .= " AND EXISTS(SELECT 1 FROM FACES B WHERE A.".$pk_col." =B.NIK) ";
		    			// echo  $sql;
         //               die;
             if ($is_database == '222'){
             	$q = $this->db->query($sql);
             }else if($is_database == '221'){
             	$q = $this->db221->query($sql);
             }else if($is_database == '2'){
             	$q = $this->db2->query($sql);
             }else if($is_database == 'YZDB'){
             	$q = $this->yzdb->query($sql);
             }

               return $q->result();
        }

        public function get_detail_suket($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT 
                        A.NO_KEC 
                        , A.NAMA_KEC";
            if($is_today == 1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
			if ($is_coklit == 1){
				$sql .= " , CASE WHEN B.UMUM IS NULL THEN 0 ELSE B.UMUM END AS UMUM
                        , CASE WHEN B.COKLIT IS NULL THEN 0 ELSE B.COKLIT END AS COKLIT
                		, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
			}else{
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
            }
            
            if ($is_database == '222'){
				$sql .= "  FROM SETUP_KEC A LEFT JOIN";
			}else{
				$sql .= "  FROM SETUP_KEC".$is_dblink." A LEFT JOIN";
			}
                 $sql .= "  
           			(SELECT 
					    X.NO_KEC
					    , X.NAMA_KEC";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					    , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " , to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL
					    , COUNT(DISTINCT(A.NIK)) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK 
					INNER JOIN ";
			if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON C.NO_PROP = X.NO_PROP 
					    AND C.NO_KAB = X.NO_KAB 
					    AND C.NO_KEC = X.NO_KEC ";
			}else{
				$sql .= " INNER JOIN ";
				if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON A.NO_PROP = X.NO_PROP 
					    AND A.NO_KAB = X.NO_KAB 
					    AND A.NO_KEC = X.NO_KEC ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
				$sql .= " GROUP BY 
					    X.NO_KEC
					    ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY')
					ORDER BY 
					    X.NO_KEC,X.NAMA_KEC) B ON 
                        A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
 				
 			

                       // return $sql;
                       // die;
             if ($is_database == '222'){
             	$q = $this->db->query($sql);
             }else if($is_database == '221'){
             	$q = $this->db221->query($sql);
             }else if($is_database == '2'){
             	$q = $this->db2->query($sql);
             }else if($is_database == 'YZDB'){
             	$q = $this->yzdb->query($sql);
             }

               return $q->result();
        }
         
         function get_jumlah_suket($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT  COUNT(DISTINCT(A.NIK)) AS JUMLAH";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					      , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
             if ($is_database == '222'){
             	$q = $this->db->query($sql);
             }else if($is_database == '221'){
             	$q = $this->db221->query($sql);
             }else if($is_database == '2'){
             	$q = $this->db2->query($sql);
             }else if($is_database == 'YZDB'){
             	$q = $this->yzdb->query($sql);
             }

               return $q->result();
        }

        public function get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today)
        {
        	$sql = "";
        	$sql .= "SELECT 
						A.NO_KEC 
						, A.NAMA_KEC ";
			if($is_today ==1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else{
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH 
						, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA 
					FROM SETUP_KEC A 
					LEFT JOIN 
						(
							SELECT 
								X.NO_KEC 
								, X.NAMA_KEC 
								, to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL 
								, COUNT(DISTINCT(A.".$pk_col.")) as JUMLAH
					            , COUNT(A.".$pk_col.") AS JUMLAH_ANGGOTA
								from ".$tbl_name." ON A.".$pk_col." = B.".$pk_col."
							INNER JOIN SETUP_KEC X";
							if($check_is_pindah ==1){
					$sql .= " ON A.FROM_NO_PROP = X.NO_PROP 
							AND A.FROM_NO_KAB = X.NO_KAB 
							AND A.FROM_NO_KEC = X.NO_KEC 
							WHERE 1=1 AND
					        A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB =73";
							} else{
					$sql .= " ON A.NO_PROP = X.NO_PROP 
							AND A.NO_KAB = X.NO_KAB 
							AND A.NO_KEC = X.NO_KEC 
							WHERE 1=1 AND
					        A.NO_PROP = 32 AND A.NO_KAB =73";
							}
					if($is_today ==1){
						$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
					}else if($is_today ==2){
						$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
					}
					$sql .= "
							GROUP BY X.NO_KEC ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY') 
							ORDER BY X.NO_KEC,X.NAMA_KEC) B 
							ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
							
             	$q = $this->db->query($sql);
               return $q->result();
        }
         public function get_jumlah_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today)
        {
        	$sql = "";
        	$sql .= "SELECT COUNT(DISTINCT(A.".$pk_col.")) as JUMLAH
					        , COUNT(A.".$pk_col.") AS JUMLAH_ANGGOTA
							from ".$tbl_name." ON A.".$pk_col." = B.".$pk_col."
							WHERE 1=1";
					if($is_today ==1){
						$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
					}else if($is_today ==2){
						$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
					}
					if($check_is_pindah ==1){
					$sql .= " AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB =73";
							} else{
					$sql .= " AND A.NO_PROP = 32 AND A.NO_KAB =73";
					}
             	$q = $this->db->query($sql);
               return $q->result();
        }
        	
        public function get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where = '')
        {
        	$sql = "";
        	$sql .= "SELECT 
                        A.NO_KEC 
                        , A.NAMA_KEC";
            if($is_today == 1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
            
            if ($is_database == '222'){
				$sql .= "  FROM SETUP_KEC A LEFT JOIN";
			}else{
				$sql .= "  FROM SETUP_KEC".$is_dblink." A LEFT JOIN";
			}
                 $sql .= "  
           			(SELECT 
					    X.NO_KEC
					    , X.NAMA_KEC";
				$sql .= " , to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL
					    , COUNT(1) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK 
					INNER JOIN ";
			if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON C.NO_PROP = X.NO_PROP 
					    AND C.NO_KAB = X.NO_KAB 
					    AND C.NO_KEC = X.NO_KEC ";
			}
				$sql .= "	   
					WHERE
						1=1";
			if ($is_where != ''){
				$sql .= $is_where;
			}
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
				$sql .= " GROUP BY 
					    X.NO_KEC
					    ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY')
					ORDER BY 
					    X.NO_KEC,X.NAMA_KEC) B ON 
                        A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
 				
 				
				// echo $sql;
 			// 	die;

             if ($is_database == '222'){
             	$q = $this->db->query($sql);
             }else if($is_database == '221'){
             	$q = $this->db221->query($sql);
             }else if($is_database == '2'){
             	$q = $this->db2->query($sql);
             }else if($is_database == 'YZDB'){
             	$q = $this->yzdb->query($sql);
             }

               return $q->result();
        	}

        	public function get_jumlah_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where = '')
        	{
        	$sql = "";
        	$sql .= "SELECT COUNT(1) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			if ($is_where != ''){
				$sql .= $is_where;
			}
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
 				
 				
				// echo $sql;
 			// 	die;

             if ($is_database == '222'){
             	$q = $this->db->query($sql);
             }else if($is_database == '221'){
             	$q = $this->db221->query($sql);
             }else if($is_database == '2'){
             	$q = $this->db2->query($sql);
             }else if($is_database == 'YZDB'){
             	$q = $this->yzdb->query($sql);
             }

               return $q->result();
        	}
        	 public function get_detail_capil($tbl_name,$date_col,$is_today,$get_col)
        	 {
        	 		$sql ="";
        	 		$sql .="
        	 				SELECT 
	        	 				".$get_col."
        	 				FROM 
        	 					".$tbl_name." A 
        	 				WHERE 
        	 					1=1";
 					if($is_today == 1){
						$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')";
 	 				}else{
 	 					$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')";
 	 				}
        	 		$q = $this->db->query($sql);
        	 		return $q->result();

        	 }

        	  public function get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where)
        	 {
        	 		$sql ="";
        	 		$sql .="
        	 				SELECT 
	        	 				".$get_col."
        	 				FROM 
        	 					".$tbl_name." A 
        	 				WHERE 
        	 					1=1";
 					if($is_today == 1){
						$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')";
 	 				}else{
 	 					$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')";
 	 				}
 	 					$sql .=" ".$is_where." ";
        	 		$q = $this->db->query($sql);
        	 		return $q->result();

        	 }

        	  public function get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where)
        	 {
        	 		$sql ="";
        	 		$sql .="
        	 				SELECT 
	        	 				".$get_col."
        	 				FROM 
        	 					".$tbl_name." A 
        	 				WHERE 
        	 					1=1";
 					if($is_today == 1){
						$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')";
 	 				}else{
 	 					$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')";
 	 				}
 	 					$sql .=" ".$is_where." ";
        	 		$q = $this->db221->query($sql);
        	 		return $q->result();

        	 }
        function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}