<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Laporankelahiran extends CI_Model {
		function __construct()
		{
			parent:: __construct();
		}

		public function get_data_kelahiran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel,$jns_lap,$usia_lap){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.BAYI_NO)) AS JUMLAH
              			FROM CAPIL_LAHIR A INNER JOIN SETUP_KEC C
              ON A.ADM_NO_PROV = C.NO_PROP 
              AND A.ADM_NO_KAB = C.NO_KAB 
              AND A.ADM_NO_KEC = C.NO_KEC 
                    WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D'";
          	if($jns_lap == 1){
              	$sql .= " AND A.ADM_AKTA_NO LIKE '%LU%' ";
          	}else if ($jns_lap == 2){
          		$sql .= " AND A.ADM_AKTA_NO LIKE '%LT%' ";
          	}
            if($usia_lap == 1){
                $sql .= " AND TRUNC(MONTHS_BETWEEN(A.PLPR_TGL_LAPOR,A.BAYI_TGL_LAHIR)/12) <= 18";
            }else if ($usia_lap == 2){
              $sql .= " AND TRUNC(MONTHS_BETWEEN(A.PLPR_TGL_LAPOR,A.BAYI_TGL_LAHIR)/12) > 18";
            }
        		$sql .= " AND A.PLPR_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.PLPR_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.BAYI_NO)) AS JUMLAH
              			FROM CAPIL_LAHIR A INNER JOIN SETUP_KEL C
              ON A.ADM_NO_PROV = C.NO_PROP 
              AND A.ADM_NO_KAB = C.NO_KAB 
              AND A.ADM_NO_KEC = C.NO_KEC 
              AND A.ADM_NO_KEL = C.NO_KEL
                    WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D'";
          	if($jns_lap == 1){
              	$sql .= " AND A.ADM_AKTA_NO LIKE '%LU%' ";
          	}else if ($jns_lap == 2){
          		$sql .= " AND A.ADM_AKTA_NO LIKE '%LT%' ";
          	}
            if($usia_lap == 1){
                $sql .= " AND TRUNC(MONTHS_BETWEEN(A.PLPR_TGL_LAPOR,A.BAYI_TGL_LAHIR)/12) <= 18";
            }else if ($usia_lap == 2){
              $sql .= " AND TRUNC(MONTHS_BETWEEN(A.PLPR_TGL_LAPOR,A.BAYI_TGL_LAHIR)/12) > 18";
            }
        		$sql .= " AND A.PLPR_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.PLPR_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->db->query($sql);
               return $q->result();


		}

		public function get_jumlah_kelahiran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel,$jns_lap,$usia_lap){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT COUNT(DISTINCT(A.BAYI_NO)) AS JUMLAH
              			FROM CAPIL_LAHIR A INNER JOIN SETUP_KEC C
              ON A.ADM_NO_PROV = C.NO_PROP 
              AND A.ADM_NO_KAB = C.NO_KAB 
              AND A.ADM_NO_KEC = C.NO_KEC 
                    WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D'";
          	if($jns_lap == 1){
              	$sql .= " AND A.ADM_AKTA_NO LIKE '%LU%' ";
          	}else if ($jns_lap == 2){
          		$sql .= " AND A.ADM_AKTA_NO LIKE '%LT%' ";
          	}
            if($usia_lap == 1){
                $sql .= " AND TRUNC(MONTHS_BETWEEN(A.PLPR_TGL_LAPOR,A.BAYI_TGL_LAHIR)/12) <= 18";
            }else if ($usia_lap == 2){
              $sql .= " AND TRUNC(MONTHS_BETWEEN(A.PLPR_TGL_LAPOR,A.BAYI_TGL_LAHIR)/12) > 18";
            }
        		$sql .= " AND A.PLPR_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.PLPR_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND C.NO_PROP = 32 AND C.NO_KAB = 73";	
			}else{
				$sql .="SELECT COUNT(DISTINCT(A.BAYI_NO)) AS JUMLAH
              			FROM CAPIL_LAHIR A INNER JOIN SETUP_KEL C
              ON A.ADM_NO_PROV = C.NO_PROP 
              AND A.ADM_NO_KAB = C.NO_KAB 
              AND A.ADM_NO_KEC = C.NO_KEC 
              AND A.ADM_NO_KEL = C.NO_KEL
                    WHERE 1=1 AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D'";
          	if($jns_lap == 1){
              	$sql .= " AND A.ADM_AKTA_NO LIKE '%LU%' ";
          	}else if ($jns_lap == 2){
          		$sql .= " AND A.ADM_AKTA_NO LIKE '%LT%' ";
          	}
            if($usia_lap == 1){
                $sql .= " AND TRUNC(MONTHS_BETWEEN(A.PLPR_TGL_LAPOR,A.BAYI_TGL_LAHIR)/12) <= 18";
            }else if ($usia_lap == 2){
              $sql .= " AND TRUNC(MONTHS_BETWEEN(A.PLPR_TGL_LAPOR,A.BAYI_TGL_LAHIR)/12) > 18";
            }
        		$sql .= " AND A.PLPR_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.PLPR_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
				if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." and  C.NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->db->query($sql);
               return $q->result();


		}
		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}