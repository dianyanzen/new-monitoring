<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Barang extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->yzdb = $this->load->database('YZDB', TRUE);
		}
		public function max_barang(){
            $sql = "SELECT CASE WHEN MAX(BARANG_ID) +1 IS NULL THEN 1 ELSE MAX(BARANG_ID) +1 END JML FROM SIAK_MASTER_BARANG";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
           	return $r[0]->JML;
        }
        public function max_aset($kategori){
            $sql = "SELECT CASE WHEN MAX(NO) +1 IS NULL THEN 1 ELSE MAX(NO) +1 END JML FROM SIAK_MASTER_ASET WHERE SECT = $kategori";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r[0]->JML;
        }
        public function max_barang_in(){
            $sql = "SELECT CASE WHEN MAX(SEQ_ID) +1 IS NULL THEN 1 ELSE MAX(SEQ_ID) +1 END JML FROM SIAK_BARANG_MASUK";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r[0]->JML;
        }
        public function max_barang_out(){
            $sql = "SELECT CASE WHEN MAX(SEQ_ID) +1 IS NULL THEN 1 ELSE MAX(SEQ_ID) +1 END JML FROM SIAK_BARANG_KELUAR";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r[0]->JML;
        }
        public function get_master_barang(){
            $sql = "SELECT BARANG_ID, JENIS_BARANG, SATUAN, HARGA, KETERANGAN FROM SIAK_MASTER_BARANG ORDER BY BARANG_ID";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_master_aset($kategori){
            $sql = "SELECT NO, DESCRIP, SECT FROM SIAK_MASTER_ASET WHERE SECT = $kategori ORDER BY NO";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_master_barang_status(){
            $sql = "SELECT 
                        A.BARANG_ID
                        , A.JENIS_BARANG
                        , A.SATUAN
                        , A.HARGA
                        , CONCAT((CASE WHEN B.JML IS NULL THEN 0 ELSE B.JML END),CONCAT(' ',A.SATUAN)) JML_MASUK
                        , CONCAT((CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END),CONCAT(' ',A.SATUAN)) JML_KELUAR 
                        , CONCAT((CASE WHEN B.JML IS NULL THEN 0 ELSE B.JML END - CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END),CONCAT(' ',A.SATUAN)) SALDO
                        , ((CASE WHEN B.JML IS NULL THEN 0 ELSE B.JML END - CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END) * A.HARGA) HARGA_ASET
                    FROM SIAK_MASTER_BARANG A 
                    LEFT JOIN (SELECT KODE_BARANG, SUM(JUMLAH) JML FROM SIAK_BARANG_MASUK GROUP BY KODE_BARANG) B ON A.BARANG_ID = B.KODE_BARANG
                    LEFT JOIN (SELECT KODE_BARANG, SUM(JUMLAH) JML FROM SIAK_BARANG_KELUAR GROUP BY KODE_BARANG) C ON A.BARANG_ID = C.KODE_BARANG
                    ORDER BY BARANG_ID";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_master_barang_laporan($tgl_start,$tgl_end,$barang){
            $sql = "";
            $sql .= "SELECT SEQ_ID, JENIS_BARANG, KODE_BARANG, CONCAT(JUMLAH,CONCAT(' ',SATUAN)) JUMLAH, HARGA, SATUAN,TO_CHAR(TANGGAL,'DD-MM-YYYY') TANGGAL, RECEIVED  FROM 
                    (SELECT A.SEQ_ID,B.JENIS_BARANG,A.KODE_BARANG, A.JUMLAH, B.SATUAN, (A.JUMLAH * B.HARGA) HARGA, A.CREATED_DT TANGGAL, A.RECEIVED 
                    FROM SIAK_BARANG_MASUK A LEFT JOIN SIAK_MASTER_BARANG B ON A.KODE_BARANG = B.BARANG_ID 
                    UNION ALL
                    SELECT A.SEQ_ID,B.JENIS_BARANG,A.KODE_BARANG, A.JUMLAH, B.SATUAN, (A.JUMLAH * B.HARGA) HARGA, A.CREATED_DT TANGGAL, A.RECEIVED 
                    FROM SIAK_BARANG_KELUAR A LEFT JOIN SIAK_MASTER_BARANG B ON A.KODE_BARANG = B.BARANG_ID) 
                    WHERE 1=1 AND TANGGAL >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND TANGGAL < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1 ";
            if ($barang != 0){
                $sql .= "AND KODE_BARANG = $barang";
            }
            $sql .= "ORDER BY TANGGAL";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_master_barang_in(){
            $sql = "SELECT A.SEQ_ID,B.JENIS_BARANG, CONCAT(A.JUMLAH,CONCAT(' ',B.SATUAN)) JUMLAH, (A.JUMLAH * B.HARGA) HARGA, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, A.RECEIVED FROM SIAK_BARANG_MASUK A LEFT JOIN SIAK_MASTER_BARANG B ON A.KODE_BARANG = B.BARANG_ID ORDER BY A.CREATED_DT DESC";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_master_barang_out(){
            $sql = "SELECT A.SEQ_ID,B.JENIS_BARANG, CONCAT(A.JUMLAH,CONCAT(' ',B.SATUAN)) JUMLAH, (A.JUMLAH * B.HARGA) HARGA, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, A.RECEIVED FROM SIAK_BARANG_KELUAR A LEFT JOIN SIAK_MASTER_BARANG B ON A.KODE_BARANG = B.BARANG_ID ORDER BY A.CREATED_DT DESC";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_data_barang($barang_id){
            $sql = "SELECT BARANG_ID, JENIS_BARANG, SATUAN, HARGA, KETERANGAN, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL FROM SIAK_MASTER_BARANG WHERE BARANG_ID = $barang_id";
            $q = $this->yzdb->query($sql);
      		return $q->result();
        }
        public function aset_select_data($aset_no,$aset_sect){
            $sql = "SELECT DESCRIP FROM SIAK_MASTER_ASET WHERE NO = $aset_no AND SECT = $aset_sect";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r[0]->DESCRIP;
        }
        public function get_data_barang_in($barang_id){
            $sql = "SELECT SEQ_ID, KODE_BARANG, JUMLAH, RECEIVED, KETERANGAN, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL FROM SIAK_BARANG_MASUK WHERE SEQ_ID = $barang_id";
            $q = $this->yzdb->query($sql);
            return $q->result();
        }     
        public function get_data_barang_out($barang_id){
            $sql = "SELECT SEQ_ID, KODE_BARANG, JUMLAH, RECEIVED, KETERANGAN, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL FROM SIAK_BARANG_KELUAR WHERE SEQ_ID = $barang_id";
            $q = $this->yzdb->query($sql);
            return $q->result();
        }
        public function save_barang($barang_id,$jenis_barang,$satuan,$harga_satuan,$keterangan,$user_id){
          $sql = "INSERT INTO SIAK_MASTER_BARANG (BARANG_ID ,JENIS_BARANG ,SATUAN ,HARGA ,KETERANGAN ,CREATED_BY ,CREATED_DT) VALUES ($barang_id,'$jenis_barang','$satuan',$harga_satuan,'$keterangan','$user_id',SYSDATE)";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function save_aset($barang_id,$sect,$descrip){
          $sql = "INSERT INTO SIAK_MASTER_ASET (NO ,DESCRIP ,SECT) VALUES ($barang_id,'$descrip',$sect)";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function save_barang_in($barang_id,$kd_brg,$received,$jumlah,$keterangan,$user_id){
          $sql = "INSERT INTO SIAK_BARANG_MASUK (SEQ_ID ,KODE_BARANG ,RECEIVED ,JUMLAH ,KETERANGAN ,CREATED_BY ,CREATED_DT) VALUES ($barang_id,$kd_brg,'$received',$jumlah,'$keterangan','$user_id',SYSDATE)";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function save_barang_out($barang_id,$kd_brg,$received,$jumlah,$keterangan,$user_id){
          $sql = "INSERT INTO SIAK_BARANG_KELUAR (SEQ_ID ,KODE_BARANG ,RECEIVED ,JUMLAH ,KETERANGAN ,CREATED_BY ,CREATED_DT) VALUES ($barang_id,$kd_brg,'$received',$jumlah,'$keterangan','$user_id',SYSDATE)";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function edit_aset($mdl_no_data,$mdl_value_data,$mdl_sect_data){
          $sql = "UPDATE SIAK_MASTER_ASET 
                       SET 
                       DESCRIP = '$mdl_value_data'
                     WHERE NO = $mdl_no_data AND SECT = $mdl_sect_data";

            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function edit_barang($barang_id,$jenis_barang,$satuan,$harga_satuan,$keterangan,$user_id){
          $sql = "UPDATE SIAK_MASTER_BARANG 
                       SET 
                       JENIS_BARANG = '$jenis_barang'
                       , SATUAN = '$satuan'
                       , HARGA = $harga_satuan
                       , KETERANGAN = '$keterangan' 
                       , CHANGED_BY = '$user_id' 
                       , CHANGED_DT = SYSDATE 
                     WHERE BARANG_ID = $barang_id";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function edit_barang_in($kode_barang,$kd_brg,$received,$jumlah,$keterangan,$user_id){
          $sql = "UPDATE SIAK_BARANG_MASUK 
                       SET 
                       KODE_BARANG = '$kd_brg'
                       , RECEIVED = '$received'
                       , JUMLAH = $jumlah
                       , KETERANGAN = '$keterangan' 
                       , CREATED_BY = '$user_id' 
                       , CREATED_DT = SYSDATE 
                     WHERE SEQ_ID = $kode_barang";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function edit_barang_out($kode_barang,$kd_brg,$received,$jumlah,$keterangan,$user_id){
          $sql = "UPDATE SIAK_BARANG_KELUAR
                       SET 
                       KODE_BARANG = '$kd_brg'
                       , RECEIVED = '$received'
                       , JUMLAH = $jumlah
                       , KETERANGAN = '$keterangan' 
                       , CREATED_BY = '$user_id' 
                       , CREATED_DT = SYSDATE 
                     WHERE SEQ_ID = $kode_barang";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function delete_master_barang($barang_id){
          $sql = "DELETE FROM SIAK_MASTER_BARANG
                     WHERE BARANG_ID = $barang_id";
            $q = $this->yzdb->query($sql);
            $sql = "DELETE FROM SIAK_BARANG_MASUK
                     WHERE KODE_BARANG = $barang_id";
            $q = $this->yzdb->query($sql);
            $sql = "DELETE FROM SIAK_BARANG_KELUAR
                     WHERE KODE_BARANG = $barang_id";
            $q = $this->yzdb->query($sql);
            return $q;
        }

        public function delete_master_in_barang($barang_id){
          $sql = "DELETE FROM SIAK_BARANG_MASUK
                     WHERE SEQ_ID = $barang_id";
            $q = $this->yzdb->query($sql);
            return $q;
        }

        public function delete_master_out_barang($barang_id){
          $sql = "DELETE FROM SIAK_BARANG_KELUAR
                     WHERE SEQ_ID = $barang_id";
            $q = $this->yzdb->query($sql);
            return $q;
        }

        public function delete_master_aset($barang_id,$sect){
          $sql = "DELETE FROM SIAK_MASTER_ASET
                     WHERE NO = $barang_id AND SECT = $sect";
            $q = $this->yzdb->query($sql);
            return $q;
        }

		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}