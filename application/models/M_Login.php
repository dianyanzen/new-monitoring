<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Login extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->yzdb = $this->load->database('YZDB', TRUE);
		}

		public function check_user($user_id)
        {
        	 
             	$sql = "SELECT USER_ID, NAMA_LGKP, NIK, TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') AS TGL_LHR, JENIS_KLMIN, GOL_DRH, PENDIDIKAN, PEKERJAAN, NAMA_KANTOR, ALAMAT_KANTOR, TELP, ALAMAT_RUMAH, USER_LEVEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL,NO_RW,NO_RT, NAMA_DPN, USER_PWD, IS_ASN,IPADDRESS_CHECK FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id' ";
        	 	$q = $this->yzdb->query($sql);
             	$r = $q->result();

             	return $r;
        }
        public function check_ip($ip_address)
        {
             
                $sql = "SELECT IP_ADDRESS FROM SIAK_IPADDRESS WHERE IP_ADDRESS = '$ip_address' ";
                $q = $this->yzdb->query($sql);
                $r = $q->result();
                return $r;
        }
        public function check_user_gisa($user_id)
        {
             
                $sql = "SELECT USER_ID, NAMA_LGKP, NIK, TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') AS TGL_LHR, JENIS_KLMIN, GOL_DRH, PENDIDIKAN, PEKERJAAN, NAMA_KANTOR, ALAMAT_KANTOR, TELP, ALAMAT_RUMAH, USER_LEVEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, NAMA_DPN, USER_PWD FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id'  AND IS_GISA =1";
                $q = $this->yzdb->query($sql);
                $r = $q->result();

                return $r;
        }

        public function check_gisa_token($token)
        {
                $sql = "SELECT USER_ID, FLAG_STATUS, ANDROID_TOKEN, ANDROID_DEVICE, ANDROID_REALISE, ANDROID_SDK, ANDROID_MANUFACTUR, ANDROID_MODEL, CREATED_BY, CREATED_DT, CHANGED_BY, CHANGED_DT FROM SIAK_GISA_SESSIONS WHERE ANDROID_TOKEN = '$token'";
                $q = $this->yzdb->query($sql);
                $r = $q->result();

                return $r;
        }
        public function change_pwd($user_id, $new_password)
        {
                $sql = "UPDATE SIAK_USER_PLUS SET USER_PWD = '$new_password'  WHERE USER_ID = '$user_id'";
                $this->yzdb->query($sql);
        }
        public function save_gisa_token($user_id, $token, $flag, $device, $release, $version, $manufactur, $model)
        {
                $sql = "INSERT INTO SIAK_GISA_SESSIONS (USER_ID, FLAG_STATUS, ANDROID_TOKEN, ANDROID_DEVICE, ANDROID_REALISE, ANDROID_SDK, ANDROID_MANUFACTUR, ANDROID_MODEL, CREATED_BY, CREATED_DT) VALUES ('$user_id', $flag, '$token', '$device', '$release', '$version', '$manufactur', '$model', '$user_id', SYSDATE)";
                $this->yzdb->query($sql);
        }
        public function update_gisa_token($user_id, $token, $flag, $device, $release, $version, $manufactur, $model)
        {
                $sql = "UPDATE SIAK_GISA_SESSIONS SET USER_ID = '$user_id', FLAG_STATUS= $flag, ANDROID_DEVICE='$device', ANDROID_REALISE = '$release', ANDROID_SDK = '$version', ANDROID_MANUFACTUR = '$manufactur', ANDROID_MODEL = '$model', CHANGED_BY = '$user_id', CHANGED_DT = SYSDATE  WHERE ANDROID_TOKEN = '$token'";
                $this->yzdb->query($sql);
        }

        public function last_activity($user_id,$session_id,$ip_address,$user_agent){
            $sql = "SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'";
            $q = $this->yzdb->query($sql);
             $r = (int) $q->row()->CNT;
             if($r > 0){
                $sql = "UPDATE SIAK_SESSION_PLUS SET SESSION_ID = '$session_id', IP_ADDRESS= '$ip_address', USER_AGENT='$user_agent', LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 1  WHERE USER_ID = '$user_id'";
                $this->yzdb->query($sql);
             }else{
                $sql = "INSERT INTO SIAK_SESSION_PLUS (SESSION_ID, IP_ADDRESS, USER_AGENT, LAST_ACTIVITY, USER_ID, IS_ACTIVE) VALUES ('$session_id','$ip_address','$user_agent',SYSDATE,'$user_id',1)";
                $this->yzdb->query($sql);
             }
             
        } 
        public function save_token($user_id,$token){
            $sql = "SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'";
            $q = $this->yzdb->query($sql);
             $r = (int) $q->row()->CNT;
             if($r > 0){
                $sql = "UPDATE SIAK_SESSION_PLUS SET API_TOKEN = '$token', LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 1  WHERE USER_ID = '$user_id'";
                $this->yzdb->query($sql);
             }
             
        } 
        public function stop_activity($user_id){
        	$sql = "SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'";
			$q = $this->yzdb->query($sql);
             $r = (int) $q->row()->CNT;
             if($r > 0){
                $sql = "UPDATE SIAK_SESSION_PLUS SET LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 0  WHERE USER_ID = '$user_id'";
                $this->yzdb->query($sql);
             }
             
        }
}
