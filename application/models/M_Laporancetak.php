<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Laporancetak extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->db2 = $this->load->database('DB2', TRUE);
		}
		
		public function get_data_cetak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC@DB222 A LEFT JOIN
					(select 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(B.NIK) AS JUMLAH FROM CARD_MANAGEMENT B 
					    INNER JOIN DEMOGRAPHICS A 
					    ON A.NIK =  B.NIK INNER JOIN SETUP_KEC@DB222 C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    WHERE 
					    CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND CASE WHEN UPPER(B.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(B.CREATED_USERNAME) ELSE UPPER(B.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB@YZDB WHERE IS_ACTIVE = 1)
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
						FROM SETUP_KEL@DB222 A LEFT JOIN
						(select 
						    C.NO_KEL
						    , C.NAMA_KEL
						    , COUNT(B.NIK) AS JUMLAH FROM CARD_MANAGEMENT B 
						    INNER JOIN DEMOGRAPHICS A 
						    ON A.NIK =  B.NIK INNER JOIN SETUP_KEL@DB222 C
						    ON A.NO_PROP = C.NO_PROP 
						    AND A.NO_KAB = C.NO_KAB 
						    AND A.NO_KEC = C.NO_KEC 
						    AND A.NO_KEL = C.NO_KEL 
						    WHERE 
						   CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND CASE WHEN UPPER(B.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(B.CREATED_USERNAME) ELSE UPPER(B.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB@YZDB WHERE IS_ACTIVE = 1)";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				
				$q = $this->db2->query($sql);
               return $q->result();


		}

		public function get_jumlah_cetak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT COUNT(B.NIK) AS JUMLAH FROM CARD_MANAGEMENT B 
					    INNER JOIN DEMOGRAPHICS A ON A.NIK = B.NIK WHERE 
					    CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND CASE WHEN UPPER(B.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(B.CREATED_USERNAME) ELSE UPPER(B.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB@YZDB WHERE IS_ACTIVE = 1)
					    AND A.NO_PROP = 32 AND A.NO_KAB = 73";	
			}else{
				$sql .="SELECT COUNT(B.NIK) AS JUMLAH FROM CARD_MANAGEMENT B 
						INNER JOIN DEMOGRAPHICS A ON A.NIK =  B.NIK WHERE 
						CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND CASE WHEN UPPER(B.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(B.CREATED_USERNAME) ELSE UPPER(B.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB@YZDB WHERE IS_ACTIVE = 1) ";
						if($no_kel == 0){    
					$sql .=" AND A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->db2->query($sql);
               return $q->result();


		}
		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}