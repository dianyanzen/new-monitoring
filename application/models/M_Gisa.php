

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Gisa extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->yzdb = $this->load->database('YZDB', TRUE);
		}
		public function get_kk($no_kec = 0,$no_kel =0,$no_rw = 0,$no_rt = 0){
			$sql = "SELECT A.NO_KK, A.NAMA_LGKP AS NAMA_KEP, A.ALAMAT, A.RW NO_RW,A.RT NO_RT, A.NAMA_KEC, A.NAMA_KEL FROM KAMPUNG_GISA_DKB A WHERE A.NO_KEC = $no_kec AND A.NO_KEL =$no_kel AND A.RW = $no_rw AND A.RT = $no_rt AND A.STAT_HBKEL = 'KEPALA KELUARGA' ORDER BY A.NAMA_LGKP";
		
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}
		public function get_count_kk($no_kec = 0,$no_kel =0,$no_rw = 0,$no_rt = 0){
			$sql = "SELECT COUNT(1) AS JML FROM KAMPUNG_GISA_DKB A WHERE A.NO_KEC = $no_kec AND A.NO_KEL =$no_kel AND A.RW = $no_rw AND A.RT = $no_rt AND A.STAT_HBKEL = 'KEPALA KELUARGA'";
			$q = $this->yzdb->query($sql);
			return $q->row(0)->JML;
		}
		public function export_gisa($no_kk = ''){
			$sql = "";
			$sql .= "SELECT A.NO_KK, A.NAMA_LGKP AS NAMA_KEP, A.ALAMAT, A.RW NO_RW,A.RT NO_RT, A.NAMA_KEC,A.STATUS_KAWIN AS STAT_KWN, A.NAMA_KEL FROM KAMPUNG_GISA_DKB A WHERE A.STAT_HBKEL = 'KEPALA KELUARGA' ";
			if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
				$sql .= " AND A.NO_KK IN ($no_kk) ";
			} 
			$sql .= " ORDER BY A.NAMA_LGKP ";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_count_detail_kk($no_kk){
			$sql = "SELECT COUNT(1) AS JML FROM KAMPUNG_GISA_DKB A WHERE A.NO_KK = $no_kk";
			$q = $this->yzdb->query($sql);
			return $q->row(0)->JML;
		}
		public function get_detail_kk($no_kk){
			$sql = "SELECT 
					  A.NO_KK
					  , A.ALAMAT
					  , A.RT AS NO_RT 
					  , A.RW AS NO_RW
					  , A.NO_KEC 
					  , A.NAMA_KEC 
					  , A.NO_KEL 
					  , A.NAMA_KEL
					  , 73 AS NO_KAB 
					  , '".$this->get_nm_kab()."' AS NAMA_KAB 
					  , 32 AS NO_PROP 
					  , '".$this->get_nm_prop()."' AS NAMA_PROP
					  FROM KAMPUNG_GISA_DKB A WHERE
					 A.NO_KK = $no_kk AND ROWNUM =1";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_detail_anggota_kk($no_kk){
			$sql = "SELECT A.NIK
				  , A.NO_KK
				  , A.NAMA_LGKP
          		  , A.JENIS_KLMIN
          		  , A.TMPT_LHR
          		  , A.TGL_LHR
          		  , A.UMUR
          		  , A.STAT_HBKEL
          		  , A.AGAMA
          		  , CASE WHEN A.STATUS_KAWIN = 'KAWIN' AND A.STATUS_AKTA_KWN = 'TERCATAT' THEN 'KAWIN TERCATAT' WHEN A.STATUS_KAWIN = 'KAWIN' AND A.STATUS_AKTA_KWN = 'TIDAK TERCATAT' THEN 'KAWIN BELUM TERCATAT' ELSE A.STATUS_KAWIN END AS STATUS_KAWIN
          		  , A.GOL_DRH
          		  , A.PEKERJAAN
          		  , A.STATUS_KK
          		  , A.STATUS_KIA
          		  , A.STATUS_KTP
          		  , A.STATUS_REKAM
          		  , A.STATUS_AKTA_LHR
          		  , A.STATUS_AKTA_KWN
            	  , A.STATUS_AKTA_CRAI
          		  , A.NO_AKTA_LHR
          		  , A.NO_AKTA_KWN
          		  , A.NO_AKTA_CRAI
          		  FROM KAMPUNG_GISA_DKB A LEFT JOIN REF_SIAK_WNI B ON A.STAT_HBKEL = UPPER(B.DESCRIP)
				 WHERE 
				 A.NO_KK = $no_kk
				ORDER BY A.NO_KEC, A.NO_KEL, A.NO_KK, B.NO";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_count_detail_nik($nik){
			$sql = "SELECT COUNT(1) AS JML FROM KAMPUNG_GISA_DKB A WHERE A.NIK = $nik";
			$q = $this->yzdb->query($sql);
			return $q->row(0)->JML;
		}
		public function get_detail_nik($nik){
			$sql = "SELECT 
					  A.NIK
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , A.JENIS_KLMIN
          		  	  , A.TMPT_LHR
          		  	  , A.TGL_LHR
          		  	  , A.UMUR
          		  	  , CASE WHEN A.STATUS_KAWIN = 'KAWIN' AND A.STATUS_AKTA_KWN = 'TERCATAT' THEN 'KAWIN TERCATAT' WHEN A.STATUS_KAWIN = 'KAWIN' AND A.STATUS_AKTA_KWN = 'TIDAK TERCATAT' THEN 'KAWIN BELUM TERCATAT' ELSE A.STATUS_KAWIN END AS STATUS_KAWIN
          		  	  , A.STATUS_AKTA_LHR
          		  	  , A.STATUS_AKTA_KWN
            	  	  , A.STATUS_AKTA_CRAI
          		  	  , A.NO_AKTA_LHR
          		  	  , A.NO_AKTA_KWN
          		  	  , TO_CHAR(A.TGL_KWN,'DD/MM/YYYY') AS TGL_KWN
          		  	  , A.NO_AKTA_CRAI
          		  	  , TO_CHAR(A.TGL_CRAI,'DD/MM/YYYY') AS TGL_CRAI
					  FROM KAMPUNG_GISA_DKB A WHERE
					 A.NIK = $nik AND ROWNUM =1";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function insert_hist_gisa($nik,$akta_lhr,$no_akta_lhr,$akta_kwn,$no_akta_kwn,$akta_crai,$no_akta_crai,$user_name){
			$sql = "INSERT INTO KAMPUNG_GISA_HIST (ID,NIK,STATUS_AKTA_LHR,NO_AKTA_LHR,STATUS_AKTA_KWN,NO_AKTA_KWN,STATUS_AKTA_CRAI,NO_AKTA_CERAI,CREATED_BY,CREATED_DT) VALUES ('$nik-GISA-".time()."',$nik,'$akta_lhr','$no_akta_lhr','$akta_kwn','$no_akta_kwn','$akta_crai','$no_akta_crai','$user_name',SYSDATE)";
			// echo $sql;
			// die;
			$q = $this->yzdb->query($sql);
			return $q;
		}
		Public function update_gisa($nik,$akta_lhr,$no_akta_lhr,$akta_kwn,$no_akta_kwn,$akta_crai,$no_akta_crai){
			$sql = "UPDATE KAMPUNG_GISA_DKB SET STATUS_AKTA_LHR = '$akta_lhr', NO_AKTA_LHR = '$no_akta_lhr', STATUS_AKTA_KWN = '$akta_kwn', NO_AKTA_KWN = '$no_akta_kwn', STATUS_AKTA_CRAI = '$akta_crai', NO_AKTA_CRAI = '$no_akta_crai' WHERE NIK = $nik";
			// echo $sql;
			// die;
			$q = $this->yzdb->query($sql);
			return $q;
		}
		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_nm_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_nm_prop();
    	}
    	function get_nm_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_nm_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}