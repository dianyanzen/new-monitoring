<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Masterlaporanusia extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->yzdb = $this->load->database('YZDB', TRUE);

		}
		// usia 0-5 tahun
		public function get_laporan_umur($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT NO_KEC NO_WIL,
					F5_GET_NAMA_KECAMATAN(32,73,NO_KEC) NAMA_WIL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC";	
			}else{
				$sql .="SELECT NO_KEL NO_WIL,
					  F5_GET_NAMA_KELURAHAN(32,73,NO_KEC,NO_KEL) NAMA_WIL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEC,NO_KEL
						    ORDER BY NO_KEC,NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEC,NO_KEL
						    ORDER BY NO_KEC,NO_KEL";
						}
				}
				// ECHO $sql;
				// die;
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		public function get_laporan_umur_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		public function get_laporan_umur_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),1)) R9, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R10, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),1)) R11, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R12, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),1)) R13, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R14, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),1)) R15, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R16, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		// usia 0-10 tahun
		public function get_laporan_umur10($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9, 
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur10($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		public function get_laporan_umur10_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur10_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		public function get_laporan_umur10_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur10_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),1)) R5, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),1)) R6, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),1)) R7, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),1)) R8, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) R9,
					(COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		// usia 0-1 tahun
		public function get_laporan_umur1($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur1($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		public function get_laporan_umur1_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur1_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		public function get_laporan_umur1_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur1_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0.9),1)) R1, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),1.9),1)) R2,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2.9),1)) R3,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),3.9),1)) R4,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4.9),1)) R5,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5.9),1)) R6, 
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6.9),1)) R7,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7.9),1)) R8,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),8.9),1)) R9,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),9.9),1)) R10,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),10.9),1)) R11, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),11.9),1)) R12,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12.9),1)) R13,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13.9),1)) R14,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),14.9),1)) R15,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15.9),1)) R16, 
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16.9),1)) R17,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),17.9),1)) R18,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18.9),1)) R19,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),19.9),1)) R20,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),20.9),1)) R21,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),21.9),1)) R22,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),22.9),1)) R23,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),23.9),1)) R24,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),24.9),1)) R25,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),25.9),1)) R26,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),26.9),1)) R27,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),27.9),1)) R28,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),28.9),1)) R29,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),29.9),1)) R30,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),30.9),1)) R31,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),31.9),1)) R32,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),32.9),1)) R33,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),33.9),1)) R34,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),34.9),1)) R35,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),35.9),1)) R36,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),36.9),1)) R37,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),37.9),1)) R38,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),38.9),1)) R39,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),39.9),1)) R40,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),40.9),1)) R41,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),41.9),1)) R42,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),42.9),1)) R43,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),43.9),1)) R44,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),44.9),1)) R45,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),45.9),1)) R46,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),46.9),1)) R47,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),47.9),1)) R48,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),48.9),1)) R49,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),49.9),1)) R50,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),50.9),1)) R51,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),51.9),1)) R52,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),52.9),1)) R53,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),53.9),1)) R54,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),54.9),1)) R55,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),55.9),1)) R56,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),56.9),1)) R57,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),57.9),1)) R58,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),58.9),1)) R59,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),59.9),1)) R60,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),60.9),1)) R61,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),61.9),1)) R62,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),62.9),1)) R63,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),63.9),1)) R64,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),64.9),1)) R65,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),65.9),1)) R66,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),66.9),1)) R67,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),67.9),1)) R68,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),68.9),1)) R69,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),69.9),1)) R70,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),70.9),1)) R71,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),71.9),1)) R72,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),72.9),1)) R73,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),73.9),1)) R74,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),74.9),1)) R75,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),75.9),1)) R76,
			    	  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),76.9),1)) R77,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),77.9),1)) R78,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),78.9),1)) R79,
			          COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),79.9),1)) R80,
					  COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),80),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),2000),1)) REND,
					  (COUNT(1))JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		// usia sekolah tahun
		public function get_laporan_umur_sekolah($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur_sekolah($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE  (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		public function get_laporan_umur_sekolah_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur_sekolah_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 1 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		public function get_laporan_umur_sekolah_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur_sekolah_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),4),1)) R1, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),5),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),6),1)) R2, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),7),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),12),1)) R3, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),13),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),15),1)) R4, 
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),16),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) R5,
					COUNT(DECODE(GREATEST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),0),LEAST((TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY')),18),1)) JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE JENIS_KLMIN = 2 AND (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		public function get_laporan_umur_pemuda($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2, (B.R1 + B.R2) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					SUM (CASE WHEN JENIS_KLMIN = 1 THEN 1 ELSE 0 END)  AS R1,
          			SUM (CASE WHEN JENIS_KLMIN = 2 THEN 1 ELSE 0 END)  AS R2
					FROM ".$this->get_dkb_bio()." WHERE (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') >=16 AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') <=30
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2, (B.R1 + B.R2) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					SUM (CASE WHEN JENIS_KLMIN = 1 THEN 1 ELSE 0 END)  AS R1,
          			SUM (CASE WHEN JENIS_KLMIN = 2 THEN 1 ELSE 0 END)  AS R2
					FROM ".$this->get_dkb_bio()." WHERE (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) 
						AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') >=16 AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') <=30 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur_pemuda($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					SUM (CASE WHEN JENIS_KLMIN = 1 THEN 1 ELSE 0 END)  AS R1,
          			SUM (CASE WHEN JENIS_KLMIN = 2 THEN 1 ELSE 0 END)  AS R2,
          			COUNT(1) AS JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73
					AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') >=16 AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') <=30
					";	
			}else{
				$sql .="SELECT
					SUM (CASE WHEN JENIS_KLMIN = 1 THEN 1 ELSE 0 END)  AS R1,
          			SUM (CASE WHEN JENIS_KLMIN = 2 THEN 1 ELSE 0 END)  AS R2,
          			COUNT(1) AS JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73
					AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') >=16 AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') <=30
					";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		public function get_laporan_umur_pemuda_pendidikan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10, (B.R1+B.R2+B.R3+B.R4+B.R5+B.R6+B.R7+B.R8+B.R9+B.R10) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					SUM(CASE WHEN PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) AS JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73 
					AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') >=16 AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') <=30
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10, (B.R1+B.R2+B.R3+B.R4+B.R5+B.R6+B.R7+B.R8+B.R9+B.R10) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					SUM(CASE WHEN PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) AS JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) 
						AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') >=16 AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') <=30 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}
		
		public function get_jumlah_laporan_umur_pemuda_pendidikan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					SUM(CASE WHEN PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) AS JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73
					AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') >=16 AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') <=30
					";	
			}else{
				$sql .="SELECT
					SUM(CASE WHEN PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) AS JUMLAH
					FROM ".$this->get_dkb_bio()." WHERE (FLAG_STATUS = '0' OR (FLAG_STATUS = '2' AND FLAG_PINDAH IN(1,2,3,4,5))) AND NO_PROP = 32 AND NO_KAB =73
					AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') >=16 AND TO_CHAR(TO_DATE('".$this->get_cut_off_date()."','DD/MM/YYYY'),'yyyy')-TO_CHAR(TGL_LHR,'YYYY') <=30
					";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->yzdb->query($sql);
               return $q->result();
		}

		function get_dkb_bio()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_dkb_bio();
    	}
    	function get_dkb_keluarga()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_dkb_keluarga();
    	}
    	function get_cut_off_date()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cut_off_date();
    	}
    	function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}