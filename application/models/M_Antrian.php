<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Antrian extends CI_Model {
	function __construct()
		{
			parent:: __construct();
            $this->smsdb = $this->load->database('SMSDB', TRUE);
			$this->yzdb = $this->load->database('YZDB', TRUE);
		}
		public function get_loket(){
			$sql = "SELECT DISTINCT(KODE_PELAYANAN) KODE_PELAYANAN  FROM SMS_PANGGILAN ORDER BY KODE_PELAYANAN";
			$q = $this->smsdb->query($sql);
			return $q->result();
		}
		public function get_antrian($loket){
			$sql = "SELECT NIK, NAMA, KODE_PELAYANAN, URUTAN, TO_CHAR(TGL_PELAYANAN,'DD-MM-YYYY') AS TANGGAL, JAM, HARI, TELEPON FROM SMS_ANTRIAN WHERE URUTAN LIKE '$loket%' AND TO_CHAR(TGL_PELAYANAN,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ORDER BY TGL_PELAYANAN ASC";
			$q = $this->smsdb->query($sql);
			return $q;
		}
        public function get_cek_antrian($nik,$tlp){
            $sql = "SELECT NIK, NAMA, KODE_PELAYANAN, URUTAN, TO_CHAR(TGL_PELAYANAN,'DD-MM-YYYY') AS TANGGAL, JAM, HARI, TELEPON FROM (SELECT A.*, ROW_NUMBER() OVER (ORDER BY TGL_PELAYANAN ASC) AS SEQNUM FROM SMS_ANTRIAN A WHERE NIK = '$nik' OR TELEPON = '$tlp') RIL WHERE SEQNUM = 1";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_antrianlog($nik,$tlp){
            $sql = "SELECT NIK, NAMA, KODE_PELAYANAN, URUTAN, TO_CHAR(TGL_PELAYANAN,'DD-MM-YYYY') AS TANGGAL, JAM, HARI, TELEPON FROM SMS_ANTRIAN_LOG WHERE NIK = '$nik' OR TELEPON = '$tlp' ORDER BY TGL_PELAYANAN DESC";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_nohp($tlp){
            $sql = "SELECT TELEPON, JUMLAH_SMS, STATUS_BLOKIR, TO_CHAR(TANGGAL_BLOKIR,'DD-MM-YYYY') TANGGAL_BLOKIR FROM SMS_DATA WHERE TELEPON = '$tlp'";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_antrian_biodata($nik){
            $sql = "SELECT A.NIK, A.NAMA_LGKP, A.TMPT_LHR, TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR, CASE WHEN A.AKTA_LHR = 2 THEN 'ADA' ELSE 'TIDAK ADA' END AS STATUS_AKTA,CASE WHEN A.AKTA_LHR = 2 THEN A.NO_AKTA_LHR ELSE '-' END AS NO_AKTA_LHR, CASE WHEN B.NIK IS NULL THEN 0 ELSE 1 END STATUS_DATA FROM BIODATA_WNI@DB222 A LEFT JOIN BIODATA_WNI B ON A.NIK = B.NIK WHERE A.NIK = $nik";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_antrianakta($nik){
            $sql = "SELECT NIK, NAMA_LGKP, TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') TGL_LHR, CASE WHEN AKTA_LHR = 2 THEN 'ADA' ELSE 'TIDAK ADA' END AS STATUS_AKTA,CASE WHEN AKTA_LHR = 2 THEN NO_AKTA_LHR ELSE '-' END AS NO_AKTA_LHR FROM BIODATA_WNI WHERE NIK = $nik";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_antrian_invalid($nik){
            $sql = "SELECT NIK, NAMA, KODE_PELAYANAN, URUTAN, TO_CHAR(TGL_PELAYANAN,'DD-MM-YYYY') AS TANGGAL, JAM, HARI, TELEPON FROM SMS_ANTRIAN A WHERE NIK = '$nik'";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_antrianakta_invalid($nik){
            $sql = "SELECT A.NIK, A.AKTA_LHR, A.NO_AKTA_LHR FROM BIODATA_WNI A WHERE A.NIK = $nik AND A.NO_AKTA_LHR IS NOT NULL AND A.AKTA_LHR = 2 AND NOT EXISTS (SELECT 1 FROM CAPIL_LAHIR".$this->get_siak_dblink()." B WHERE TO_CHAR(A.NIK) = TO_CHAR(B.BAYI_NIK)) AND EXISTS (SELECT 1 FROM BIODATA_WNI".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK)";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_antrianakta_not_invalid($nik){
            $sql = "SELECT A.NIK, A.AKTA_LHR, A.NO_AKTA_LHR FROM BIODATA_WNI A WHERE A.NIK = $nik  ";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_insert_biodata($nik){
            $sql = "SELECT A.NIK FROM BIODATA_WNI@DB222 A LEFT JOIN BIODATA_WNI B ON A.NIK = B.NIK WHERE B.NIK IS NULL AND A.NIK = $nik";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function get_cek_nohp_block($tlp){
            $sql = "SELECT TELEPON, JUMLAH_SMS, STATUS_BLOKIR, TO_CHAR(TANGGAL_BLOKIR,'DD-MM-YYYY') TANGGAL_BLOKIR FROM SMS_DATA WHERE TELEPON = '$tlp'";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function check_calo($tlp){
            $sql = "SELECT COUNT(1) JML FROM SMS_DATA WHERE TELEPON = '$tlp' AND TANGGAL_BLOKIR >= TRUNC(SYSDATE+30)";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function delete_nohp($tlp){
            $sql = "DELETE FROM SMS_DATA WHERE TELEPON = '$tlp' AND TANGGAL_BLOKIR < TRUNC(SYSDATE+30)";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function insert_nik_biodata($nik){
            $sql = "INSERT INTO BIODATA_WNI SELECT * FROM BIODATA_WNI@DB222 WHERE NIK = $nik";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function delete_antrian($nik){
            $sql = "DELETE FROM SMS_ANTRIAN WHERE NIK = '$nik'";
            $q = $this->smsdb->query($sql);
            return $q;
        }
        public function delete_akta($nik_akta,$akta_lhr,$no_akta_lhr,$user_id,$ip_address){
            $sql = "INSERT INTO SIAK_HIST_AKTA (ID,NIK,AKTA_LHR,NO_AKTA_LHR,UPDATE_DT,UPDATE_BY,IP_ADDRESS) VALUES ('$nik_akta-SMSAKTA-".time()."',$nik_akta,$akta_lhr,'$no_akta_lhr',SYSDATE,'$user_id','$ip_address')";
            $q = $this->yzdb->query($sql);
            $sql2 = "UPDATE BIODATA_WNI SET AKTA_LHR = 1, NO_AKTA_LHR = NULL WHERE NIK = $nik_akta";
            $q2 = $this->smsdb->query($sql2);
            $sql3 = "UPDATE BIODATA_WNI SET AKTA_LHR = 1, NO_AKTA_LHR = NULL WHERE NIK = $nik_akta";
            $q3 = $this->db->query($sql3);
            return $q3;
        }
        public function update_akta($nik_akta,$akta_lhr,$no_akta_lhr,$user_id,$ip_address){
            $sql = "INSERT INTO SIAK_HIST_AKTA (ID,NIK,AKTA_LHR,NO_AKTA_LHR,UPDATE_DT,UPDATE_BY,IP_ADDRESS) VALUES ('$nik_akta-UPDAKTA-".time()."',$nik_akta,$akta_lhr,'$no_akta_lhr',SYSDATE,'$user_id','$ip_address')";
            $q = $this->yzdb->query($sql);
            $sql2 = "UPDATE BIODATA_WNI SET AKTA_LHR = 2, NO_AKTA_LHR = '$no_akta_lhr' WHERE NIK = $nik_akta";
            $q2 = $this->smsdb->query($sql2);
            $sql3 = "UPDATE BIODATA_WNI SET AKTA_LHR = 2, NO_AKTA_LHR = '$no_akta_lhr' WHERE NIK = $nik_akta";
            $q3 = $this->db->query($sql3);
       /*     echo $sql;
            echo '<br>';
            echo '<br>';
            echo $sql2;
            echo '<br>';
            echo '<br>';
            echo $sql3;
            echo '<br>';
            echo '<br>';*/
            die;
            return $q3;
        }
		public function get_antrian_today($loket){
			$sql = "SELECT COUNT(1) AS JML FROM SMS_ANTRIAN WHERE URUTAN LIKE '$loket%' AND TO_CHAR(TGL_PELAYANAN,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')";
			$q = $this->smsdb->query($sql);
			return $q->result();
		}
		public function get_antrian_tomorrow($loket){
			$sql = "SELECT COUNT(1) AS JML FROM SMS_ANTRIAN WHERE URUTAN LIKE '$loket%' AND TO_CHAR(TGL_PELAYANAN,'DD/MM/YYYY') = TO_CHAR(SYSDATE+1,'DD/MM/YYYY')";
			$q = $this->smsdb->query($sql);
			return $q->result();
		}
		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}