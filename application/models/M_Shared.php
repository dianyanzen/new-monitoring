<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Shared extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->yzdb = $this->load->database('YZDB', TRUE);
		}
		
		public function get_all_kecamatan($no_kec = 0){
			$sql = "";
			$sql .= "SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC WHERE NO_PROP = 32 AND NO_KAB =  73 ";
			if ($no_kec != 0){
				$sql .= " AND NO_KEC = $no_kec ";
			}
			$sql .= " ORDER BY NO_KEC";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_one_kecamatan($no_kec = 0){
			$sql ="";
			$sql .=" SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC WHERE NO_PROP = 32 AND NO_KAB =  73";
			if($no_kec != 0){
			$sql .=" AND NO_KEC = $no_kec";	
			}
			$sql .=" ORDER BY NO_KEC";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_prop($no_prop = 0){
			$sql ="";
			$sql .=" SELECT NO_PROP, NAMA_PROP FROM SETUP_PROP WHERE 1=1";
			if($no_prop != 0){
			$sql .=" AND NO_PROP = $no_prop";	
			}
			$sql .=" ORDER BY NO_PROP";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_kab($no_prop = 0, $no_kab = 0){
			$sql ="";
			$sql .=" SELECT NO_PROP, NO_KAB, NAMA_KAB FROM SETUP_KAB WHERE 1=1";
			if($no_prop != 0){
			$sql .=" AND NO_PROP = $no_prop";	
			}
			if($no_kab != 0){
			$sql .=" AND NO_KAB = $no_kab";	
			}
			$sql .=" ORDER BY NO_KAB";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_kec($no_prop = 0, $no_kab = 0, $no_kec = 0){
			$sql ="";
			$sql .=" SELECT NO_PROP, NO_KAB, NO_KEC, NAMA_KEC FROM SETUP_KEC WHERE 1=1";
			if($no_prop != 0){
			$sql .=" AND NO_PROP = $no_prop";	
			}
			if($no_kab != 0){
			$sql .=" AND NO_KAB = $no_kab";	
			}
			if($no_kec != 0){
			$sql .=" AND NO_KEC = $no_kec";	
			}
			$sql .=" ORDER BY NO_KEC";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_kel($no_prop = 0, $no_kab = 0, $no_kec = 0, $no_kel = 0){
			$sql ="";
			$sql .=" SELECT NO_PROP, NO_KAB, NO_KEC, NO_KEL, NAMA_KEL FROM SETUP_KEL WHERE 1=1";
			if($no_prop != 0){
			$sql .=" AND NO_PROP = $no_prop";	
			}
			if($no_kab != 0){
			$sql .=" AND NO_KAB = $no_kab";	
			}
			if($no_kec != 0){
			$sql .=" AND NO_KEC = $no_kec";	
			}
			if($no_kel != 0){
			$sql .=" AND NO_KEL = $no_kel";	
			}
			$sql .=" ORDER BY NO_KEL";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_user_cpltte(){
			$sql ="";
			$sql .="SELECT DISTINCT(LOWER(REQ_BY)) REQ_BY FROM BSRE_KELAHIRAN ORDER BY REQ_BY";
			$q = $this->db->query($sql);
			return $q->result();
		}

		public function get_all_kelurahan($no_kec=0, $no_kel=0){
			$sql = "";
			$sql .= "SELECT NO_KEL, NAMA_KEL, NO_KEC FROM SETUP_KEL WHERE NO_PROP= 32 AND NO_KAB= 73 ";
			if ($no_kec != 0){
				$sql .= " AND NO_KEC = $no_kec ";
			}
			if ($no_kel != 0){
				$sql .= " AND NO_KEL = $no_kel ";
			}
			  $sql .= " ORDER BY NO_KEC,NO_KEL";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_all_rw($no_kec=0,$no_kel=0){
			$sql = "SELECT NO_KEC, NO_KEL, NO_RW, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW, COUNT(1) JML FROM DATA_KELUARGA WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW ORDER BY NO_KEC, NO_KEL, NO_RW) WHERE JML > 50 ORDER BY NO_KEC, NO_KEL, NO_RW";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_all_rt($no_kec=0,$no_kel=0,$no_rw=0){
			$sql = "SELECT NO_KEC, NO_KEL, NO_RW,NO_RT, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW,NO_RT, COUNT(1) JML FROM DATA_KELUARGA WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW = $no_rw AND NO_RW IS NOT NULL AND NO_RT IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW, NO_RT ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT) WHERE JML > 5 ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_nama_kec($no_kec){
			$sql = "SELECT NAMA_KEC FROM SETUP_KEC WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->NAMA_KEC;
		}
		public function get_nama_kel($no_kec, $no_kel){
			$sql = "SELECT NAMA_KEL FROM SETUP_KEL WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->NAMA_KEL;
		}
		public function get_all_wil(){
			$sql = "SELECT NO_WIL, NAMA_WIL FROM SIAK_KODE_WIL ORDER BY NO_WIL";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_master_menu(){
			$sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_LAPORAN ORDER BY MENU_ID";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_master_usia(){
			$sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_USIA ORDER BY MENU_ID";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_master_cakupan(){
			$sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_CAKUPAN ORDER BY MENU_ID";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_month_date($bln){
			$sql = "SELECT 
				      TO_CHAR(DAY,'DD-MM-YYYY') DAY
				    , TO_CHAR(DAY, 'd') DOW
				FROM (SELECT TRUNC(TO_DATE('$bln','MM-YYYY'), 'MM') + LEVEL - 1 AS DAY FROM DUAL CONNECT BY TRUNC(TRUNC(TO_DATE('$bln','MM-YYYY'), 'MM') + LEVEL - 1, 'MM') = TRUNC(TO_DATE('$bln','MM-YYYY'), 'MM')) ORDER BY DAY";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dkb(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_dkb_bio(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_BIO'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_dkb_keluarga(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_KELUARGA'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_cut_off_date(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_CUTOFF'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		
		public function get_user_daily(){
			$sql = "SELECT DISTINCT(USER_ID) FROM SIAK_DAILY ORDER BY USER_ID";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_no_prop(){
			$sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NO_PROP'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_no_kab(){
			$sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NO_KAB'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_nm_prop(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NM_PROP'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_nm_kab(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NM_KAB'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_siak_dblink(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'SIAK_DBLINK'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_rekam_dblink(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'REKAM_DBLINK'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_cetak_dblink(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'CETAK_DBLINK'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_master_dblink(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'MASTER_DBLINK'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function is_valid(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'EXP'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_default_pass(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DEFAULT_PASS'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_give_kec(){
			$sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'GIVE_ALL_KEC'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_give_kel(){
			$sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'GIVE_ALL_KEL'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_menu($level = 0){
			$sql = "SELECT A.MENU_ID,A.PARENT_ID, A.ICON,A.TITLE,A.URL,A.IS_ACTIVE, A.USER_LEVEL, A.MENU_LEVEL FROM VW_SIAK_MENU A  WHERE A.USER_LEVEL = $level AND A.MENU_LEVEL = 1 AND A.ACTIVE_MENU = 1 ORDER BY A.LEVEL_HEAD,A.LEVEL_SUB,A.LEVEL_CONTENT, A.TITLE";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			$i=0;
        	foreach($r as $row){
            	$r[$i]->SUB_MENU = $this->get_sub_menu($row->MENU_ID,$level);
            	$i++;
        	}
        	return $r;
		}
		public function get_sub_menu($parent_id = 0,$level = 0){
			$sql = "SELECT A.MENU_ID,A.PARENT_ID, A.ICON,A.TITLE,A.URL,A.IS_ACTIVE, A.USER_LEVEL, A.MENU_LEVEL FROM VW_SIAK_MENU A  WHERE A.USER_LEVEL = $level AND A.PARENT_ID = $parent_id AND A.ACTIVE_MENU = 1 ORDER BY A.LEVEL_HEAD,A.LEVEL_SUB,A.LEVEL_CONTENT, A.TITLE";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			$i=0;
        	foreach($r as $row){
        		if($row->IS_ACTIVE != 1){
            	$r[$i]->SUB_MENU = $this->get_sub_menu($row->MENU_ID,$level);
            	}
            	$i++;
        	}
        	return $r;
		}
		public function get_menu_all($level = 0){
			$sql = "SELECT TO_NUMBER(A.MENU_ID) MENU_ID,A.PARENT_ID,A.TITLE,A.ACTIVE_MENU, A.USER_LEVEL, A.MENU_LEVEL FROM VW_SIAK_MENU A  WHERE A.USER_LEVEL = $level AND A.MENU_LEVEL = 1 ORDER BY A.LEVEL_HEAD,A.LEVEL_SUB,A.LEVEL_CONTENT";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
        	return $r;
		}
		public function get_sub_menu_all($parent_id = 0,$level = 0){
			$sql = "SELECT TO_NUMBER(A.MENU_ID) MENU_ID,A.PARENT_ID,A.TITLE,A.ACTIVE_MENU, A.USER_LEVEL, A.MENU_LEVEL FROM VW_SIAK_MENU A  WHERE A.USER_LEVEL = $level AND A.PARENT_ID = $parent_id ORDER BY A.LEVEL_HEAD,A.LEVEL_SUB,A.LEVEL_CONTENT";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
        	return $r;
		}
		public function cek_is_akses($level = 0,$menu_id = 0){
			$sql = "SELECT IS_ACTIVE VAL FROM SIAK_AKSES WHERE MENU_ID = $menu_id AND USER_LEVEL = $level";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}

		public function get_islogin($ip_address,$user_id){
			$sql = "SELECT COUNT(1) VAL FROM SIAK_SESSION_PLUS WHERE IP_ADDRESS = '$ip_address' AND USER_ID = '$user_id' AND IS_ACTIVE = 1";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		 public function stop_activity($user_id){
        	$sql = "SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'";
			$q = $this->yzdb->query($sql);
             $r = (int) $q->row()->CNT;
             if($r > 0){
                $sql = "UPDATE SIAK_SESSION_PLUS SET LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 0  WHERE USER_ID = '$user_id'";
                $this->yzdb->query($sql);
             }
             
        }
        public function update_no_prop($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_NUM = $val
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'NO_PROP'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_no_kab($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_NUM = $val
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'NO_KAB'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_nm_prop($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'NM_PROP'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_nm_kab($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'NM_KAB'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_siak_dblink($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'SIAK_DBLINK'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_cetak_dblink($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'CETAK_DBLINK'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_rekam_dblink($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'REKAM_DBLINK'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_master_dblink($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'MASTER_DBLINK'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_dkb_bio($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DKB_BIO'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_dkb_kk($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DKB_KELUARGA'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_dkb_tahun($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DKB'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_dkb_cutoff($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DKB_CUTOFF'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_default_pass($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DEFAULT_PASS'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_give_kec($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_NUM = $val
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'GIVE_ALL_KEC'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_give_kel($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_NUM = $val
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'GIVE_ALL_KEL'";
            $q = $this->yzdb->query($sql);
            return $q;
        }


}