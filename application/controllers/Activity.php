<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login','lgn');
		$this->load->model('M_Activity','act');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function index()
	{
			$menu_id = 136;
			$is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
			if ($is_akses == 0){
				redirect('404Notfound','refresh');
			}
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			/*$rs = $this->act->get_all_activity($this->session->userdata(S_USER_ID));
			$rbc = $this->act->get_all_bcard($this->session->userdata(S_USER_ID));	
			$jbe = $this->act->get_count_benroll($this->session->userdata(S_USER_ID));*/
			$data = array(
		 		"stitle"=>'My Activity Page',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		/*"data_siak"=>$rs,
		 		"data_bcard"=>$rbc,
		 		"data_benroll"=>$rbe,*/
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Activity/index',$data);
		
	}
	public function rekap() 
	{
			$menu_id = 137;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap My Activity Siak ',
		 		"mtitle"=>'Rekap My Activity Siak ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Rekap_siak/index',$data);
	}

	public function kedatangan() 
	{
			$menu_id = 182;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Kedatangan Siak ',
		 		"mtitle"=>'Rekap Kedatangan Siak ',
		 		"my_url"=>'kedatangan',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Rekap_siak_kedatangan/index',$data);
	}
	public function detail_kk() 
	{
			$menu_id = 158;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Daftar Pencetakan Kartu Keluarga ',
		 		"mtitle"=>'Daftar Pencetakan Kartu Keluarga ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Rekap_detail_kk/index',$data);
	}
	public function detail_kk_bulan()
	{
			$menu_id = 159;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('no_kec') != null){
	          $no_kec = $this->input->post('no_kec');
	          $no_kel = 0;
	          $tgl = $this->input->post('tanggal');
	          $val = $this->act->get_pivot_kel_str($no_kec,$no_kel);
	          $j = $this->act->get_jum_kel($no_kec,$no_kel);
	          $r = $this->act->rekap_detail_kk_bulan($this->session->userdata(S_USER_ID),$tgl,$no_kec,$no_kel,$val);

			$data = array(
		 		"stitle"=>'Detail Cetak KK User '.$this->session->userdata(S_NAMA_LGKP).' Bulan '.$tgl ,
		 		"mtitle"=>'Detail Cetak KK User '.$this->session->userdata(S_NAMA_LGKP).' Bulan '.$tgl,
		 		"my_url"=>'Bulanan',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"nama_wil"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Detail KK Bulanan',
		 		"mtitle"=>'Detail KK Bulanan',
		 		"my_url"=>'Bulanan',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('Rekap_bulan_kk/index',$data);
		
	}

	public function rekap_kk() 
	{
			$menu_id = 160;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Daftar Pencetakan Kartu Keluarga ',
		 		"mtitle"=>'Daftar Pencetakan Kartu Keluarga ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Rekap_kk/index',$data);
	}
	public function setting()
	{
		
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$r = $this->act->get_user($this->session->userdata(S_USER_ID));
			$data = array(
		 		"stitle"=>'Account Setting',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Activity_setting/index',$data);
		
	}
	public function get_rekap_data()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $user_id = $this->input->post('user_id');
          $tgl = $this->input->post('tanggal');
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->act->get_recap_activity_data($user_id,$tgl_start,$tgl_end);
          $data = array();
          $i = 0;
          foreach($rekap_siak->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->NAMA_LGKP,
                    $tgl,
                    $r->P1,
                    $r->JML
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_siak->num_rows(),
                 "recordsFiltered" => $rekap_siak->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}

	public function get_rekap_detail_kk()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $user_id = $this->input->post('user_id');
          $no_kec = $this->input->post('no_kec');
          $no_kel = $this->input->post('no_kel');
          $tgl = $this->input->post('tanggal');
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->act->rekap_detail_kk($user_id,$tgl_start,$tgl_end,$no_kec,$no_kel);
          $data = array();
          $i = 0;
          foreach($rekap_siak->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->NO_KK,
                    $r->NAMA_KEP,
                    $r->TGL_REQ,
                    $r->TGL_CETAK,
                    ''
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_siak->num_rows(),
                 "recordsFiltered" => $rekap_siak->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}

	public function get_rekap_kedatangan()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $no_kec = $this->input->post('no_kec');
          $no_kel = $this->input->post('no_kel');
          $tgl = $this->input->post('tanggal');
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->act->rekap_kedatangan($tgl_start,$tgl_end,$no_kec,$no_kel);
          $data = array();
          $i = 0;
          foreach($rekap_siak->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->KEC,
                    $r->KEL,
                    $r->RW,
                    $r->RT,
                    $r->ALAMAT ,
                    $r->NO_DATANG,
                    $r->NO_PINDAH,
                    $r->PROP_ASAL,
                    $r->KAB_ASAL,
                    $r->KEC_ASAL,
                    $r->KEL_ASAL,
                    '\''.$r->NO_KK,
                    '\''.$r->NIK_PEMOHON,
                    $r->NAMA_PEMOHON,
                    $r->TGL,
                    $r->JAM,
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_siak->num_rows(),
                 "recordsFiltered" => $rekap_siak->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}

	public function rekap_kk_data()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $user_id = $this->input->post('user_id');
          $no_kec = $this->input->post('no_kec');
          $tgl = $this->input->post('tanggal');
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->act->rekap_kk($user_id,$tgl_start,$tgl_end,$no_kec);
          $data = array();
          $i = 0;
          foreach($rekap_siak->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->NAMA_WIL,
                    $tgl,
                    $r->JML
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_siak->num_rows(),
                 "recordsFiltered" => $rekap_siak->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_data_first(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$is_online = $this->act->is_online($user_id);
		$clock_in = $this->act->get_clockin($user_id);
		$clock_out = $this->act->get_clockout($user_id);
		$response["clock_in"] = $clock_in;
		$response["clock_out"] = $clock_out;
		$response["user_siak"] = $is_online[0]->USER_SIAK;
		$response["user_id"] = $is_online[0]->USER_ID;
		$response["monev_last"] = $is_online[0]->MONEV_LAST;
		$response["monev_ip"] = $is_online[0]->MONEV_IP;
		$response["siak_last"] = $is_online[0]->SIAK_LAST;
		$response["siak_ip"] = $is_online[0]->SIAK_IP;
        echo json_encode($response);
	}
	public function get_data_second(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
			$js = $this->act->get_count_activity($user_id);
			$jbc = $this->act->get_count_bcard($user_id);
			$jbe = $this->act->get_count_benroll($user_id);
			$response["jml_siak"] = $js;
			$response["jml_bcard"] = $jbc;
			$response["jml_benroll"] = $jbe;
        echo json_encode($response);
	}
	public function get_data_third(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->act->get_all($user_id);
        echo json_encode($output);	
	}
	public function get_data_four(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->act->get_recap_activity($user_id);
        echo json_encode($output);	
	}
	public function siak_data(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->act->get_all_activity($user_id);
        echo json_encode($output);	
	}
	public function bcard_data(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->act->get_all_bcard($user_id);
        echo json_encode($output);	
	}
	public function benroll_data(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->act->get_all_benroll($user_id);
        echo json_encode($output);	
	}
	public function activity_daily()
	{
			$menu_id = 47;
			$is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
			if ($is_akses == 0){
				redirect('404Notfound','refresh');
			}
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('user_id') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$user_id = $this->input->post('user_id');
			$r = $this->act->cek_daily_activity($tgl_start,$tgl_end,$user_id);
			$j = $this->act->cek_count_daily_activity($tgl_start,$tgl_end,$user_id);
			$data = array(
		 		"stitle"=>$this->input->post('user_id').' Rekap Daily Report',
		 		"mtitle"=>$this->input->post('user_id').' Rekap Daily Report',
		 		"my_url"=>'activity_daily',
		 		"type_tgl"=>'Report',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Daily Report',
		 		"mtitle"=>'Rekap Daily Report',
		 		"my_url"=>'activity_daily',
		 		"type_tgl"=>'Report',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('activity_daily/index',$data);
		
	}
	public function absensi_daily()
	{
			$menu_id = 48;
			$is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
			if ($is_akses == 0){
				redirect('404Notfound','refresh');
			}
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('user_id') != null){
			$user_id = $this->input->post('user_id');
			$r = $this->act->cek_daily_absensi($user_id);
			$j = $this->act->cek_count_daily_absensi($user_id);
			$data = array(
		 		"stitle"=>$this->input->post('user_id').' Rekap Daily Absensi',
		 		"mtitle"=>$this->input->post('user_id').' Rekap Daily Absensi',
		 		"my_url"=>'absensi_daily',
		 		"type_tgl"=>'Absensi',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Daily Absensi',
		 		"mtitle"=>'Rekap Daily Absensi',
		 		"my_url"=>'absensi_daily',
		 		"type_tgl"=>'Absensi',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('absensi_daily/index',$data);
		
	}
	public function do_save(){
		header('Content-type: application/json');
		$user_name = $this->input->post('user_name');
		$old_password = md5($this->input->post('old_password'));
		$new_password = md5($this->input->post('new_password'));
		$r = $this->lgn->check_user($user_name, $old_password);
		if (count($r) > 0) {
			$r = $r[0];
			if ($r->USER_PWD == $old_password ) {
				$r = $this->lgn->change_pwd($user_name, $new_password);
				$output = array(
		    			"message_type"=>1,
		    			"message"=>"Your Password Has Been Changed Successfully! Thank you."
		    	);
			}else{
				$output = array(
		    			"message_type"=>0,
		    			"message"=>"Old Password Incorect For User $user_name Please Try Again"
		    		);
			}
		}else{
				$output = array(
	    			"message_type"=>0,
	    			"message"=>"Sorry User $user_name Not Found, Please Contact Admin For Registration"
	    		);
		}
		echo json_encode($output);
	}
	public function do_reset(){
		header('Content-type: application/json');
		if ($this->input->post('user_name') != null){
			$user_name = $this->input->post('user_name');
			$old_password = md5($this->input->post('old_password'));
			$new_password = md5($this->input->post('new_password'));
			$this->lgn->change_pwd($user_name, $new_password);
			$output = array(
	    			"message_type"=>1,
	    			"message"=>"Your Password Has Been Changed Successfully! Thank you."
	    	);
			echo json_encode($output);	
		}
	}
	public function do_pdf_detail_kk() {
        
		if($this->input->post('pdf_tanggal') != null){
			$pdf_userid = $this->session->userdata(S_USER_ID);
			$tgl = $this->input->post('pdf_tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('pdf_no_kec');
			$no_kel = $this->input->post('pdf_no_kel');
			$fileName = strtoupper($this->session->userdata(S_USER_ID)).'_activity_data_'.time().'.xlsx';  
			$this->load->library('pdf');
			$activity_data =  $this->act->rekap_detail_kk_pdf($pdf_userid,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$atasan =  $this->act->get_atasan($pdf_userid);
			// set document information
			$this->pdf->SetCreator('Dian Yanzen');
			$this->pdf->SetAuthor('Dian Yanzen');
			$this->pdf->SetTitle('PDF Monitoring');
			$this->pdf->SetSubject('YZ PDF');
			$this->pdf->SetKeywords('YZ, PDF, Monitoring, DisdukCapil, Kota Bandung');
			// set auto page breaks
			$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			// set image scale factor
			$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			// ---------------------------------------------------------
			// set font
			// set default header data
			$this->pdf->SetMargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
			$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			// set font
			$this->pdf->SetFont('times', '', 12);
			$this->pdf->AddPage();
			// add a page
			$html = '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>
			  table.first {
			    padding: 0px;
			    margin: 0px;
			    font-family: helvetica;
			    font-size: 7pt;
			  }
			</style>
			<table class="first" cellpadding="0" cellspacing="5" border="0">
			  <tr>
			    <td width="50" align="left">Nama
			    </td>
			    <td width="20" align="center">:
			    </td>
			    <td width="140" align="left">
			      <b>'.$this->session->userdata(S_NAMA_LGKP).'
			      </b>
			    </td>
			    <td width="80" align="center">
			    </td>
			    <td width="60" align="center">
			    </td>
			    <td width="20" align="center">
			    </td>
			    <td width="220" align="left">
			    </td>
			  </tr>
			  <tr>
			    <td width="50" align="left">Nik
			    </td>
			    <td width="20" align="center">:
			    </td>
			    <td width="140" align="left">'.$this->session->userdata(S_NIK).'
			    </td>
			    <td width="80" align="center">
			    </td>
			    <td width="60" align="center">
			    </td>
			    <td width="20" align="center">
			    </td>
			    <td width="220" align="left">
			    </td>
			  </tr>
			  <tr>
			    <td width="50" align="left">Perihal
			    </td>
			    <td width="20" align="center">:
			    </td>
			    <td width="210" align="left">Daftar Pencetakan Kartu Keluarga
			    </td>
			    <td width="10" align="center">
			    </td>
			    <td width="60" align="right">
			    </td>
			    <td width="20" align="center">
			    </td>
			    <td width="220" align="left">
			    </td>
			  </tr>
			  <tr>
			    <td width="50" align="left">Tanggal
			    </td>
			    <td width="20" align="center">:
			    </td>
			    <td width="210" align="left">'.$tgl.'
			    </td>
			    <td width="10" align="center">
			    </td>
			    <td width="60" align="right">
			    </td>
			    <td width="20" align="center">
			    </td>
			    <td width="220" align="left">
			    </td>
			  </tr>
			</table>
			<br>
			<br>
			<table id="demo-foo-row-toggler" class="first" data-page-size="100" cellpadding="10">
			  <thead>
			    <tr>
			      <th width="10%" style="border: 2px solid #e4e7ea; text-align: center;">No
			      </th>
			      <th width="17%" style="border: 2px solid #e4e7ea;text-align: center;">Nomor KK
			      </th>
			      <th width="20%" style="border: 2px solid #e4e7ea;text-align: center;">Nama Kepala Keluarga
			      </th>
			      <th width="15%" style="border: 2px solid #e4e7ea;text-align: center;">Tanggal Pengajuan
			      </th>
			      <th width="15%" style="border: 2px solid #e4e7ea;text-align: center;">Tanggal Cetak
			      </th>
			      <th width="18%" style="border: 2px solid #e4e7ea;text-align: center;">Keterangan
			      </th>
			    </tr>
			  </thead>
			  <tbody id="my_data" style="border: 1px solid #e4e7ea;">
			    ';
			    $style = array('border' => 0, 'vpadding' => 'auto', 'hpadding' => 'auto', 'fgcolor' => array(0, 0, 0), 'bgcolor' => false, 'module_width' => 1, 'module_height' => 1);
			    $i = 0;
			    foreach ($activity_data as $val) 
			    {
			    // new style
			    $i++;
			    // QRCODE,H : QR-CODE Best error correction
			    // print a line using Cell()
			    // $this->pdf->Cell(0, 12, strtoupper($this->session->userdata(S_USER_ID)).'_activity_data_'.time(), 0, 1, 'C');
			    $html .= '
			    <tr style="border: 1px solid #e4e7ea;">
			      <td width="10%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">'.$i.'
			      </td>
			      <td width="17%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">'.$val->NO_KK.'
			      </td>
			      <td width="20%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">'.$val->NAMA_KEP.'
			      </td>
			      <td width="15%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">'.$val->TGL_REQ.'
			      </td>
			      <td width="15%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">'.$val->TGL_CETAK.'
			      </td>
			      <td width="18%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">
			      </td>
			    </tr>
			    ';
			    // embed image, masked with previously embedded mask
			    // $this->pdf->Image(base_url('assets/upload/pp/'.$this->session->userdata(S_NIK).'.jpg'), 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
			    // $this->pdf->Image($image_file_bg, 10, 120, 180, '', '', '', '', false, 300, '', false, 300);
			    }
			    $html .= '
			  </tbody>
			</table>
			<table class="first" cellpadding="0" cellspacing="5" border="0">
			  <tr>
			    <td width="45%">
			    </td>	
			    <td width="10%">
			    </td>											
			    <td width="45%" align="center">
			    </td>
			  </tr>
			  <tr>
			    <td height="80" valign="top" align="center">
			    </td>
			    <td height="80" valign="top" align="center">
			    </td>
			    <td height="80" valign="top" align="center">Petugas KK :
			    </td>
			  </tr>        
			  <tr>
			    <td align="center">
			    </td>
			    <td>
			    </td>
			    <td align="center">
			      <u>'.$this->session->userdata(S_NAMA_LGKP).'
			      </u>
			    </td>
			  </tr>
			  <tr style="line-height:7px">
			    <td align="center">
			    </td>
			    <td>
			    </td>
			    <td align="center">NIK. '.$this->session->userdata(S_NIK).'
			    </td>
			  </tr>
			</table>
			';
			$this->pdf->writeHTML($html, true, false, true, false, '');
			//Close and output PDF document
			$this->pdf->Output(strtoupper($this->session->userdata(S_USER_ID)).'_activity_data_'.time().'.pdf', 'I');
			} 
			else 
			{
			redirect('/','refresh');
			}

    }
	public function clean($string) {
	   	$string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
	   	return preg_replace('/[^A-Za-z0-9\- ,.]/', '', $string); // Removes special chars.
	}


}