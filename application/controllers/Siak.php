<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siak extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Siak','siak');
		$this->load->model('M_Shared','shr');	
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}	
	}
	public function index()
	{
		redirect('/','refresh');
	}
	public function repair_local_biometric()
	{
			$menu_id = 120;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$bo = $this->input->post('biometric_option');
			if ($bo == 1 || $bo == 0 || $bo == 3){
				$r = $this->siak->get_data_rekam($nik,$this->session->userdata(S_NO_KEC));
				$j = $this->siak->get_count_face_rekam($nik);
				if ($j > 0) {
				$f = $this->siak->get_face_rekam($nik);
				}else{
				$f = [];	
				}	
			}else{
				$r = $this->siak->get_data_cetak($nik,$this->session->userdata(S_NO_KEC));
				$j = $this->siak->get_count_face_cetak($nik);
				if ($j > 0) {
				$f = $this->siak->get_face_cetak($nik);
				}else{
				$f = [];	
				}	
			}
			
			$data = array(
		 		"stitle"=>'Perbaikan Biometric Local',
		 		"mtitle"=>'Perbaikan Biometric Local',
		 		"my_url"=>'repair_local_biometric',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"face"=>$f,
		 		"option"=>$bo,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Perbaikan Biometric Local',
		 		"mtitle"=>'Perbaikan Biometric Local',
		 		"my_url"=>'repair_local_biometric',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('Siak_hapus_rekam_local/index',$data);
		
	}
	public function delete_biometric_cetak() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$j = $this->siak->get_data_cetak($nik,$this->session->userdata(S_NO_KEC));
			if($j > 0){
				$this->siak->do_hist_delete($nik,$this->session->userdata(S_USER_ID),$this->input->ip_address(),'BIOMETRIC CETAK');
				$this->siak->do_delete_cetak_full($nik);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Bersihkan";
        		echo json_encode($data);
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Bersihkan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_biometric_rekam() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$j = $this->siak->get_data_rekam($nik,$this->session->userdata(S_NO_KEC));
			if($j > 0){
				$this->siak->do_hist_delete($nik,$this->session->userdata(S_USER_ID),$this->input->ip_address(),'BIOMETRIC REKAM');
				$this->siak->do_delete_rekam_full($nik);
				$this->siak->do_delete_cetak_full($nik);
				$this->siak->do_delete_demographics_all($nik);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Bersihkan";
        		echo json_encode($data);
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Bersihkan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function restore_bio_wni()
	{
			$menu_id = 113;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
				$r = [];
				$r_pindah = [];
				$r_mati = [];
				$r_kk = [];
				$j = $this->siak->count_restore_cek_siak($nik);
				if ($j > 0) {
					$r = $this->siak->get_restore_cek_siak($nik);
					$r_kk = $this->siak->get_restore_cek_wni_kk($nik);
				}else{
					$j = $this->siak->count_restore_cek_delete($nik);
					if ($j > 0) {
					$r = $this->siak->get_restore_cek_delete($nik);
					$r_kk = $this->siak->get_restore_cek_delete_kk($nik);
					$r_pindah = $this->siak->get_history_pindah($nik);
					$r_mati = $this->siak->get_history_kematian($nik);
					}
				}
				
			
			$data = array(
		 		"stitle"=>'Restore Biodata Wni',
		 		"mtitle"=>'Restore Biodata Wni',
		 		"my_url"=>'Restore',
		 		"type_tgl"=>'Tanggal',
		 		"pindah"=>$r_pindah,
		 		"mati"=>$r_mati,
		 		"data_kk"=>$r_kk,
		 		"data"=>$r,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else if($this->input->get('nik') != null){
			$nik = $this->input->get('nik');
				$r = [];
				$r_pindah = [];
				$r_mati = [];
				$r_kk = [];
				$j = $this->siak->count_restore_cek_siak($nik);
				if ($j > 0) {
					$r = $this->siak->get_restore_cek_siak($nik);
					$r_kk = $this->siak->get_restore_cek_wni_kk($nik);
				}else{
					$j = $this->siak->count_restore_cek_delete($nik);
					if ($j > 0) {
					$r = $this->siak->get_restore_cek_delete($nik);
					$r_kk = $this->siak->get_restore_cek_delete_kk($nik);
					$r_pindah = $this->siak->get_history_pindah($nik);
					$r_mati = $this->siak->get_history_kematian($nik);
					}
				}
				
			
			$data = array(
		 		"stitle"=>'Restore Biodata Wni',
		 		"mtitle"=>'Restore Biodata Wni',
		 		"my_url"=>'Restore',
		 		"type_tgl"=>'Tanggal',
		 		"pindah"=>$r_pindah,
		 		"mati"=>$r_mati,
		 		"data_kk"=>$r_kk,
		 		"data"=>$r,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else if($this->input->post('no_kk') != null){
			$no_kk = $this->input->post('no_kk');
				$r = [];
				$r_pindah = [];
				$r_mati = [];
				$r_kk = [];
				$j = $this->siak->count_restore_cek_siak_no_kk($no_kk);
				if ($j > 0) {
					$r_kk = $this->siak->get_restore_cek_wni_nokk($no_kk);
				}else{
					$j = $this->siak->count_restore_cek_delete_no_kk($no_kk);
					if ($j > 0) {
					$r_kk = $this->siak->get_restore_cek_delete_nokk($no_kk);
					}
				}
				
			
			$data = array(
		 		"stitle"=>'Restore Biodata Wni',
		 		"mtitle"=>'Restore Biodata Wni',
		 		"my_url"=>'Restore',
		 		"type_tgl"=>'Tanggal',
		 		"pindah"=>$r_pindah,
		 		"mati"=>$r_mati,
		 		"data_kk"=>$r_kk,
		 		"data"=>$r,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Restore Biodata Wni',
		 		"mtitle"=>'Restore Biodata Wni',
		 		"my_url"=>'Restore',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('Siak_restore_bio_wni/index',$data);
		
	}
	public function restore_biodata_wni() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$no_kk = $this->input->post('no_kk');
			$stat_hbkel = ($this->input->post('stat_hbkel') == 'KEPALA KELUARGA') ? 1 : 0;
			$j = $this->siak->count_restore_cek_delete($nik);
			if($j > 0){
				if($stat_hbkel > 0){
				$this->siak->do_hist_restore($nik,$no_kk,$this->session->userdata(S_USER_ID),$this->input->ip_address(),($stat_hbkel == 1) ? "RESTORE WNI KEPALA KELUARGA" : "RESTORE WNI ANGGOTA KELUARGA");
				$this->siak->do_restore($nik,$no_kk,$stat_hbkel);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Restore";
        		echo json_encode($data);
        		}else{
        			$j = $this->siak->count_restore_cek_kk($nik);
        			if($j > 0){
						$this->siak->do_hist_restore($nik,$no_kk,$this->session->userdata(S_USER_ID),$this->input->ip_address(),($stat_hbkel == 1) ? "RESTORE WNI KEPALA KELUARGA" : "RESTORE WNI ANGGOTA KELUARGA");
						$this->siak->do_restore($nik,$no_kk,$stat_hbkel);
						$data["success"] = TRUE;
						$data["is_done"] = 0;
		        		$data["message"] = "Data Berhasil Di Restore";
		        		echo json_encode($data);
        			}else{
        				$data["success"] = FALSE;
						$data["is_done"] = 1;
        				$data["message"] = "Data Gagal Di Restore, Karena Tidak Memiliki Kepala Keluarga atau Kepala Keluarga Tidak Aktif Di Nomor KK Tujuan !";
        				echo json_encode($data);
        			}
        		}
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Restore";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function repair_biodata_wni() 
	{
		if($this->input->post('nik_duplicate') != null){
			$nik_duplicate = $this->input->post('nik_duplicate');
			$nik_single = $this->input->post('nik_single');
			$status_ektp_duplicate = $this->input->post('status_ektp_duplicate');
			$status_ektp_single = $this->input->post('status_ektp_single');
			$nama_duplicate = $this->input->post('nama_duplicate');
			$nama_single = $this->input->post('nama_single');
			
			$j = $this->siak->count_repair_cek($nik_duplicate);
			if($j > 0){
				$j = $this->siak->count_repair_cek_delete($nik_single);
				if($j > 0){
				$this->siak->do_hist_repair($nik_duplicate,$nik_single,$status_ektp_duplicate,$status_ektp_single,$nama_duplicate,$nama_single,$this->session->userdata(S_USER_ID),$this->input->ip_address());
					$this->siak->do_repair($nik_duplicate,$nik_single);
					$data["success"] = TRUE;
					$data["is_done"] = 0;
	        		$data["message"] = "Data Berhasil Di Sesuaikan";
	        		echo json_encode($data);
        		}else{
					$data["success"] = FALSE;
					$data["is_done"] = 1;
	        		$data["message"] = "Data Gagal Di Sesuaikan";
	        		echo json_encode($data);
				}
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Sesuaikan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function restore_biodata_pindah_wni() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$no_kk = $this->input->post('no_kk');
			$stat_hbkel = ($this->input->post('stat_hbkel') == 'KEPALA KELUARGA') ? 1 : 0;
			$j = $this->siak->count_restore_cek_delete($nik);
			if($j > 0){
				if($stat_hbkel > 0){
				$this->siak->do_hist_restore($nik,$no_kk,$this->session->userdata(S_USER_ID),$this->input->ip_address(),($stat_hbkel == 1) ? "RESTORE WNI PINDAH KEPALA KELUARGA" : "RESTORE WNI PINDAH ANGGOTA KELUARGA");
				$this->siak->do_restore($nik,$no_kk,$stat_hbkel);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Restore";
        		echo json_encode($data);
        		}else{
        			$j = $this->siak->count_restore_cek_kk($nik);
        			if($j > 0){
						$this->siak->do_hist_restore($nik,$no_kk,$this->session->userdata(S_USER_ID),$this->input->ip_address(),($stat_hbkel == 1) ? "RESTORE WNI PINDAH KEPALA KELUARGA" : "RESTORE WNI PINDAH ANGGOTA KELUARGA");
						$this->siak->do_restore($nik,$no_kk,$stat_hbkel);
						$data["success"] = TRUE;
						$data["is_done"] = 0;
		        		$data["message"] = "Data Berhasil Di Restore";
		        		echo json_encode($data);
        			}else{
        				$data["success"] = FALSE;
						$data["is_done"] = 1;
        				$data["message"] = "Data Gagal Di Restore, Karena Tidak Memiliki Kepala Keluarga atau Kepala Keluarga Tidak Aktif Di Nomor KK Tujuan !";
        				echo json_encode($data);
        			}
        		}
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Restore";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}

	public function delete_biodata_wni() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$no_kk = $this->input->post('no_kk');
			$j = $this->siak->count_restore_cek_siak($nik);
			$option = "DELETE WNI ANGGOTA KELUARGA";
			if($j > 0){
				$this->siak->do_hist_delete_wni($nik,$no_kk,$this->session->userdata(S_USER_ID),$this->input->ip_address(),$option);
				$this->siak->do_delete_wni($nik);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Delete";
        		echo json_encode($data);
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Delete";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function cek_no_kk_baru() 
	{
		if($this->input->post('no_kk_baru') != null){
			$no_kk_baru = $this->input->post('no_kk_baru');
			$j = $this->siak->count_no_kk_baru($no_kk_baru);
			if($j > 0){
				$r = $this->siak->get_no_kk_baru($no_kk_baru);
				$element = [];
				$element["no_kk"] =  $r[0]->NO_KK;
				$element["nama_kep"] =  $r[0]->NAMA_KEP;
				$element["no_prop"] =  $r[0]->NO_PROP;
				$element["no_kab"] =  $r[0]->NO_KAB;
				$element["no_kec"] =  $r[0]->NO_KEC;
				$element["no_kel"] =  $r[0]->NO_KEL;
				$element["rw"] =  $r[0]->RW;
				$element["rt"] =  $r[0]->RT;
				$element["alamat"] =  $r[0]->ALAMAT;
				$data["data"] = $element;
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Nomor KK Baru Ditemukan";
        		echo json_encode($data);
			}else{
				$element = [];
				$data["data"] = $element;
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Nomor KK Baru Tidak Ditemukan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function change_no_kk() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$no_kk_lama = $this->input->post('no_kk_lama');
			$no_kec_lama = $this->input->post('no_kec_lama');
			$no_kel_lama = $this->input->post('no_kel_lama');
			$no_rw_lama = $this->input->post('no_rw_lama');
			$no_rt_lama = $this->input->post('no_rt_lama');
			$alamat_lama = $this->input->post('alamat_lama');
			$no_kk_baru = $this->input->post('no_kk_baru');
			$no_kec_baru = $this->input->post('no_kec_baru');
			$no_kel_baru = $this->input->post('no_kel_baru');
			$no_rw_baru = $this->input->post('no_rw_baru');
			$no_rt_baru = $this->input->post('no_rt_baru');
			$alamat_baru = $this->input->post('alamat_baru');
			
			$j = $this->siak->count_restore_cek_delete($nik);
			if ($j > 0) {
				$this->siak->do_hist_change_kk($nik,$no_kk_lama,$no_kec_lama,$no_kel_lama,$no_rw_lama,$no_rt_lama,$alamat_lama,$no_kk_baru,$no_kec_baru,$no_kel_baru,$no_rw_baru,$no_rt_baru,$alamat_baru,$this->session->userdata(S_USER_ID),$this->input->ip_address());
				$this->siak->do_change_kk($nik,$no_kk_baru,$no_kec_baru,$no_kel_baru);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Nomor KK Berhasil Di Rubah";
        		echo json_encode($data);
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Nomor KK Gagal Di Rubah";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function get_js_kk(){
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
				$j = $this->siak->count_restore_cek_siak($nik);
				if ($j > 0) {
					$data = $this->siak->get_restore_cek_wni_kk($nik);
				}else{
					$j = $this->siak->count_restore_cek_delete($nik);
					if ($j > 0) {
						$data = $this->siak->get_restore_cek_delete_kk($nik);
					}
				}
			echo json_encode($data);
		}
	}
	public function repair_bio_wni()
	{
			$menu_id = 116;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('nik') != null && $this->input->post('nik_single') != null){
			$nik = $this->input->post('nik');
			$nik_single = $this->input->post('nik_single');

				$r = [];
				$r_pindah = [];
				$r_mati = [];
				$j = $this->siak->count_restore_cek_siak($nik);
				if ($j > 0) {
					$r = $this->siak->get_restore_cek_siak($nik);
				}else{
					$j = $this->siak->count_restore_cek_delete($nik);
					if ($j > 0) {
					$r = $this->siak->get_restore_cek_delete($nik);
					$r_pindah = $this->siak->get_history_pindah($nik);
					$r_mati = $this->siak->get_history_kematian($nik);
					}
				}
				$sr = [];
				$sr_pindah = [];
				$sr_mati = [];
				$sj = $this->siak->count_restore_cek_siak($nik_single);
				if ($sj > 0) {
					$sr = $this->siak->get_restore_cek_siak($nik_single);
				}else{
					$sj = $this->siak->count_restore_cek_delete($nik_single);
					if ($sj > 0) {
					$sr = $this->siak->get_restore_cek_delete($nik_single);
					$sr_pindah = $this->siak->get_history_pindah($nik_single);
					$sr_mati = $this->siak->get_history_kematian($nik_single);
					}
				}
			
			$data = array(
		 		"stitle"=>'Penyesuaian Biodata Wni',
		 		"mtitle"=>'Penyesuaian Biodata Wni',
		 		"my_url"=>'Penyesuaian',
		 		"type_tgl"=>'Tanggal',
		 		"pindah"=>$r_pindah,
		 		"mati"=>$r_mati,
		 		"data"=>$r,
		 		"spindah"=>$sr_pindah,
		 		"smati"=>$sr_mati,
		 		"sdata"=>$sr,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Penyesuaian Biodata Wni',
		 		"mtitle"=>'Penyesuaian Biodata Wni',
		 		"my_url"=>'Penyesuaian',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('Siak_repair_bio_wni/index',$data);
		
	}
	public function do_perbaikan_data()
	{
			$menu_id = 136;
			$is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
			if ($is_akses == 0){
				redirect('404Notfound','refresh');
			}
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			/*$rs = $this->act->get_all_activity($this->session->userdata(S_USER_ID));
			$rbc = $this->act->get_all_bcard($this->session->userdata(S_USER_ID));	
			$jbe = $this->act->get_count_benroll($this->session->userdata(S_USER_ID));*/
			$data = array(
		 		"stitle"=>'Perbaikan Data',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		/*"data_siak"=>$rs,
		 		"data_bcard"=>$rbc,
		 		"data_benroll"=>$rbe,*/
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Perbaikan_data/index',$data);
		
	}
	public function list_perbaikan_data()
	{
			$menu_id = 136;
			$is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
			if ($is_akses == 0){
				redirect('404Notfound','refresh');
			}
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			/*$rs = $this->act->get_all_activity($this->session->userdata(S_USER_ID));
			$rbc = $this->act->get_all_bcard($this->session->userdata(S_USER_ID));	
			$jbe = $this->act->get_count_benroll($this->session->userdata(S_USER_ID));*/
			$data = array(
		 		"stitle"=>'List Perbaikan Data',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		/*"data_siak"=>$rs,
		 		"data_bcard"=>$rbc,
		 		"data_benroll"=>$rbe,*/
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Perbaikan_list_data/index',$data);
		
	}
	public function send_request() 
	{
		header("Content-Type: application/json", true);
		if($this->input->post('request_id') != null){
			$request_id =  $this->input->post('request_id');
	    	$keterangan =  strtoupper($this->clean($this->input->post('keterangan')));
			$user_id =  $this->input->post('user_id');
				$this->siak->insert_helpdesk_request($request_id,$keterangan,$user_id);
				$output = array(
	    			"message_type"=>1,
	    			"request_id"=>$request_id,
	    			"message"=> "Terima Kasih Sudah Mengajukan Request Anda <span class='fa fa-smile-o'></span>"
	    		);
			echo json_encode($output);
		}

	}
	public function send_request_aprove() 
	{
		header("Content-Type: application/json", true);
		if($this->input->post('aprove_seq_id') != null){
			$aprove_seq_id =  $this->input->post('aprove_seq_id');
	    	$balasan_aprove =  strtoupper($this->clean($this->input->post('balasan_aprove')));
			$user_id =  $this->input->post('user_id');
				$this->siak->aprove_helpdesk_request($aprove_seq_id,$balasan_aprove,$user_id);
				$output = array(
	    			"message_type"=>1,
	    			"aprove_seq_id"=>$aprove_seq_id,
	    			"message"=> "Request Telah Di Aprove <span class='fa fa-smile-o'></span>"
	    		);
			echo json_encode($output);
		}

	}
	public function send_request_reject() 
	{
		header("Content-Type: application/json", true);
		if($this->input->post('reject_seq_id') != null){
			$reject_seq_id =  $this->input->post('reject_seq_id');
	    	$balasan_reject =  strtoupper($this->clean($this->input->post('balasan_reject')));
			$user_id =  $this->input->post('user_id');
				$this->siak->reject_helpdesk_request($reject_seq_id,$balasan_reject,$user_id);
				$output = array(
	    			"message_type"=>1,
	    			"reject_seq_id"=>$reject_seq_id,
	    			"message"=> "Request Telah Di Reject <span class='fa fa-smile-o'></span>"
	    		);
			echo json_encode($output);
		}

	}
	public function request_pending(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->siak->request_pending($user_id);
        echo json_encode($output);	
	}
	public function request_success(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->siak->request_success($user_id);
        echo json_encode($output);	
	}
	public function request_reject(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->siak->request_reject($user_id);
        echo json_encode($output);	
	}
	public function list_request_pending(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->siak->list_request_pending($user_id);
        echo json_encode($output);	
	}
	public function list_request_success(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->siak->list_request_success($user_id);
        echo json_encode($output);	
	}
	public function list_request_reject(){
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$output = $this->siak->list_request_reject($user_id);
        echo json_encode($output);	
	}
	public function get_request(){
		header('Content-type: application/json');
		$seq_id = $this->input->post('seq_id');
		$output = $this->siak->get_request($seq_id);
        echo json_encode($output);	
	}
	public function clean($string) {
	   	$string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
	   	return preg_replace('/[^A-Za-z0-9\- ,.]/', '', $string); // Removes special chars.
	}
}