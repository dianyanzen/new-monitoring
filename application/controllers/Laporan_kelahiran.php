<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_kelahiran extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');	
		$this->load->model('M_Laporankelahiran','lhr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function index()
	{
			$menu_id = 31;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$jns_lap = $this->input->post('jns_lap');
			$usia_lap = $this->input->post('usia_lap');
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			$title_lap = '';
			if($jns_lap == 1){
					if($usia_lap == 1){
						$title_lap = ' LU 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' LU DI ATAS 18';
					}else{
						$title_lap = ' LU';
					}
			}else if ($jns_lap == 2){
				if($usia_lap == 1){
						$title_lap = ' LT 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' LT DI ATAS 18';
					}else{
						$title_lap = ' LT';
					}
			}else{
				if($usia_lap == 1){
						$title_lap = ' 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' DI ATAS 18';
					}
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower($this->shr->get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower($this->shr->get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->lhr->get_data_kelahiran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel,$jns_lap,$usia_lap);
			$j = $this->lhr->get_jumlah_kelahiran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel,$jns_lap,$usia_lap);
			$data = array(
		 		"stitle"=>'Laporan Akta Kelahiran'.$title_lap.''.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Akta Kelahiran',
		 		"my_url"=>'Laporan_kelahiran',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Akta Kelahiran',
		 		"mtitle"=>'Laporan Akta Kelahiran',
		 		"my_url"=>'Laporan_kelahiran',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			$this->load->view('Laporan_kelahiran/index',$data);
	}
	
	

}
