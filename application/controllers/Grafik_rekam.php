<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik_rekam extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');	
		$this->load->model('M_laporanrekam','rkm');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function index()
	{
			$menu_id = 51;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower($this->shr->get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower($this->shr->get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->rkm->get_data_rekam($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$j = $this->rkm->get_jumlah_rekam($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Perekaman'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Perekaman',
		 		"my_url"=>'Grafik_rekam',
		 		"type_tgl"=>'Perekaman',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Perekaman',
		 		"mtitle"=>'Grafik Perekaman',
		 		"my_url"=>'Grafik_rekam',
		 		"type_tgl"=>'Perekaman',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			$this->load->view('Grafik/index',$data);
	}
	
}
