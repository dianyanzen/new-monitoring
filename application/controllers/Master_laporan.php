<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_laporan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');	
		$this->load->model('M_Masterlaporan','mst');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function index()
	{
			$menu_id = 42;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$t = $this->shr->get_dkb();
			$r = [];
			if($this->input->post('mlap') != null && $this->input->post('mlap') != 0){
			$mlap = $this->input->post('mlap');
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower($this->shr->get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower($this->shr->get_nama_kel($no_kec,$no_kel)));
			}
			$desc_lap = '';
			if($mlap == 1){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kelamin';
			$r = $this->mst->get_laporan_kelamin($no_kec,$no_kel);
			}else if($mlap == 2){
			$desc_lap = ' Jumlah Penduduk Menurut Usia';
			$r = $this->mst->get_laporan_umur($no_kec,$no_kel);
			}else if($mlap == 3){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Laki-Laki';
			$r = $this->mst->get_laporan_umur_laki_laki($no_kec,$no_kel);
			}else if($mlap == 4){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Perempuan';
			$r = $this->mst->get_laporan_umur_perempuan($no_kec,$no_kel);
			}else if($mlap == 5){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut Jenis Kelamin';
			$r = $this->mst->get_laporan_kk_kelamin($no_kec,$no_kel);
			}else if($mlap == 6){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan';
			$r = $this->mst->get_laporan_pendidikan($no_kec,$no_kel);
			}else if($mlap == 7){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan Laki-Laki';
			$r = $this->mst->get_laporan_pendidikan_laki_laki($no_kec,$no_kel);
			}else if($mlap == 8){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan Perempuan';
			$r = $this->mst->get_laporan_pendidikan_perempuan($no_kec,$no_kel);
			}else if($mlap == 9){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan';
			$r = $this->mst->get_laporan_pekerjaan($no_kec,$no_kel);
			}else if($mlap == 10){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan Laki-Laki';
			$r = $this->mst->get_laporan_pekerjaan_laki_laki($no_kec,$no_kel);
			}else if($mlap == 11){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan Perempuan';
			$r = $this->mst->get_laporan_pekerjaan_perempuan($no_kec,$no_kel);
			}else if($mlap == 12){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin';
			$r = $this->mst->get_laporan_status_kawin($no_kec,$no_kel);
			}else if($mlap == 13){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin Laki-Laki';
			$r = $this->mst->get_laporan_status_kawin_laki_laki($no_kec,$no_kel);
			}else if($mlap == 14){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin Perempuan';
			$r = $this->mst->get_laporan_status_kawin_perempuan($no_kec,$no_kel);
			}else if($mlap == 15){
			$desc_lap = ' Jumlah Penduduk Menurut Agama';
			$r = $this->mst->get_laporan_agama($no_kec,$no_kel);
			}else if($mlap == 16){
			$desc_lap = ' Jumlah Penduduk Menurut Agama Laki-Laki';
			$r = $this->mst->get_laporan_agama_laki_laki($no_kec,$no_kel);
			}else if($mlap == 17){
			$desc_lap = ' Jumlah Penduduk Menurut Agama Perempuan';
			$r = $this->mst->get_laporan_agama_perempuan($no_kec,$no_kel);
			}else if($mlap == 18){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah';
			$r = $this->mst->get_laporan_gol_drh($no_kec,$no_kel);
			}else if($mlap == 19){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah Laki-Laki';
			$r = $this->mst->get_laporan_gol_drh_laki_laki($no_kec,$no_kel);
			}else if($mlap == 20){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah Perempuan';
			$r = $this->mst->get_laporan_gol_drh_perempuan($no_kec,$no_kel);
			}else if($mlap == 21){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas)';
			$r = $this->mst->get_laporan_disabilitas($no_kec,$no_kel);
			}else if($mlap == 22){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas) Laki-Laki';
			$r = $this->mst->get_laporan_disabilitas_laki_laki($no_kec,$no_kel);
			}else if($mlap == 23){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas) Perempuan';
			$r = $this->mst->get_laporan_disabilitas_perempuan($no_kec,$no_kel);
			}else if($mlap == 24){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia';
			$r = $this->mst->get_laporan_umur_lansia($no_kec,$no_kel);
			}else if($mlap == 25){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia Laki-Laki';
			$r = $this->mst->get_laporan_umur_lansia_laki_laki($no_kec,$no_kel);
			}else if($mlap == 26){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia Perempuan';
			$r = $this->mst->get_laporan_umur_lansia_perempuan($no_kec,$no_kel);
			}else if($mlap == 27){
			$desc_lap = ' Jumlah Penduduk Menurut Wajib Ktp';
			$r = $this->mst->get_laporan_wajib_ktp($no_kec,$no_kel);
			}else if($mlap == 28){
			$desc_lap = ' Jumlah Penduduk Menurut Wajib Ktp';
			$r = $this->mst->get_laporan_non_ktp($no_kec,$no_kel);
			}

			$data = array(
		 		"stitle"=>'Laporan '.$t.''.$desc_lap.''.$title_wil,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_laporan',
		 		"jenis_lap"=>$mlap,
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan '.$t,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_laporan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			$this->load->view('Master_laporan/index',$data);
	}
	
}
