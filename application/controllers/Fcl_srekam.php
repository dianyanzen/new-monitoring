<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fcl_srekam extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Prr','prr');	
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}	
	}
	public function index()
	{		$menu_id = 72;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('no_kec') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = 0;
			$r = $this->prr->get_list_perekaman_fcl($tgl_start,$tgl_end,$no_kec,$no_kel);
			$j = $this->prr->count_list_perekaman_fcl($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Perekaman Gerai',
		 		"mtitle"=>'List Perekaman Gerai'.$tgl,
		 		"my_url"=>'FclRekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Perekaman Gerai',
		 		"mtitle"=>'List Perekaman Gerai',
		 		"my_url"=>'FclRekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('fclsperekaman/index',$data);
		
		
	}
}