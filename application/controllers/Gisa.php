<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gisa extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Gisa','gisa');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}		
	}
	public function index()
	{		$menu_id = 112;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('no_kec') != null && $this->input->post('no_kel') != null && $this->input->post('no_rw') != null && $this->input->post('no_rt') != null ){
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$no_rw = $this->input->post('no_rw');
			$no_rt = $this->input->post('no_rt');
			$r = $this->gisa->get_kk($no_kec,$no_kel,$no_rw,$no_rt);
			$j = $this->gisa->get_count_kk($no_kec,$no_kel,$no_rw,$no_rt);
			$data = array(
		 		"stitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"mtitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"my_url"=>'Gisa',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
		 		"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else if($this->input->get('no_kec') != null && $this->input->get('no_kel') != null && $this->input->get('no_rw') != null && $this->input->get('no_rt') != null ){
			$no_kec = $this->input->get('no_kec');
			$no_kel = $this->input->get('no_kel');
			$no_rw = $this->input->get('no_rw');
			$no_rt = $this->input->get('no_rt');
			$r = $this->gisa->get_kk($no_kec,$no_kel,$no_rw,$no_rt);
			$j = $this->gisa->get_count_kk($no_kec,$no_kel,$no_rw,$no_rt);
			$data = array(
		 		"stitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"mtitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"my_url"=>'Gisa',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
		 		"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"mtitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"my_url"=>'Gisa',
		 		"menu"=>$menu,
		 		"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('Gisa/index',$data);
	}

	public function cek_detail()
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if (!empty($_GET['no_kk'])){
			$no_kk = $_GET['no_kk'];
			// echo $no_kk;
			// die;
			$j = $this->gisa->get_count_detail_kk($no_kk);
				if ($j>0){
				$k = $this->gisa->get_detail_kk($no_kk);
				$h = $this->gisa->get_detail_anggota_kk($no_kk);
				$data = array(
			 		"stitle"=>'Detail Kartu Keluarga',
			 		"sno_kk"=> $no_kk,
			 		"menu"=>$menu,
			 		"user_id"=>$this->session->userdata(S_USER_ID),
			 		"user_nik"=>$this->session->userdata(S_NIK),
			 		"data_kk"=>$k,
			 		"data_nik"=>$h,
			 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
			 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
	    		);
				$this->load->view('gisa_detail/index',$data);
				}else{
					redirect('/','refresh');
				}
			}else{
				redirect('/','refresh');
			}
	    
	}
	public function do_edit()
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if (!empty($_GET['nik'])){
			$nik = $_GET['nik'];
			// echo $nik;
			// die;
			$j = $this->gisa->get_count_detail_nik($nik);
				if ($j>0){
				$d = $this->gisa->get_detail_nik($nik);
				$data = array(
			 		"stitle"=>'Edit #GISA',
			 		"snik"=> 'Edit #GISA Nik '.$nik,
			 		"nik"=> $nik,
			 		"menu"=>$menu,
			 		"user_id"=>$this->session->userdata(S_USER_ID),
			 		"user_nik"=>$this->session->userdata(S_NIK),
			 		"data_nik"=>$d,
			 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
			 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
	    		);
				$this->load->view('gisa_edit/index',$data);
				}else{
					redirect('/','refresh');
				}
			}else{
				redirect('/','refresh');
			}
	    
	}
	public function getdata() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$j = $this->gisa->get_count_detail_nik($nik);
			if($j > 0){
				$r = $this->gisa->get_detail_nik($nik);
				$data["akta_lhr"] = $r[0]->STATUS_AKTA_LHR;
				$data["no_akta_lhr"] = $r[0]->NO_AKTA_LHR;
        		$data["akta_kwn"] = $r[0]->STATUS_AKTA_KWN;
        		$data["no_akta_kwn"] = $r[0]->NO_AKTA_KWN;
        		$data["tgl_kwn"] = (!empty($r[0]->TGL_KWN)) ? $r[0]->TGL_KWN : '';
        		$data["akta_crai"] = $r[0]->STATUS_AKTA_CRAI;
        		$data["no_akta_crai"] = $r[0]->NO_AKTA_CRAI;
        		$data["tgl_crai"] = (!empty($r[0]->TGL_CRAI)) ? $r[0]->TGL_CRAI : '';
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}else{
				$data["akta_lhr"] = "";
				$data["no_akta_lhr"] = "";
        		$data["akta_kwn"] = "";
        		$data["no_akta_kwn"] = "";
        		$data["tgl_kwn"] = "";
        		$data["akta_crai"] = "";
        		$data["no_akta_crai"] = "";
        		$data["tgl_crai"] = "";
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function dosave() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$akta_lhr = $this->input->post('akta_lhr');
			$no_akta_lhr = (!empty($this->input->post('no_akta_lhr'))) ? $this->input->post('no_akta_lhr') : '-';
			$akta_kwn = (!empty($this->input->post('akta_kwn'))) ? $this->input->post('akta_kwn') : '-';
			$no_akta_kwn = (!empty($this->input->post('no_akta_kwn'))) ? $this->input->post('no_akta_kwn') : '-';
			$akta_crai = (!empty($this->input->post('akta_crai'))) ? $this->input->post('akta_crai') : '-';
			$no_akta_crai = (!empty($this->input->post('no_akta_crai'))) ? $this->input->post('no_akta_crai') : '-';
			$user_name = $this->input->post('user_name');
			$j = $this->gisa->get_count_detail_nik($nik);
			if($j > 0){
				$this->gisa->insert_hist_gisa($nik,$akta_lhr,$no_akta_lhr,$akta_kwn,$no_akta_kwn,$akta_crai,$no_akta_crai,$user_name);
				$this->gisa->update_gisa($nik,$akta_lhr,$no_akta_lhr,$akta_kwn,$no_akta_kwn,$akta_crai,$no_akta_crai);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		echo json_encode($data);
			}else{
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Tidak Ditemukan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function export() {
		if($this->input->post('no_kks') != null){
			$no_kk = $this->input->post('no_kks');
			if (substr($no_kk, 0, 1) === ','){
				$no_kk = ltrim($no_kk, ',');
			}
			$fileName = 'GISA_'.strtoupper($this->session->userdata(S_USER_ID)).''.time().'.xlsx';  
		// load excel library
        $this->load->library('excel');
        $data_rekam =  $this->gisa->export_gisa($no_kk);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $style_aligment = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
    	);
    	$style_color = array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '01B0F1')
	        )
    	);
		
    	$objPHPExcel->getActiveSheet()->getStyle("C1:G5")->applyFromArray($style_aligment);
    	$objPHPExcel->getActiveSheet()->getStyle("A9:I9")->applyFromArray($style_color);
        $objPHPExcel->getActiveSheet()->setTitle('Data Gisa');
        // set Header
        $objPHPExcel->getActiveSheet()->mergeCells('B1:G1');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', "DAFTAR GISA ".strtoupper($this->session->userdata(S_USER_ID)));

		$objPHPExcel->getActiveSheet()->mergeCells('C2:G2');
		$objPHPExcel->getActiveSheet()->setCellValue('C2', "DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL");

		$objPHPExcel->getActiveSheet()->mergeCells('C3:G3');
		$objPHPExcel->getActiveSheet()->setCellValue('C3', "KOTA BANDUNG");

		$objPHPExcel->getActiveSheet()->mergeCells('C4:G4');
		$objPHPExcel->getActiveSheet()->setCellValue('C4', "-YZ-");

		$objPHPExcel->getActiveSheet()->getStyle('C1:G5')->getFont()->setName('Arial');
		$objPHPExcel->getActiveSheet()->getStyle('C1:G5')->getFont()->setSize(14);
		$objPHPExcel->getActiveSheet()->getStyle('C1:G5')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A9:I9')->getFont()->setBold(true);

		// untuk sub judul
		$objPHPExcel->getActiveSheet()->setCellValue('B6', "Kota : Bandung");
		$objPHPExcel->getActiveSheet()->setCellValue('B7', "Provinsi : Jawa Barat");
	
		$objPHPExcel->getActiveSheet()->getStyle('B6:I7')->getFont()->setName('Arial');
		$objPHPExcel->getActiveSheet()->getStyle('B6:I7')->getFont()->setSize(9);
		
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo_disduk');
		$objDrawing->setDescription('Logo_disduk');
		// $logo = base_path() . '/images/logo.png';
		$objDrawing->setPath('assets/plugins/images/pemkot.png');
		$objDrawing->setCoordinates('B1');
		$objDrawing->setOffsetX(30); 
		$objDrawing->setOffsetY(10); 
		$objDrawing->setHeight(100);
		$objDrawing->setWidth(100);
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		

		$objPHPExcel->getActiveSheet()->SetCellValue('A9', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B9', 'NO KK');
        $objPHPExcel->getActiveSheet()->SetCellValue('C9', 'NAMA KEPALA KELUARGA');
        $objPHPExcel->getActiveSheet()->SetCellValue('D9', 'STATUS PERKAWINAN');
        $objPHPExcel->getActiveSheet()->SetCellValue('E9', 'ALAMAT');
        $objPHPExcel->getActiveSheet()->SetCellValue('F9', 'RW');       
        $objPHPExcel->getActiveSheet()->SetCellValue('G9', 'RT');       
        $objPHPExcel->getActiveSheet()->SetCellValue('H9', 'NAMA KECAMATAN');       
        $objPHPExcel->getActiveSheet()->SetCellValue('I9', 'NAMA KELURAHAN');       

        // set Row
        $rowCount = 10;
        $i = 0;
        foreach ($data_rekam as $val) 
        {
        	$i++;
        	$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $i);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $val->NO_KK);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $val->NAMA_KEP);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $val->STAT_KWN);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $val->ALAMAT);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $val->NO_RW);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $val->NO_RT);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $val->NAMA_KEC);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $val->NAMA_KEL);
            $rowCount++;
        }
        $endrow = $rowCount-1;
        $detailrow = $rowCount+1;
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$detailrow.':J'.$detailrow);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$detailrow, "Sumber : Aplikasi Monitoring Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung, Copyright © ".date('Y')." DianYanzen (PIAK)");
 		$styleborder = array(
		      'borders' => array(
		          'allborders' => array(
		              'style' => PHPExcel_Style_Border::BORDER_THIN
		          )
		      )
		  );
		$objPHPExcel->getActiveSheet()->getStyle("A9:I". $endrow)->applyFromArray($styleborder);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('excel/'.$fileName);
		// download file
        header("Content-Type: application/vnd.ms-excel");
         redirect(site_url().'excel/'.$fileName); 
		}
		
    }
	
}