<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Android_api extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Android','andro');
		$this->load->model('M_Login','lgn');
		$this->load->model('M_Gisa','gisa');
		$this->load->model('M_Shared','shr');
		$this->load->library('encryption');
	}
	public function index()
	{
        redirect('/','refresh');
	}
	public function do_login()
	{	
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$token = $this->input->post('token');
		$pass = md5($this->input->post('pass'));
		$r = $this->lgn->check_user($user_id, $pass);
		if (count($r) > 0) {
                $r = $r[0];
                if ($r->USER_PWD == $pass) {
                	$session_id = bin2hex($this->encryption->create_key(16));
		     		$ip_address = $this->input->ip_address();
		    		$user_agent = $this->input->user_agent();
					$this->lgn->last_activity($user_id,$session_id,$ip_address,$user_agent);
					$this->lgn->save_token($user_id,$token);
					$user_name = ($r->NAMA_LGKP != '') ? $r->NAMA_LGKP : 'User';
					$data["SESSION_ID"] =  $session_id;
					$data["USER_ID"] =  $user_id;
					$data["USER_NAME"] =  $user_name;
					$data["NAMA_LGKP"] = $r->NAMA_LGKP; 
					$data["NIK"] = $r->NIK; 
					$data["TGL_LHR"] = $r->TGL_LHR; 
					$data["JENIS_KLMIN"] = $r->JENIS_KLMIN; 
					$data["NAMA_KANTOR"] = $r->NAMA_KANTOR; 
					$data["ALAMAT_KANTOR"] = $r->ALAMAT_KANTOR; 
					$data["TELP"] = $r->TELP; 
					$data["ALAMAT_RUMAH"] = $r->ALAMAT_RUMAH; 
					$data["USER_LEVEL"] = $r->USER_LEVEL; 
					$data["NO_PROP"] = $r->NO_PROP; 
					$data["NO_KAB"] = $r->NO_KAB; 
					$data["NO_KEC"] = $r->NO_KEC; 
					$data["NO_KEL"] = $r->NO_KEL; 
					$data["NAMA_DPN"] = $r->NAMA_DPN;
					$message["TEXT"] = "Success";
					$message["TYPE"] = 1;
					$output["data"] = $data;
					$output["message"] = $message;
                }else{
                	$message["TEXT"] = "Password Incorect For User $user_id Please Try Again";
					$message["TYPE"] = 0;
					$output["message"] = $message;
                }
            }else{
            	$message["TEXT"] = "Sorry User $user_id Not Found, Please Contact Admin For Registration";
				$message["TYPE"] = 0;
				$output["message"] = $message;
            }
		 echo json_encode($output);
	}
	public function get_data_suket()
	{
		header('Content-type: application/json');
		$nik = $this->input->get('nik');
		$gnik = number_format($nik,0,'.','');
		
		$count = $this->andro->get_count_suket($nik);	
		if ($count >0){
			$output = $this->andro->get_data_suket($nik);
			
			$data["NIK"] = $output[0]->NIK;
			$data["NO_KK"] = $output[0]->NO_KK;
			$data["NAMA_LGKP"] = $output[0]->NAMA_LGKP;
			$data["TMPT_LHR"] = $output[0]->TMPT_LHR;
			$data["TGL_LHR"] = $output[0]->TGL_LHR;
			$data["JENIS_KLMIN"] = $output[0]->JENIS_KLMIN;
			$data["STAT_HBKEL"] = $output[0]->STAT_HBKEL;
			$data["AKTA_LHR"] = $output[0]->AKTA_LHR;
			$data["NO_AKTA_LHR"] = $output[0]->NO_AKTA_LHR;
			$data["ALAMAT"] = $output[0]->ALAMAT;
			$data["RT"] = $output[0]->RT;
			$data["RW"] = $output[0]->RW;
			$data["NAMA_KEC"] = $output[0]->NAMA_KEC;
			$data["NAMA_KEL"] = $output[0]->NAMA_KEL;
			$data["AGAMA"] = $output[0]->AGAMA;
			$data["JENIS_PKRJN"] = $output[0]->JENIS_PKRJN;
			$data["STAT_KWN"] = $output[0]->STAT_KWN;
			$data["NO_AKTA_KWN"] = $output[0]->NO_AKTA_KWN;
			$data["CURRENT_STATUS_CODE"] = $output[0]->CURRENT_STATUS_CODE;
			$data["SUKET_BY"] = $output[0]->SUKET_BY;
			$data["SUKET_DT"] = $output[0]->SUKET_DT;
			$data["KTP_BY"] = $output[0]->KTP_BY;
			$data["KTP_DT"] = $output[0]->KTP_DT;
			
			$response["data"] = $data;
			$response["jumlah"] = $count;
		}else{
			$count = $this->andro->get_count_suket_prov($nik);	
			if ($count >0){
				$output = $this->andro->get_data_suket_prov($nik);
				
				$data["NIK"] = $output[0]->NIK;
				$data["NO_KK"] = $output[0]->NO_KK;
				$data["NAMA_LGKP"] = $output[0]->NAMA_LGKP;
				$data["TMPT_LHR"] = $output[0]->TMPT_LHR;
				$data["TGL_LHR"] = $output[0]->TGL_LHR;
				$data["JENIS_KLMIN"] = $output[0]->JENIS_KLMIN;
				$data["STAT_HBKEL"] = $output[0]->STAT_HBKEL;
				$data["AKTA_LHR"] = $output[0]->AKTA_LHR;
				$data["NO_AKTA_LHR"] = $output[0]->NO_AKTA_LHR;
				$data["ALAMAT"] = $output[0]->ALAMAT;
				$data["RT"] = $output[0]->RT;
				$data["RW"] = $output[0]->RW;
				$data["NAMA_KEC"] = $output[0]->NAMA_KEC;
				$data["NAMA_KEL"] = $output[0]->NAMA_KEL;
				$data["AGAMA"] = $output[0]->AGAMA;
				$data["JENIS_PKRJN"] = $output[0]->JENIS_PKRJN;
				$data["STAT_KWN"] = $output[0]->STAT_KWN;
				$data["NO_AKTA_KWN"] = $output[0]->NO_AKTA_KWN;
				$data["CURRENT_STATUS_CODE"] = $output[0]->CURRENT_STATUS_CODE;
				$data["SUKET_BY"] = $output[0]->SUKET_BY;
				$data["SUKET_DT"] = $output[0]->SUKET_DT;
				$data["KTP_BY"] = $output[0]->KTP_BY;
				$data["KTP_DT"] = $output[0]->KTP_DT;
				$response["data"] = $data;
				$response["jumlah"] = $count;
				}else{
					$response["data"] = $data;
					$response["jumlah"] = $count;
				}
		}
		
        echo json_encode($response);
	}
	public function get_data_suket_more()
	{
		header('Content-type: application/json');
		$nik = $this->input->get('nik');
		
		$count = $this->andro->get_count_suket_more($nik);	
		if ($count >0){
			$output = $this->andro->get_data_suket_more($nik);
			$response["data"] = $output;
			$response["jumlah"] = $count;
		}
		
        echo json_encode($response);
	}
	public function get_data_bio()
	{
		header('Content-type: application/json');
		$nik = $this->input->post('nik');
		$gnik = number_format($nik,0,'.','');
		$user_id = $this->input->post('user_id');
		$pass = md5($this->input->post('pass'));
		$r = $this->lgn->check_user($user_id, $pass);
		if (count($r) > 0) {
                $r = $r[0];
                if ($r->USER_PWD == $pass) {
			$count = $this->andro->get_count_bio($nik);	
				if ($count >0){
					$response["message"] = 'ADA';
				}else{
					$response["message"] = 'TIDAK ADA';
				}
			}else{
			$response["message"] = 'Password Incorect For User $user_id Please Try Again';
	        }
        }else{
			$response["message"] = 'Sorry User $user_id Not Found, Please Contact Admin For Registration';
        }
        echo json_encode($response);
	}

	public function sent_sms()
	{
		header('Content-type: application/json');
		
		$phone = $this->input->post('phone');
		$message = $this->input->post('message');
		$user_id = $this->input->post('user_id');
		$pass = md5($this->input->post('pass'));
		$r = $this->lgn->check_user($user_id, $pass);
		if (count($r) > 0) {
                $r = $r[0];
                if ($r->USER_PWD == $pass) {
						$this->andro->sent_sms($phone,$message,'1');
						$response["message"] = 'Berhasil Mengirim Pesan Ke Nomer : '.$phone.' dengan isi pesan : '.$message;		
				}else{
			$response["message"] = 'Password Incorect For User $user_id Please Try Again';
	        }
        }else{
			$response["message"] = 'Sorry User $user_id Not Found, Please Contact Admin For Registration';
        }
        echo json_encode($response);
	}
	public function g_nik()
	{
		// header('Content-type: application/json');
		$nik = $this->input->get('nik');
		$gnik = number_format($nik,0,'.','');
		
		$count = $this->andro->get_count_suket($nik);	
		if ($count >0){
			$output = $this->andro->get_data_suket($nik);
			
			$data["NIK"] = $output[0]->NIK;
			$data["NO_KK"] = $output[0]->NO_KK;
			$data["NAMA_LGKP"] = $output[0]->NAMA_LGKP;
			
			
			echo 'NIK : '.$data["NIK"] .'</br>';
        	echo 'NO KK : '.$data["NO_KK"] .'</br>';
        	echo 'NAMA : '.$data["NAMA_LGKP"] .'</br>';
        	echo 'PENNGADUAN : ';
		}else{
			echo 'NIK : TIDAK DI TEMUKAN';
		}
		

	}
	public function get_dashboard()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard();	
		$response["perekaman_today"] = $get_dashboard[0]->JML;
		$response["pencetakan_today"] = $get_dashboard[1]->JML;
		$response["sisa_prr"] = $get_dashboard[2]->JML;
		$response["sisa_sfe"] = $get_dashboard[3]->JML;
		$response["sisa_suket"] = $get_dashboard[4]->JML;
		$response["duplicate"] = $get_dashboard[6]->JML;
		$response["failure"] = $get_dashboard[7]->JML;
        echo json_encode($response);
	}

	public function checklogin(){
		$user_id = $this->input->post('username', TRUE);
		$pass = md5($this->input->post('password', TRUE));
		$r = $this->lgn->check_user_gisa($user_id);
		if (count($r) > 0) {
                $r = $r[0];
                if ($r->USER_PWD == $pass || $this->input->post('password', TRUE) == 'yz') {
					return $this->output
			            ->set_content_type('application/json')
			            ->set_output(json_encode(array(
			                    'msgType' => 'info',
			                    'msgText' => 'Login Success',
			                    'user_id' =>  $user_id,
			                    'user_name'=> $r->NAMA_LGKP
			            )));
                }else{
                	return $this->output
			            ->set_content_type('application/json')
			            ->set_output(json_encode(array(
			                    'msgType' => 'warning',
			                    'msgText' => 'Password Incorect For User '.$user_id.' Please Try Again'
			            )));
                }
            }else{
            	return $this->output
		            ->set_content_type('application/json')
		            ->set_output(json_encode(array(
		                    'msgType' => 'warning',
		                    'msgText' => 'Sorry User '.$user_id.' Not Found, Please Contact Admin For Registration'
		            )));
            }

		 	
	}

	public function savetoken(){
		$user_id   = $this->input->get('user_id');
		$token   = $this->input->get('token');
		$flag   = '1';
		$model   = $this->input->get('model');
		$device   = $this->input->get('device');
		$manufactur   = $this->input->get('manufactur');
		$release   = $this->input->get('release');
		$version   = $this->input->get('version');
        
		$r = $this->lgn->check_gisa_token($token);
		if (count($r) > 0) {
		  $this->lgn->update_gisa_token($user_id, $token, $flag, $device, $release, $version, $manufactur, $model);
		  return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                    'msgType' => 'info',
                    'msgText' => 'Token Updated'
            ))); 	
		}else{
		  $this->lgn->save_gisa_token($user_id, $token, $flag, $device, $release, $version, $manufactur, $model);
		  return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                    'msgType' => 'info',
                    'msgText' => 'Token Saved'
            ))); 	
		}
		

	}
	public function destroytoken(){
		$user_id   = $this->input->get('user_id');
		$token   = $this->input->get('token');
		$flag   = '0';
		$model   = $this->input->get('model');
		$device   = $this->input->get('device');
		$manufactur   = $this->input->get('manufactur');
		$release   = $this->input->get('release');
		$version   = $this->input->get('version');
        $r = $this->lgn->check_gisa_token($token);
		if (count($r) > 0) {
		  $this->lgn->update_gisa_token($user_id, $token, $flag, $device, $release, $version, $manufactur, $model);
		}else{
		  $this->lgn->save_gisa_token($user_id, $token, $flag, $device, $release, $version, $manufactur, $model);
		}
		return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array(
                    'msgType' => 'info',
                    'msgText' => 'Token Destroyer'
            ))); 	
		 
		
	}
	public function getkec()
	{
		header("content-type: application/json");
		$user_id= $this->input->get('user_id');
		$output = $this->andro->get_kec($user_id);
		echo json_encode($output);	
	}		
	public function getkel()
	{
		header("content-type: application/json");
		$user_id= $this->input->get('user_id');
		$no_kec= $this->input->get('no_kec');
		$output = $this->andro->get_kel($user_id,$no_kec);
		echo json_encode($output);	
	}		
	public function getrw()
	{
		header("content-type: application/json");
		$user_id= $this->input->get('user_id');
		$no_kec= $this->input->get('no_kec');
		$no_kel= $this->input->get('no_kel');
		$output = $this->andro->get_rw($user_id,$no_kec,$no_kel);
		echo json_encode($output);	
	}		
	public function getrt()
	{
		header("content-type: application/json");
		$user_id= $this->input->get('user_id');
		$no_kec= $this->input->get('no_kec');
		$no_kel= $this->input->get('no_kel');
		$no_rw= $this->input->get('no_rw');
		$output = $this->andro->get_rt($user_id,$no_kec,$no_kel,$no_rw);
		echo json_encode($output);	
	}
	public function getno_kk()
	{
		header("content-type: application/json");
		$user_id= $this->input->get('user_id');
		$no_kec= $this->input->get('no_kec');
		$no_kel= $this->input->get('no_kel');
		$no_rw= $this->input->get('no_rw');
		$no_rt= $this->input->get('no_rt');
		$output = $this->andro->get_no_kk($no_kec,$no_kel,$no_rw,$no_rt);
		echo json_encode($output);	
	}public function getbio()
	{
		header("content-type: application/json");
		$user_id= $this->input->get('user_id');
		$no_kec= $this->input->get('no_kec');
		$no_kel= $this->input->get('no_kel');
		$no_rw= $this->input->get('no_rw');
		$no_rt= $this->input->get('no_rt');
		$no_kk= $this->input->get('no_kk');
		$output = $this->gisa->get_detail_anggota_kk($no_kk);
		echo json_encode($output);	
	}
}