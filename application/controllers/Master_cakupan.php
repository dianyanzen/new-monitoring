<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_cakupan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');	
		$this->load->model('M_Mastercakupan','mst');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
		
	}
	public function index()
	{
			$menu_id = 43;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));	
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();		
			$t = $this->shr->get_dkb();
			$r = [];
			$j = [];
			if($this->input->post('mlap') != null && $this->input->post('mlap') != 0){
			$mlap = $this->input->post('mlap');
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower($this->shr->get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower($this->shr->get_nama_kel($no_kec,$no_kel)));
			}
			$desc_lap = '';
			if($mlap == 1){
			$desc_lap = ' Jumlah Cakupan Kepemilikan Nik';
			$r = $this->mst->get_cakupan_nik($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_nik($no_kec,$no_kel);
			}else if($mlap == 2){
			$desc_lap = ' Jumlah Cakupan Kepemilikan KK';
			$r = $this->mst->get_cakupan_kk($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_kk($no_kec,$no_kel);
			}else if($mlap == 3){
			$desc_lap = ' Jumlah Cakupan Kepemilikan KIA';
			$r = $this->mst->get_cakupan_kia($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_kia($no_kec,$no_kel);
			}else if($mlap == 4){
			$desc_lap = ' Jumlah Cakupan Perekaman KTP-El';
			$r = $this->mst->get_cakupan_sudah_rekam($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_sudah_rekam($no_kec,$no_kel);
			}else if($mlap == 5){
			$desc_lap = ' Jumlah Cakupan Pencetakan KTP-El';
			$r = $this->mst->get_cakupan_sudah_cetak($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_sudah_cetak($no_kec,$no_kel);
			}else if($mlap == 6){
			$desc_lap = ' Jumlah Cakupan Akta Kelahiran 0-18';
			$r = $this->mst->get_cakupan_akta_018($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_akta_018($no_kec,$no_kel);
			}else if($mlap == 7){
			$desc_lap = ' Jumlah Cakupan Akta Kelahiran 18 Keatas';
			$r = $this->mst->get_cakupan_akta_18($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_akta_18($no_kec,$no_kel);
			}else if($mlap == 8){
			$desc_lap = ' Jumlah Cakupan Akta Kelahiran Keseluruhan';
			$r = $this->mst->get_cakupan_akta($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_akta($no_kec,$no_kel);
			}else if($mlap == 9){
			$desc_lap = ' Jumlah Cakupan Akta Perkawinan';
			$r = $this->mst->get_cakupan_perkawinan($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_perkawinan($no_kec,$no_kel);
			}else if($mlap == 10){
			$desc_lap = ' Jumlah Cakupan Akta Perkawinan Non Muslim';
			$r = $this->mst->get_cakupan_perkawinan_non_muslim($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_perkawinan_non_muslim($no_kec,$no_kel);
			}else if($mlap == 11){
			$desc_lap = ' Jumlah Cakupan Akta Perkawinan Muslim';
			$r = $this->mst->get_cakupan_perkawinan_muslim($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_perkawinan_muslim($no_kec,$no_kel);
			}else if($mlap == 12){
			$desc_lap = ' Jumlah Cakupan Akta Perceraian';
			$r = $this->mst->get_cakupan_perceraian($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_perceraian($no_kec,$no_kel);
			}else if($mlap == 13){
			$desc_lap = ' Jumlah Cakupan Akta Perceraian Non Muslim';
			$r = $this->mst->get_cakupan_perceraian_non_muslim($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_perceraian_non_muslim($no_kec,$no_kel);
			}else if($mlap == 14){
			$desc_lap = ' Jumlah Cakupan Akta Perceraian Muslim';
			$r = $this->mst->get_cakupan_perceraian_muslim($no_kec,$no_kel);
			// $j = $this->mst->get_jumlah_cakupan_perceraian_muslim($no_kec,$no_kel);
			}

			$data = array(
		 		"stitle"=>'Laporan '.$t.''.$desc_lap.''.$title_wil,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_cakupan',
		 		"jenis_lap"=>$mlap,
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan '.$t,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_cakupan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			$this->load->view('Master_cakupan/index',$data);
        
	}
	
}
