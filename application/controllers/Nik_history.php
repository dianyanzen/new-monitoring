<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nik_history extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Search','scr');	
		$this->load->model('M_Shared','shr');	
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function index()
	{
			$menu_id = 76;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    $isakses_kec = $this->shr->get_give_kec();
		    $isakses_kel = $this->shr->get_give_kel();
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			
			$r = $this->scr->cek_data_history($nik,$this->session->userdata(S_NO_KEC));
			$j = $this->scr->cek_count_history($nik);
			$data = array(
		 		"stitle"=>'Ktp-el Cek History Cetak',
		 		"mtitle"=>'Ktp-el Cek History Cetak',
		 		"my_url"=>'History',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Ktp-el Cek History Cetak',
		 		"mtitle"=>'Ktp-el Cek History Cetak',
		 		"my_url"=>'History',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('cekhistory/index',$data);
		
	}
	
}