<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik_perkawinan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');	
		$this->load->model('M_Laporanperkawinan','kwn');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function index()
	{
			$menu_id = 66;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = 0;
			$no_kel = 0;
			$title_wil = '';
			$no_wil = 'No';
			$kode_wil = 'Agama';
			$r = $this->kwn->get_data_perkawinan($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$j = $this->kwn->get_jumlah_perkawinan($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Akta Perkawinan Menurut Agama '.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Akta Perkawinan',
		 		"my_url"=>'Grafik_perkawinan',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_colnotwil"=>'Y'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Akta Perkawinan',
		 		"mtitle"=>'Grafik Akta Perkawinan',
		 		"my_url"=>'Grafik_perkawinan',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>'No',
		 		"kode_wil"=>'Agama',
		 		"is_colnotwil"=>'Y'
    		);
    		}
			$this->load->view('Grafik/index',$data);
	}
	
	
}
