<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }
     public function Header() {
        // Logo
	        $image_file = K_PATH_IMAGES.'pemkot.png';
	        $this->Image($image_file, 15, 10, 25, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        	$this->ln();
        	$this->SetFont('times', 'B', 12);
        	$this->Cell(0, 0, 'PEMERINTAH KOTA BANDUNG', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        	$this->ln();
        	$this->SetFont('times', 'B', 12);
            $this->Cell(0, 0, 'DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL', 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $this->ln();
            $this->SetFont('times', '', 12);
            $this->Cell(0, 0, 'JL. AMBON NO.1B KODE POS 40116', 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $this->ln();
            $this->Cell(0, 0, 'Telp. (022) 4209891 FAX. (022) 4218695', 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $this->ln();
            $this->ln();
            $this->writeHTML("<hr>", true, false, false, false, '');

            

    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Halaman '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */