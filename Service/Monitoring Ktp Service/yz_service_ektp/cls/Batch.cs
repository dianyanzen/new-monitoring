﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;

namespace yz_service_ektp.cls
{
    class Batch
    {
        #region Main Proccess
        OracleConnection con;
    
        public static string dbcon222 = yz_service_ektp.Properties.Settings.Default.db222;
        private string app_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location.ToString());
        public void process()
        {
            Batch bct = new Batch();
            Console.WriteLine("Checking Connections");
            this.ReadSystemFile("Checking First Connections");
            bct.Connectfirst();
            bct.Connectsecond();
            bct.Connectthird();
            bct.Connectfour();
            bct.Connectfive();
            bct.Connectsix();
            bct.Connectseven();
            bct.Connecteight();

            Console.WriteLine("Progres Done For One Cycle");
            this.ReadSystemFile("Progres Done For One Cycle");
            bct.Closeall();
            Console.WriteLine("Close All Connections");
            this.ReadSystemFile("Close All Connections");
        }
        void Connectfirst()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 221 " + con.ServerVersion);
                this.ReadSystemFile("1. Connected to Oracle db 221 " + con.ServerVersion);
                create_temp_table_first();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("1. " + ex.Message + "Connected to Oracle db 221 : {0}");
            }

        }
        void Connectsecond()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("2. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_second();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("2. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectthird()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("3. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_third();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("3. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectfour()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("4. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_four();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("4. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectfive()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("5. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_five();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("5. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectsix()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 221 " + con.ServerVersion);
                this.ReadSystemFile("6. Connected to Oracle db 221 " + con.ServerVersion);
                create_temp_table_six();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("6. " + ex.Message + "Connected to Oracle db 221 : {0}");
            }

        }
        void Connectseven()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("7. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_seven();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("7. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connecteight()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("8. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_eight();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("8. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }

        void Closeall()
        {
            con.Close();
            con.Dispose();
        }
        #endregion

        #region Demogh 221 di update dari demogh 2
        public void create_temp_table_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("1. State Demog 221 From Demog 2: {0}", connection.State);
                    this.WriteToFile("1. State Demog 221 From Demog 2: " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("1. Proccess Starting demogh 2 Update 221 with " + cnt.ToString() + " Data {0}");
                            do_select_table_first();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_first();

                        }
                        else
                        {
                            do_create_table_first();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_first();
            }
        }
        public void do_delete_temp_last_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT A.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("1. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_first" + ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_first();
            }
        }

        public void do_update_table_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB221 A SET (A.CURRENT_STATUS_CODE, A.TGL_CETAK_KTP,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK )";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("1. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_first();
            }
        }
        #endregion

        #region Demogh 2 di update dari demogh 221
        public void create_temp_table_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog 2 From Demog 221: {0}", connection.State);
                    this.WriteToFile("2. State Demog 2 From Demog 221: " + connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("2. Proccess Starting demogh 221 update 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_second();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_second();

                        }
                        else
                        {
                            do_create_table_second();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_second();
            }
        }
        public void do_delete_temp_last_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("2. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT B.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";
                                           
                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("2. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_second" + ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_second();
            }
        }

        public void do_update_table_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK )";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("2. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_second();
            }
        }
        #endregion

        #region Demogh all 2 di update dari demogh 2
        public void create_temp_table_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("3. State Demog All 2 From Demog 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("3. Proccess Starting demogh 2 update all 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_third();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_third();

                        }
                        else
                        {
                            do_create_table_third();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_third();
            }
        }
        public void do_delete_temp_last_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("3. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT A.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("3. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_third" + ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_third();
            }
        }

        public void do_update_table_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS_ALL@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("3. " + "Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_third();
            }
        }
        #endregion
        
        #region Demogh 2 di update dari demogh all 2
        public void create_temp_table_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog 2 From Demog All 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("4. " + "Proccess Starting demogh all 2 update 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_four();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_four();

                        }
                        else
                        {
                            do_create_table_four();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_four();
            }
        }
        public void do_delete_temp_last_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("4. " + "Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT B.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("4. " + "Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_four" + ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_four();
            }
        }

        public void do_update_table_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK )";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("4. " + "Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_four();
            }
        }
        #endregion

        #region Demogh all 2 di update dari demogh 221
        public void create_temp_table_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog All 2 From Demog 221: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("Proccess Starting demogh all 2 update 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_five();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_five();

                        }
                        else
                        {
                            do_create_table_five();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_five();
            }
        }
        public void do_delete_temp_last_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT A.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_five" + ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_five();
            }
        }

        public void do_update_table_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS_ALL@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_five();
            }
        }
        #endregion

        #region Demogh 221 di update dari demogh all 2
        public void create_temp_table_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog 221 From Demog All 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("Proccess Starting demogh all 2 update 221 with " + cnt.ToString() + " Data {0}");
                            do_select_table_six();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_six();

                        }
                        else
                        {
                            do_create_table_six();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_six();
            }
        }
        public void do_delete_temp_last_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT B.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_six" + ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_six();
            }
        }

        public void do_update_table_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB221 A SET (A.CURRENT_STATUS_CODE, A.TGL_CETAK_KTP,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_six();
            }
        }
        #endregion

        #region Demogh 2 all di insert dari demogh 2
        public void create_temp_table_seven()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog All 2 Insert From Demog 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM DEMOGRAPHICS@DB2 A LEFT OUTER JOIN DEMOGRAPHICS_ALL@DB2 B ON (A.NIK =B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("Proccess Starting demogh 2 insert all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_seven();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
       
        public void do_update_table_seven()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DEMOGRAPHICS_ALL@DB2 ( NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, NO_KTP, NO_PASPOR, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, NAMA_KET_RT, NAMA_KET_RW, STAT_HBKEL, KET_AGAMA, TMPT_SBL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_KWN, TGL_CRAI, TGL_ENTRI, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, EXCEPTION_CODE, LAST_UPDATED, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, REGION_ID, LOCAL_ID, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK) SELECT  A.NIK, A.NAMA_LGKP, A.TMPT_LHR, A.NAMA_PROP, A.NAMA_KAB, A.NAMA_KEC, A.NAMA_KEL, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.AGAMA, A.JENIS_KLMIN, A.GOL_DRH, A.STAT_KWN, A.TGL_LHR, A.IS_DIRTY, A.CURRENT_STATUS_CODE, A.CREATED, A.CREATED_USERNAME, A.NO_KTP, A.NO_PASPOR, A.NO_RT, A.NO_RW, A.PNYDNG_CCT, A.KLAIN_FSK, A.JENIS_PKRJN, A.KEBANGSAAN, A.GELAR, A.NO_AKTA_LHR, A.NO_AKTA_KWN, A.NO_AKTA_CRAI, A.NAMA_PET_REG, A.NAMA_LGKP_IBU, A.NIK_IBU, A.NAMA_LGKP_AYAH, A.NIK_AYAH, A.NO_KK, A.STAT_KTP, A.NAMA_KET_RT, A.NAMA_KET_RW, A.STAT_HBKEL, A.KET_AGAMA, A.TMPT_SBL, A.TGL_PJG_KTP, A.TGL_AKH_PASPOR, A.TGL_KWN, A.TGL_CRAI, A.TGL_ENTRI, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.GLR_AKADEMIS, A.EXCEPTION_CODE, A.LAST_UPDATED, A.LAST_UPDATED_USERNAME, A.UPLOAD_LOCATION, A.REGION_ID, A.LOCAL_ID, A.AKTA_LHR, A.AKTA_KWN, A.AKTA_CRAI, A.PDDK_AKH, A.STAT_HIDUP, A.FLAG_STATUS, A.NAMA_KEP, A.ALAMAT, A.DUSUN, A.KODE_POS, A.TELP, A.KWRNGRN, A.DOK_IMGR, A.TGL_DTBIT, A.TGL_AKH_DOK, A.SIAK_FLAG, A.COUNT_KTP, A.NO_DOK FROM DEMOGRAPHICS@DB2 A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB2 B WHERE A.NIK =B.NIK)";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Demogh 2 all di insert dari demogh 221
        public void create_temp_table_eight()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog All 2 Insert From Demog 221: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM DEMOGRAPHICS@DB221 A LEFT OUTER JOIN DEMOGRAPHICS_ALL@DB2 B ON (A.NIK = B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("Proccess Starting demogh 221 insert all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_eight();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void do_update_table_eight()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DEMOGRAPHICS_ALL@DB2 ( NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, NO_KTP, NO_PASPOR, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, NAMA_KET_RT, NAMA_KET_RW, STAT_HBKEL, KET_AGAMA, TMPT_SBL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_KWN, TGL_CRAI, TGL_ENTRI, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, EXCEPTION_CODE, LAST_UPDATED, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, REGION_ID, LOCAL_ID, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK) SELECT  A.NIK, A.NAMA_LGKP, A.TMPT_LHR, A.NAMA_PROP, A.NAMA_KAB, A.NAMA_KEC, A.NAMA_KEL, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.AGAMA, A.JENIS_KLMIN, A.GOL_DRH, A.STAT_KWN, A.TGL_LHR, A.IS_DIRTY, A.CURRENT_STATUS_CODE, A.CREATED, A.CREATED_USERNAME, A.NO_KTP, A.NO_PASPOR, A.NO_RT, A.NO_RW, A.PNYDNG_CCT, A.KLAIN_FSK, A.JENIS_PKRJN, A.KEBANGSAAN, A.GELAR, A.NO_AKTA_LHR, A.NO_AKTA_KWN, A.NO_AKTA_CRAI, A.NAMA_PET_REG, A.NAMA_LGKP_IBU, A.NIK_IBU, A.NAMA_LGKP_AYAH, A.NIK_AYAH, A.NO_KK, A.STAT_KTP, A.NAMA_KET_RT, A.NAMA_KET_RW, A.STAT_HBKEL, A.KET_AGAMA, A.TMPT_SBL, A.TGL_PJG_KTP, A.TGL_AKH_PASPOR, A.TGL_KWN, A.TGL_CRAI, A.TGL_ENTRI, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.GLR_AKADEMIS, A.EXCEPTION_CODE, A.LAST_UPDATED, A.LAST_UPDATED_USERNAME, A.UPLOAD_LOCATION, A.REGION_ID, A.LOCAL_ID, A.AKTA_LHR, A.AKTA_KWN, A.AKTA_CRAI, A.PDDK_AKH, A.STAT_HIDUP, A.FLAG_STATUS, A.NAMA_KEP, A.ALAMAT, A.DUSUN, A.KODE_POS, A.TELP, A.KWRNGRN, A.DOK_IMGR, A.TGL_DTBIT, A.TGL_AKH_DOK, A.SIAK_FLAG, A.COUNT_KTP, A.NO_DOK FROM DEMOGRAPHICS@DB221 A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB2 B WHERE A.NIK =B.NIK)";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion


        #region Update Demogh Card Printed
        public void do_update_table_nine()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE CARD PRINTED");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB2 A SET A.CURRENT_STATUS_CODE = 'CARD_PRINTED' where A.CURRENT_STATUS_CODE <> 'CARD_PRINTED' AND EXISTS(SELECT 1 FROM CARD_MANAGEMENT B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE CARD PRINTED");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Update Demogh All Card Printed
        public void do_update_table_teen()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE CARD PRINTED");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS_ALL@DB2 A SET A.CURRENT_STATUS_CODE = 'CARD_PRINTED' where A.CURRENT_STATUS_CODE <> 'CARD_PRINTED' AND EXISTS(SELECT 1 FROM CARD_MANAGEMENT B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE CARD PRINTED");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Update Demogh Rekam Card Printed
        public void do_update_table_eleven()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE CARD PRINTED");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB221 A SET A.CURRENT_STATUS_CODE = 'CARD_PRINTED' where A.CURRENT_STATUS_CODE <> 'CARD_PRINTED' AND EXISTS(SELECT 1 FROM CARD_MANAGEMENT B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE CARD PRINTED");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion


        private void WriteToFile(string text)
        {
            try
            {
                Directory.CreateDirectory(app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\");
                string path = app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\ProcessLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format(text, DateTime.Now.ToString("yyyy-MM-dd H:mm:ss fffffff")));
                    writer.WriteLine(string.Format(""));
                    writer.Close();
                }
            }
            catch (Exception e)
            {

            }

        }
        private void ReadSystemFile(string text)
        {
            try
            {
                Directory.CreateDirectory(app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\");
                string path = app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\StatusLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format(text, DateTime.Now.ToString("yyyy-MM-dd H:mm:ss fffffff")));
                    writer.WriteLine(string.Format(""));
                    writer.Close();
                }
            }
            catch (Exception e)
            {

            }

        }
    }
}
